use master
create database Student1
on primary
(
	name=Student1,
	filename='D:\SQL作业\SQL作业3\Student1.mdf',
	size=5mb,
	filegrowth=1mb
)

log on
(
	name=Student1_log,
	filename='D:\SQL作业\SQL作业3\Student1_log.ldf',
	size=1mb,
	maxsize=10mb,
	filegrowth=10%
)
go

use Student1
create table Class
(
	ClassId int primary key identity(1,1),
	ClassName nvarchar(20) not null
)
go

insert into Class(ClassName) values ('一班'),('二班'),
('三班'),('四班'),('五班'),('六班'),('七班'),('八班'),
('九班'),('十班')
go

update Class set ClassName = ('1班') where ClassId = 1

delete Class where ClassId = 10

use Student1
go
select * from Class

use Student1
create table Student
(
	StuId int primary key identity(1,1),
	ClassId int constraint FK_Class_ClassId references Class(ClassId),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) Check( StuSex in('男','女')) not null,
	StuBirthday date,
	StuPhone nvarchar(11) unique(StuPhone),
	StuAddress nvarchar(200)
)
go

insert into Student(StuName,StuSex,StuPhone,ClassId)
values
('一一','男','12345678912',1),('二二','男','12345678913',2),('三三','女','12345678914',3),('四四','女','12345678915',4),
('五五','男','12345678916',5),('六六','女','12345678917',6),('八八','男','12345678918',7),('九九','女','12345678919',8),
('小明','男','12345678922',9),('小李','男','12345678932',6),('小红','女','12345678942',1),('小丽','女','12345678952',2),
('小余','男','12345678962',8),('小陈','男','12345678972',1),('小杨','男','12345678982',5),('小毛','女','12345678992',6),
('小小','女','22345678912',1),('大大','男','32345678912',8),('少少','男','42345678912',9),('多多','女','52345678912',1)

alter table Student add CreateDate datetime default(getdate())

delete Student where ClassId = 2

use Student1
go
select * from Student

use Student1
create table Course
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique(CourseName) not null,
	CourseCredit int default(1) check(CourseCredit >=1 and CourseCredit <=5 ),
	CourseType nvarchar(10) check(CourseType = '专业课' or CourseType = '文化课')
)
go

insert into Course(CourseName)
values
('语文'),('高数'),('英语'),('思修'),('体育'),('职素')

update Course set CourseCredit = 2 where CourseName = '高数'

use Student1
go
select * from Course

use Student1
create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int constraint FK_Student_StuId references Student(StuId),
	CourseId int constraint FK_Course_CourseId references Course(CourseId),
	Score decimal(5,2) unique(Score) not null
)
go

insert into Score(StuId,CourseId,Score)
values
(13,4,86.5),(14,1,89.5),(15,3,94.5),(16,1,62.5),(17,4,60.0),
(18,3,84.5)

delete Score where StuId = 1
delete Score where CourseId = 1
alter table Score add constraint CK_Score_Score check(Score >= 0 and Score <= 100)
alter table Score add constraint DK_Score_Score default'0' for Score

use Student1
go
select * from Score

use Student1
go
select * from Student