create database Student12
on
(
   name='Student12',
   filename='F:\SQL\Student12.mdf',
   size=10mb,
   maxsize=50mb,
   filegrowth=10%
)

log on
(
   name='Student12_log',
   filename='F:\SQL\Student12_log.ldf',
   size=10mb,
   maxsize=50mb,
   filegrowth=10%
)
use Student12
go

create table Class
(
   ClassID int primary key identity(1,1),
   ClassName nvarchar(20) unique not null,
)

--insert 数据
insert into Class(ClassName) 
select '软件1班' union 
select '软件2班' union
select '软件3班' union
select '软件4班' union
select '软件5班' union
select '软件6班' union
select '软件7班' union
select '软件8班' union
select '软件9班' union
select '软件10班'

--修改编号1
update Class set ClassName='软件一班' where ClassID='1'

--删除编号10
delete Class where ClassID='10'

select * from Class

create table Student
(
    StuId int primary key identity(1,1),
	ClassId int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex  nvarchar(1) default('男') check(StuSex='男' or StuSex='女'),
	StuBirthday date ,
	StuPhone nvarchar(11) unique , 
	StuAddress nvarchar(200)
)

set IDENTITY_INSERT Student ON
--插入数据
insert into Student(StuId,ClassId,StuName,StuSex,StuBirthday,StuPhone,StuAddress)
select '10086', 1,   	'张三1',	'男',	'2020/5/6',	    15168,'福建省' union
select '10087', 2,		'张三2',	'男',	'2020/10/6',	15169,'福建省' union
select '10088', 3,		'张三3',	'男',	'2021/3/6',	    15170,'福建省' union
select '10089', 4,		'张三4',	'男',	'2021/8/6',	    15171,'福建省' union
select '10090', 5,		'张三5',	'男',	'2022/1/6',	    15172,'福建省' union
select '10091', 6,		'张三6',	'女',	'2022/6/6',	    15173,'福建省' union
select '10092', 7,		'张三7',	'女',	'2022/11/6',    15174,'福建省' union
select '10093', 8,		'张三8',	'女',	'2023/4/6',	    15175,'福建省' union
select '10094', 9,		'张三9',	'女',	'2023/9/6',	    15176,'福建省'

insert into Student(StuId,ClassId,StuName,StuSex,StuBirthday,StuPhone,StuAddress)
select '10095', 1,   	'张三1',	'男',	'2020/5/6',	    15177,'福建省' union
select '10096', 2,		'张三2',	'男',	'2020/10/6',	15178,'福建省' union
select '10097', 3,		'张三3',	'男',	'2021/3/6',	    15179,'福建省' union
select '10098', 4,		'张三4',	'男',	'2021/8/6',	    15180,'福建省' union
select '10099', 5,		'张三5',	'男',	'2022/1/6',	    15181,'福建省' union
select '10100', 6,		'张三6',	'女',	'2022/6/6',	    15182,'福建省' union
select '10101', 7,		'张三7',	'女',	'2022/11/6',    15183,'福建省' union
select '10102', 8,		'张三8',	'女',	'2023/4/6',	    15184,'福建省' union
select '10103', 9,		'张三9',	'女',	'2023/9/6',	    15185,'福建省' union
select '10104', 9,		'张三9',	'女',	'2023/9/6',	    15186,'福建省' union
select '10105', 9,		'张三9',	'女',	'2023/9/6',	    15187,'福建省' 

select * from Student
--添加创建时间的字段
alter table Student add CreateDate datetime default(getdate())

update Student set CreateDate='2021-03-10 18:21:42.417 '

--删除编号2
delete Student where ClassId='2'

set IDENTITY_INSERT Student OFF

create table Course
(
  CourseId int primary key identity(1,1),
  CourseName nvarchar(50) unique not null,
  CourseCredit int not null default(1) check(CourseCredit>=1 or CourseCredit<=5),
  Classtypes  nvarchar(10) check(Classtypes in('公共课' , '专业课'))
)

insert into Course(CourseName)
select 	'数学' union
select 	'英语' union
select 	'体育' union
select	'语文' union
select 	'html' union
select	'java'

--查看
select * from Course

--修改学分信息
update Course set CourseCredit=2  where CourseName='java'

create table Score
(
   ScoreId int primary key identity(1,1),
   StuId int foreign key references Student(StuId),
   CourseId int foreign key references Class(ClassId),
   Score decimal(5,2) unique not null
)

insert into Score(Score) 
select	60	union
select	61	union
select	62	union
select	63	union
select	64	union
select	65	union
select	66	union
select	67	union
select	68	union
select	69	union
select	70	union
select	71	union
select	72	union
select	73	union
select	74	union
select	75	union
select	76	union
select	77	union
select	78	union
select	79	
		
select * from Score

update Score  set Score=100 where  ScoreId=20
delete Score where StuId=1
delete Score where CourseId=1
alter table Score add constraint  DF default(0) for Score, check(Score>=0 or Score<=100) 

