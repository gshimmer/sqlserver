use Student
go

set identity_insert Student on

insert into Class (ClassName) values ('软件1班'),('软件2班'),('软件3班'),('软件4班'),('软件5班'),('软件6班'),('软件7班'),('软件8班'),('软件9班'),('软件10班')
update Class set ClassName = '软件11班' where ClassID = '1'
delete from Class where ClassID = '10' 

insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('5','1','张三','男','2001.12.22','123456','福州')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('7','2','李四','男','2001.12.25','123457','闽清')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('4','3','赵五','男','2001.12.10','123458','贵州')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('1','4','老六','男','2001.11.22','123459','广州')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('6','5','小芳','女','2001.10.23','123410','长沙')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('10','6','小红','女','2001.12.12','123411','厦门')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('11','7','小贾','女','2001.10.22','123422','上海')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('22','8','不懂','男','2000.12.22','123433','西安')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('15','9','李洁','女','2001.11.12','123477','北京')
insert into Student (StuID,ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values ('9','9','q宝','男','2001.11.8','123499','福州')

alter table Student add CreateDate datetime default getdate()
update Student set CreateDate = getdate() where CreateDate is null 
delete from Student where ClassID = '9'

insert into Course (CourseName) values ('数学'),('语文'),('英语'),('物理'),('化学'),('生物')
select * from Course
update Course set CourseCredit = '3' where CourseName = '数学' 

insert into Score (ScoreID,StuID,CourseID,Score) values ('1','1','3','90.5')
insert into Score (ScoreID,StuID,CourseID,Score) values ('2','4','3','91')
insert into Score (ScoreID,StuID,CourseID,Score) values ('3','5','3','92.5')
insert into Score (ScoreID,StuID,CourseID,Score) values ('4','9','3','88')
insert into Score (ScoreID,StuID,CourseID,Score) values ('6','7','3','92')

delete from Score where StuID = '1'
delete from Score where CourseID = '1'
alter table Score add constraint CK_Score default('0')
alter table Score add constraint DF_Score check(Score>=0 and Score<=100)
