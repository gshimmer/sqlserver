use master
go

create database Student
on(
	name='Student_mdf',
	filename='D:\SQL代码\Student.mdf',
	size=3MB,
	maxsize=50MB,
	filegrowth=10%
)
log on(
	name='Student_ldf',
	filename='D:\SQL代码\Student_ldf.ldf',
	size=3MB,
	maxsize=50MB,
	filegrowth=10%
)
go

use Student
go

create table Class
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)
insert into Class(ClassName) select ('软件1班') union all
select ('软件2班') union all
select ('软件3班') union all
select ('软件4班') union all
select ('软件5班') union all
select ('软件6班') union all
select ('软件7班') union all
select ('软件8班') union all
select ('软件9班') union all
select ('软件10班') 

select * from Class
update Class set ClassName='软件11班' where ClassID=1
delete Class where ClassID=10

create table Student
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex in('男','女')),
	StuBirthday date ,
	StuPhone nvarchar(11) unique ,
	StuAddress nvarchar(200) ,
)
insert into Student  (ClassID,StuName, StuSex, StuBirthday,StuPhone,StuAddress) 
values (1, '张三' ,'男','2002-05-24','153453557','江西南昌') ,
(2,'李四','男','2002-11-25','153053554','江西九江') 
 insert into Student  (ClassID,StuName, StuSex, StuBirthday,StuPhone,StuAddress)
 values(3, '张三' ,'男','2002-04-24','153442557','福建龙岩') ,
(4,'张六','男','2002-05-27','1535355469','福建龙岩') ,
 (5,'张七','男','2002-05-28','15345355417','福建龙岩') ,
(6,'张八','女','2002-05-29','15345355427','福建厦门') ,
 (7,'张九','男','2002-06-01','15345355437','福建厦门'),
 (8,'张十','男','2002-05-14','1534535477','福建厦门'),
 (9,'李七','女','2002-05-04','1534535447','福建厦门'),
 (1,'李五','女','2002-05-13','1234035377','福建三明') ,
(1,'陈六','男','2002-06-24','15745356747','福建三明') ,
(2,'陈七','男','2002-08-25','14584535466','福建三明'),
 (3,'陈八','女','2002-07-26','1574554648','福建三明') ,
(4,'陈九','男','2002-04-27','1546554679','福建福州') ,
(5,'陈十','男','2002-03-28','21345355461','福建福州') ,
(6,'王一','女','2002-02-29','3453546207','福建福州') ,
(7,'王九','男','2002-01-01','1534564637','福建福州') ,
(8,'王十','男','2002-11-14','15457554577','福建福州'),
(9,'王四','女','2002-12-04','1545654477','福建福州') ,
(2,'王五','女','2002-05-01','1534535477','福建福州')

delete Student where ClassID=1
alter table Student add CreateDate datetime default(getdate())
update Student set CreateDate='2021-03-11 13:21:42.417'
select * from Student

create  table Course
(
	Courseld int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int not null default(1) check(CourseCredit>=1 and CourseCredit<=5),
	CourseType nvarchar(10) check(CourseType in
	('专业课' , '公共课'))
)

insert into Course(CourseName)
select 	'高数' union
select 	'外语' union
select 	'体育' union
select	'语文' union
select 	'html' union
select	'java'

select * from Course
update Course set CourseCredit=5  where  CourseName='java'

create table Score 
(
	Scoreld int primary key identity(1,1),
	StuID int foreign key  references Student(StuID),
	Courseld int foreign key references Course(Courseld),
	Score decimal(5,2) unique not null
)
insert into Score(Score) 
select	88	union
select	61	union
select	92	union
select	63	union
select	74	union
select	65	union
select	66	union
select	57	union
select	68	union
select	69	union
select	70	union
select	71	union
select	72	union
select	63	union
select	74	union
select	75	union
select	86	union
select	77	union
select	58	union
select	79

select * from Score
update Course set CourseCredit=5  where  CourseName='外语'
delete Score where StuId=1
delete Score where Courseld=1
alter table Score add constraint  DF default(0) for Score, check(Score>=0 or Score<=100)