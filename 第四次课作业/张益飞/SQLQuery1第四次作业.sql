use master
go
create database student
on
(   name='student',
	fileName='D:\test\student.mdf'
)
log on
(	name='student_log',
	fileName='D:\test\student_log.ldf'
)
use student
go
create table class
(	classld   int primary key identity(1,1),
	classname nvarchar(20) unique not null
)

insert into class values('一班'),('二班'),('三班'),('四班'),('五班'),('六班'),('七班'),('八班'),('九班'),('十班')
select*from class 
update class  set classname='二班最帅' where classld=1
delete from class where classld=10 
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table student
(	stuld   int primary key identity(1,1),
	classld int,
	constraint Fk_student_classld foreign key(classld) references class(classld),
	stuname nvarchar(20) not null,
	stusex  nvarchar(1)  default('男') check(StuSex='男' or StuSex='女'),
	stubirthday date,
	stuphone nvarchar(11) unique,
	stuaddress nvarchar(200)
)
insert into student(classld,stuname,stusex,stubirthday,stuphone) values(1,'张1',default,'2021-03-08',1234),(2,'张2',default,'2021-03-08',2234),(3,'张3',default,'2021-03-08',3234),(4,'张4',default,'2021-03-08',4234),(5,'张5',default,'2021-03-08',5234),(6,'张6',default,'2021-03-08',6234),(7,'张7',default,'2021-03-08',7234),(8,'张8',default,'2021-03-08',8234),(9,'张9',default,'2021-03-08',9234)
alter table student add createdate  datetime  default(getdate())
update student set stubirthday=getdate()
delete  from student where  classld>8
select*from student
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table course
(	courseld int primary key identity(1,1),
	coursename nvarchar(50) unique not null,
	coursescore int not null default(1),
	coursecredit nvarchar(10) 
)

alter table course add constraint Ck_course_coursecredit check (coursecredit in('专业课','公共课'))
insert into course(coursename,coursecredit) select '物理','公共课'union  select '数学','公共课'union select '化学','公共课'union select '英语','专业课'union select '语文','专业课'union select '专业','专业课'
update course set coursescore='100' where coursename ='专业' 
select*from course
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table score 
(	scoreld int primary key identity(1,1),
	stuld int ,
	constraint Fk_score_student foreign key(stuld) references student(stuld),
	courseld  int,
	constraint Fk_score_course foreign key(courseld) references course(courseld),
	score  decimal(5,2) unique not null default(0)
)

alter table score add constraint Ck_score_score check(score<=100)
insert into score(stuld,courseld,score) values(11,2,099.99),(12,3,099.98),(13,4,099.97),(14,5,099.96),(15,6,099.95),(16,7,default)
update score set score=100.00 where courseld=6
delete from score where courseld=2  (之前表3课程信息表Course里CourseId没1的编号   只能删除2了)
select*from score



