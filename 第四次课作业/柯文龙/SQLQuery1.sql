create database Student

go

use Student

go

create table Class(
    ClassId int primary key identity (1,1),
	ClassName nvarchar(20) constraint unique_1 unique(ClassName) not null  
	--设置唯一约束(constraint（约束）+约束名（自己可辨识就可以）+unique(列名))
)

go

insert into Class(ClassName) values ('软件一班'),('软件二班'),('软件三班'),('软件四班'),('软件五班'),
                                    ('软件六班'),('软件七班'),('软件八班'),('软件九班'),('软件十班')

go
--数据增删改查部分只能逐一执行
select * from Class order by ClassId asc
--查询数据是否插入成功，并升序排序（asc），每次执行完操作，都可查询是否成功

-- 更新语法： update 表名 set 列名1=新数据1,列名2=新数据2列名3=新数据3, where 查询条件
update Class set ClassName='软件1班' where ClassId=1

--删除语法： delete from 表名where 查询条件;
delete from Class  where ClassId=10


create table Students(
    StuId int primary key identity (1,1),
	ClassId int constraint [Fk_Students_ClassId] foreign key([ClassId]) references [Class]([ClassId]) not null,
	--建表时创建外键关联约束（constraint +[约束名]+foreign key +([关联外键名])+references [外键表]([外键中的主键])）
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default'男'check (StuSex in('男','女')),
	StuBirthday date null, --date为时间专用字段类型
	StuPhone nvarchar(11) constraint unique_2 unique(StuPhone) null,
	StuAddress nvarchar(20) null,
	
	
	)

	go



	select * from Students

	insert into Students(ClassId,StuName,StuSex,StuBirthday,StuPhone) values 
	(1,'小胡','女','2002-09-16',1235544445),(1,'小陈','女','2001-09-11',12345345),(3,'小刘','男','2001-08-16',123445),
	(2,'小王','男','2000-09-16',1234566),(4,'小胡','女','2002-09-16',124545345),(8,'小陆','女','2001-06-16',112345),
	(9,'小胡','男','2000-03-19',123345),(2,'小李','男','2000-01-16',122345),(4,'小花','女','2001-01-11',1422345),
	(6,'小王','男','2000-01-16',1234125),(7,'小华','女','2001-09-16',12324245),(5,'小楚','女','2001-09-19',14562345),
	(1,'小赖','男','2000-06-19',12345235),(4,'小段','男','2000-01-16',1233523545),(2,'小柯','男','2002-01-16',12323523545),
	(8,'小施','女','2002-09-16',1234524532),(6,'小易','男','2000-09-16',1234525),(7,'小罗','男','2002-09-16',122532345),
	(5,'小许','男','2000-09-16',12325345),(2,'小徐','女','2002-09-16',123425235)
	--插入数据，由于有唯一约束影响，所以手机列数据必须不同

	alter table Students add CreateDate datetime default (getdate())
	--插入列CreateDate：alter table 表名 add 列名 列名类型 默认值 getdate()--》系统时间

	update Students set CreateDate=getdate()
	--此条直接更新表里CreateDate中所有数据

	delete from Students where ClassId=1
	--删除1班所有学生信息

	--每次执行完成，最好进行一次数据查询，好确定是否成功执行

 
create table Course(
    CourseId int primary key identity (1,1),
	CourseName nvarchar(50) constraint unique_3 unique(CourseName) not null,
	CourseCredit int default 1 check (CourseCredit>=1 and CourseCredit<=5),

	--default 为默认值 check括号内为取值范围，该列的取值范围表达为列名>=x and 列名<=y
	CourseCredits nvarchar(10) check(CourseCredits in('专业课','公共课'))
)

go

insert into Course (CourseName,CourseCredits) values
('Java','专业课'),('Html5','专业课'),('英语','公共课'),
('思修','公共课'),('SQL','专业课'),('体育','公共课')

select * from Course

update Course set CourseCredit=4 where CourseName='Java'


create table Score(
    ScoreId int primary key identity (1,1),
	StuId int constraint [Fk_Score_StuId] foreign key([StuId]) references [Students]([StuId]) not null,
	CourseId int constraint [Fk_Score_CourseId] foreign key([CourseId]) references [Course]([CourseId]) not null,
	Score decimal(5,2) constraint unique_4 unique(Score) not null

)

insert into Score(StuId,CourseId,Score)values

 (1,3,52.8),(2,4,95.5),(3,5,90),(4,2,10),(5,6,15),(6,5,20),(7,4,25),
(8,3,30),(9,1,35),(10,5,80),(11,4,70),(12,2,41),(13,1,5),
(14,2,75),(15,5,100),(16,4,69),(17,3,60.5),(18,3,70.3),
(19,6,80.6),(20,6,91)

delete from  Score where StuId=1

delete from Score where CourseId=1

alter table Score add constraint CK_Score check (Score between 0 and 100)

alter table Score add constraint DF_Score default 0 for Score