use master--1
go--2
--3
create database Student04--4
on(--5
	name='Student04_mdf',
	filename='D:\SQL代码\Student04.mdf',
	size=3MB,
	maxsize=50MB,
	filegrowth=10%
)--11
log on(--12
	name='Student04_ldf',
	filename='D:\SQL代码\Student04_ldf.ldf',
	size=3MB,
	maxsize=50MB,
	filegrowth=10%
)--18
go

use Student04
go

create table Class--24
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)--28
insert into Class(ClassName) select ('软件1班') union all
select ('软件2班') union all
select ('软件3班') union all
select ('软件4班') union all
select ('软件5班') union all
select ('软件6班') union all
select ('软件7班') union all
select ('软件8班') union all
select ('软件9班') union all
select ('软件10班') 
--39
select * from Class
update Class set ClassName='软件11班' where ClassID=1
delete Class where ClassID=10

create table Student--43
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex in('男','女')),
	StuBirthday date ,
	StuPhone nvarchar(11) unique ,
	StuAddress nvarchar(200) ,
	
)--53
insert into Student  (ClassID,StuName, StuSex, StuBirthday,StuPhone,StuAddress) 
values (1, '张三' ,'男','2002-05-24','153453557','江西南昌') ,
(2,'李四','男','2002-11-25','153053554','江西九江') 
 insert into Student  (ClassID,StuName, StuSex, StuBirthday,StuPhone,StuAddress)
 values(3, '张三' ,'男','2002-04-24','153442557','江西南昌') ,
(4,'张六','男','2002-05-27','1535355469','江西抚州') ,
 (5,'张七','男','2002-05-28','15345355417','江西新余') ,
(6,'张八','女','2002-05-29','15345355427','江西南昌') ,
 (7,'张九','男','2002-06-01','15345355437','江西南昌'),
 (8,'张十','男','2002-05-14','1534535477','江西九江'),
 (9,'李七','女','2002-05-04','1534535447','江西南昌'),
 (1,'李五','女','2002-05-13','1234035377','江西南昌') ,
(1,'陈六','男','2002-06-24','15745356747','江西南昌') ,
(2,'陈七','男','2002-08-25','14584535466','江西九江'),
 (3,'程八','女','2002-07-26','1574554648','江西赣州') ,
(4,'程九','男','2002-04-27','1546554679','江西抚州') ,
(5,'程十','男','2002-03-28','21345355461','江西新余') ,
(6,'王一','女','2002-02-29','3453546207','江西南昌') ,
(7,'王九','男','2002-01-01','1534564637','江西南昌') ,
(8,'王十','男','2002-11-14','15457554577','江西九江'),
(9,'王四','女','2002-12-04','1545654477','江西南昌') ,
(2,'王五','女','2002-05-01','1534535477','江西南昌')

delete Student where ClassID=1
alter table Student add CreateDate datetime default(getdate())
update Student set CreateDate='2021-03-11 13:21:42.417'
select * from Student

create  table Course
(
	Courseld int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int not null default(1) check(CourseCredit>=1 and CourseCredit<=5),
	CourseType nvarchar(10) check(CourseType in
	('专业课' , '公共课'))
)

insert into Course(CourseName)
select 	'数学' union
select 	'英语' union
select 	'体育' union
select	'语文' union
select 	'html' union
select	'java'

select * from Course
update Course set CourseCredit=5  where  CourseName='java'

create table Score 
(
	Scoreld int primary key identity(1,1),
	StuID int foreign key  references Student(StuID),
	Courseld int foreign key references Course(Courseld),
	Score decimal(5,2) unique not null
)
insert into Score(Score) 
select	88	union
select	61	union
select	92	union
select	63	union
select	74	union
select	65	union
select	66	union
select	57	union
select	68	union
select	69	union
select	70	union
select	71	union
select	72	union
select	63	union
select	74	union
select	75	union
select	86	union
select	77	union
select	58	union
select	79

select * from Score
update Course set CourseCredit=5  where  CourseName='英语'
delete Score where StuId=1
delete Score where Courseld=1
alter table Score add constraint  DF default(0) for Score, check(Score>=0 or Score<=100) 
