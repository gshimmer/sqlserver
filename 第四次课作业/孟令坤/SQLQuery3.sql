use master
create database Students
on primary
(
	name = Students,
	filename = 'D:\TEXT\Students.mdf',
	size = 5MB,
	maxsize = 20MB,
	filegrowth = 1MB
)
log on
(
	name = Students_log,
	filename = 'D:\TEXT\Students_Log.ldf',
	size = 1MB,
	maxsize = 10MB,
	filegrowth = 10%
)
go

use Students
create table Class
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique(ClassName)
)
go

insert into Class(ClassName) values ('一班'),('二班'),('三班'),('四班'),('五班'),
('六班'),('七班'),('八班'),('九班'),('十班')
go

update Class set ClassName = ('1班') where ClassID = 1

delete Class where ClassID = 10

use Students
create table Student
(
	StuID int primary key identity(1,1),
	ClassID int constraint FK_Class_ClassID references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) not null,
	StuBirthday date,
	StuPhone nvarchar(11) unique(StuPhone),
	StuAddress nvarchar(200),
)
go

insert into Students(StuName,StuSex,StuPhone,ClassID) 
values 
('张三','男','10248191092',1),('李四','男','10248191063',2),('王五','男','10248190691',1),('赵六','女','10248191091',3),('钱七','男','10248190784',4),
('小明','男','10248190801',9),('小红','女','10248190840',8),('小王','男','10248190919',7),('小赵','男','10248190876',6),('小李','男','10248191003',5),
('老王','女','10248190767',2),('老明','男','10248190756',3),('老阿姨','男','10248191072',4),('这啥名','男','10248190613',5),('我不知道','男','10248191129',6),
('编不下去了','男','10248190866',4),('真的','女','10248190626',5),('莓办法','男','10248191107',2),('稿不了','女','10248191054',3),('再见','男','10248190856',7)

alter table Students add CreateDate datetime default(getdate())

delete Students where ClassID = 1

use Students
create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique(CourseName) not null,
	CourseCredit int default(1) check(CourseCredit >= 1 and CourseCredit <=5),
	CourseType nvarchar(10) Check(CourseType = '专业课' or CourseType = '文化课')
)
go

insert into Course(CourseName) values ('高数'),
('英语'),('概论'),('职素'),('专业'),('体育')

select * from Course

update Course set CourseCredit = 2 where CourseName = '专业'

use Students
create table Score
(
	ScoreID int primary key identity(1,1),
	StuID int constraint FK_Student_StuID references Student(StuID),
	CourseID int constraint FK_Course_CourseID references Course(CourseID),
	Score decimal(5,2) unique(Score) not null
)
go

insert into Score(StuID,CourseID,Score) 
values 
(36,1,60.5),(39,1,59.0),(34,3,64.5),(33,1,78.0),(27,1,80.5),
(27,1,81.0),(27,1,56.0),(29,1,85.0),(24,1,90.5),(26,2,65.5),
(36,2,83.5),(42,2,84.0),(29,2,90.0),(30,3,89.2),(31,4,88.3),
(32,2,99.5),(33,2,66.0),(34,6,76.5),(35,5,67.0),(42,3,60.0)

delete Score where StuID = 1
delete Score where CourseID = 1
alter table Score add constraint CK_Score_Score check(Score >= 0 and Score <= 100)
alter table Score add constraint DK_Score_Score default'0' for Score