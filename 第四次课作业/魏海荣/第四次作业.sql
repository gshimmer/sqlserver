use Student
go

insert into Class(ClassName) values ('����1��'),('��������'),('��������'),('�����İ�'),('�������'),('��������'),('�����߰�'),('�����˰�'),('�����Ű�'),('����ʮ��')

update Class set ClassName = '����һ��' where ClassID = '1'

delete from Class where ClassID = '10'

alter table Student add CreateDate datetime default(getdate())

insert into Student(ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values 
	(1,'һ��','��','2002-1-1',12345670001,'bejing'),
	(2,'����','Ů','2002-1-2',12345670010,'tianjin'),
	(3,'����','��','2002-1-17',12345670100,'shanghai'),
	(4,'�ĺ�','��','2002-4-16',12345671000,'bejing'),
	(5,'���','Ů','2002-1-15',12345671001,'fujian'),
	(6,'����','��','2002-1-1',12345671010,'guangdong'),
	(7,'�ߺ�','��','2002-1-1',12345671100,'guangxi'),
	(8,'�˺�','��','2002-4-18',12345671101,'jiangxi'),
	(9,'�ź�','Ů','2002-1-1',12345671110,'bejing'),
	(1,'ʮ��','��','2002-7-1',12345671111,'shanxi'),
	(2,'Ҽ��','��','2002-1-9',00001234567,'bejing'),
	(3,'����','��','2002-2-6',00011234567,'taiwan'),
	(4,'����','Ů','2002-3-4',00101234567,'bejing'),
	(5,'����','��','2002-5-1',01001234567,'bejing'),
	(6,'���','��','2002-3-6',10001234567,'hunan'),
	(7,'½��','��','2002-8-1',10011234567,'bejing'),
	(8,'���','��','2002-11-1',10101234567,'henan'),
	(9,'�ƺ�','Ů','2002-12-1',11001234567,'bejing'),
	(1,'����','��','2002-9-1',11011234567,'bejing'),
	(2,'ʰ��','Ů','2002-1-14',11101234567,'hebe')

update Student set CreateDate=getdate()

delete from Student Where ClassID='6'

alter table Course add CourseType nvarchar(10) check(CourseType in ('רҵ��','������'))

insert into Course(CourseName) values('����'),('��ѧ'),('Ӣ��'),('����'),('��ѧ'),('����')

select * from Course

update Course set CoueseCredit=4 where CourseName='Ӣ��'

insert into Score(StuId,CourseId,Score) values 
	(1,1,99),
	(2,1,89),
	(3,1,98.23),
	(4,2,49),
	(5,2,69),
	(6,2,79),
	(7,3,98),
	(8,3,17),
	(9,3,49.5),
	(10,3,61),
	(11,3,92),
	(12,4,83),
	(13,4,46),
	(14,4,67),
	(15,4,58),
	(16,4,79.5),
	(17,5,68),
	(18,5,72),
	(19,5,63),
	(20,6,74)

delete from Score where StuID ='1' or CourseID='1'

alter table Score add constraint DK_Score_Score default(0)
alter table Score add constraint CK_Score_Score check(Score>=0 and Score<=100)