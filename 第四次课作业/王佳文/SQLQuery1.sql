use master
go
create database Students
on(
	name='Students',
	filename='C:\TEXT\Students.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10%
)
log on(
	name='Students_log',
	filename='C:\TEXT\Students_log.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10%
)
go
use Students
go
create table Class
(
	ClassID  int primary key  ,
	ClassName nvarchar(20) unique not null,
)
create table Student
(
	StuID int  primary key identity(1,1) not null ,
	ClassID int references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女'),
	StuBirthday date,
	StuAddress nvarchar(200),
	CreateDate datetime default(getdate())

)
create table Course
(
	CourseID int primary key identity not null,
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check(CourseCredit=1 or CourseCredit=2 or CourseCredit=3 or CourseCredit=4 or CourseCredit=5    )  not null,
	CourseTypes nvarchar(10) check(CourseTypes='公共课' or CourseTypes='专业课 '),
)
create table  Score
(
	ScoreID int primary key identity not null,
	StuID int references Student(StuID) ,
	CourseID int references Class(ClassID),
	Score decimal(5,2) unique not null,
)
use students
go
select * from Class
insert into Class values(1, '软件一班'),(2, '软件二班'),(3, '软件三班'),(4, '软件四班'),(5, '软件五班'),(6, '软件六班'),(7, '软件七班'),(8, '软件八班'),(9, '软件九班'),(10, '软件十班')
update Class set ClassName='软件十一班'where ClassID=1
delete from Class where ClassID=10

select * from Student
insert into Student(ClassID,StuName,StuSex,StuBirthday,StuAddress) values(1,'张三','男','2002-2-2','福建省'),(2,'李四','女','2002-3-2','福建省'),(3,'张二三','女','2002-2-12','福建省'),(4,'王五','男','2002-2-2','福建省'),(5,'王小二','男','2002-2-2','福建省'),(6,'张大三','男','2002-2-2','福建省'),(7,'张小四','女','2002-2-2','福建省'),(8,'张三三','女','2002-2-2','福建省'),(1,'王二虎','男','2002-2-2','福建省'),(1,'张二蛋','男','2002-2-2','福建省'),(2,'李二四','女','2002-2-2','福建省'),(4,'张四三','女','2002-2-2','福建省'),(1,'张三','男','2002-2-2','福建省'),(5,'李四','女','2002-2-2','福建省'),(8,'张三','女','2002-2-2','福建省'),(6,'张三','男','2002-2-2','福建省'),(9,'王三','男','2002-2-2','福建省'),(1,'王五','女','2002-2-2','福建省'),(9,'王三','女','2002-2-2','福建省'),(4,'小唐','男','2002-2-2','福建省')
update Student set  CreateDate=getdate()
delete from Student where ClassID=1

select * from Course
insert into Course(CourseName) values('语文'),('数学'),('英语'),('物理'),('化学'),('生物')
update Course set CourseCredit=2 where CourseName='语文'

select * from Score
alter table Score add constraint DK_Score_Score default(0) for Score
insert into Score(StuID,CourseID,Score) values(2,1,90),(3,2,55),(4,1,25),(5,1,40),(6,5,70),(7,4,30),(8,1,100),(8,5,91),(7,3,67),(11,1,78),(12,1,60),(14,3,86),(15,1,66),(16,6,50),(17,2,88),(19,4,69),(20,4,98),(2,1,92),(3,1,96),(4,1,95)
delete from Score where StuID=1
delete from Score where CourseID=1
