create database Student
on
(
	fileName='D:\homework\Student.mdf',
	Name='Student',
	size=1MB,
	Maxsize=5MB,
	filegrowth=5MB
)

log on
(
	FileName='D:\homework\Student_log.ldf',
	Name='Studnet_log',
	size=1MB,
	Maxsize=5MB,
	Filegrowth=5MB
)
go

use Student
go

create table Class
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)

insert into Class (ClassName)
select '����1��' union
select '����2��' union
select '����3��' union
select '����4��' union
select '����5��' union
select '����6��' union
select '����7��' union
select '����8��' union
select '����9��' union
select '����ʮ��'

--update ���� set ����=*** where ����=*** 
update Class set ClassName='����yi��' where ClassID=1

delete from Class where ClassID=10


create table Student
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('��') check(StuSex in('��','Ů')),
	StuBirthday datetime,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(20),
)

insert into Student (ClassID,StuName,StuSex,StuBirthday,StuPhone,StuAddress)
select 1,'������','��','2000-3-10',1231,'�����' union
select 1,'�Ż���','��','2000-4-10',1232,'�����' union
select 2,'�����','��','2000-5-10',1233,'�����' union
select 2,'������','��','2000-6-10',1234,'�����' union
select 3,'ʱ����','��','2000-7-10',1235,'�����' union
select 3,'���','��','2000-8-10',1236,'�����' union
select 4,'��ӱ�','��','2000-9-10',1237,'�����' union
select 4,'������','��','2000-10-10',1238,'�����' union
select 5,'����Ƽ','��','2000-11-10',1239,'�����' union
select 5,'������','��','2000-12-10',12310,'�����' union
select 6,'�κ���','��','2000-4-11',12311,'�����' union
select 6,'������','��','2000-4-12',12312,'�����' union
select 7,'���','��','2000-4-13',12313,'�����' union
select 7,'��׿��','��','2000-4-14',12314,'�����' union
select 8,'���ھ�','��','2000-4-15',12315,'�����' union
select 8,'������','��','2000-4-16',12316,'�����' union
select 9,'�����','��','2000-4-17',12317,'�����' union
select 9,'�½���','��','2000-4-18',12318,'�����' union
select 9,'��Ԋ�s','��','2000-4-19',12319,'�����' union
select 9,'������','��','2000-4-20',12320,'�����'

alter table Student add CreateDate datetime default(getdate())

update Student set CreateDate='2021-03-10 13:21:42.417'

delete from Student where ClassID=2

create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check(CourseCredit>=1 or CourseCredit<=5) not null,
	CourseType nvarchar(10) check(CourseType in('רҵ��','������'))
)


insert into Course (CourseName)
select 'רҵ��' union
select 'Ӣ���' union
select '��ѧ��' union
select '������' union
select '���ο�' union
select '������'

select * from Course

update Course set CourseCredit=5 where CourseName='��ѧ��'
