use master
go

create database Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
go
use Student
go
create table class
(	
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)
insert into Class(ClassName) values ('����һ��'),('��������'),('��������'),('�����İ�'),('�������'),
('��������'),('�����߰�'),('�����˰�'),('�����Ű�'),('����ʮ��')
update class set classname=('һ��')
delete from Class where ClassName= '����ʮ��'

 


create table Student
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('��') Check( StuSex in('��','Ů')),
	StuBirthday date,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200),
)
insert into Student (ClassID ,StuName ,StuSex ,StuBirthday ,StuPhone ,StuAddress) values
('1','����' ,'��', '2001811','100861','������԰'),
('2','����1' ,'��', '2001811','100861','������԰'),
('3','����2' ,'��', '2001811','100861','������԰'),
('4','����3' ,'��', '2001811','100861','������԰'),
('5','����4' ,'��', '2001811','100861','������԰'),
('6','����5' ,'��', '2001811','100861','������԰'),
('7','����6' ,'��', '2001811','100861','������԰'),
('8','����7' ,'��', '2001811','100861','������԰'),
('9','����8' ,'��', '2001811','100861','������԰'),
('110','����9' ,'��', '2001811','100861','������԰')
alter table Student add CreateDate datetime default (getdate())
update student set CreateDate='2021/3/11 11:34'
delete from student where classID=4 




create table Course
(	
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check(1<=CourseCredit and CourseCredit<=5),
	CourseCred nvarchar(10) check (CourseCred='רҵ��' or CourseCred='������'),
)
insert into Course (CourseName) values ('����','����','˼��','רҵ','��ѧ','Ӣ��')
select CourseName from Course
 update Course set CourseCredit=4 where CourseName='��ѧ'





create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int foreign key references Student(StuID),
	CourseId int foreign key references Course(CourseId),
	Score  decimal (5,2) unique not null,
)  
insert into Score(Score) values
(1,2,3,4,5,6,7,8,9,0,11,12,13,14,15,16,17,18,19,20)
update Course set CourseName=2 where CourseId=3
delete from Score where StuId=1
delete from Course where CourseId=1
alter table Score add constraint UK_Score_Score default(0) for Score,check(Score>0 and Score<100)
