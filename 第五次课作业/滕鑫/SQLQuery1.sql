create database Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use Student
go
create table stuinfo
(
	stuNO nvarchar(10) unique not null,
	stuName nvarchar(10) not null,
	stuAge int not null,
	stuAddress nvarchar(200) ,
	stuSeat nvarchar(10) not null,
	stuSex nvarchar(1) default('��') check(StuSex='��' or StuSex='Ů')
)
create table stuexam
(
	examno int primary key identity(1,1),
	stuno nvarchar(10) ,
	writtenexam int not null,
	labexam int not null
)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,stuSex)
values('s2501','������','20','�������','1','��'),('s2502','��˹��','18','�����人','2','Ů'),('s2503','���Ĳ�','22','���ϳ�ɳ','3','��'),
('s2504','ŷ������','21','�����人','4','Ů'),('s2505','÷����','20','�����人','5','��'),('s2506','������','19','�������','6','��'),('s2507','�·�','20','�������','7','Ů')
insert into stuexam(stuno,writtenexam,labexam) values ('s2501','50','70'),('s2502','60','65'),('s2503','86','85'),('s2504','40','80'),('s2505','70','90'),('s2506','85','90')
select * from stuinfo
select stuNO as ѧ��, stuName as ���� ,stuAddress as ��ַ,stuSeat as ��λ��,stuSex as �Ա� from stuinfo
select stuName,stuAge,stuAddress from stuinfo
select stuno,writtenexam,labexam from stuexam
select ѧ��=stuno , ����=writtenexam,����=labexam from stuexam
select stuno as ѧ�� , writtenexam as ����, labexam as ���� from stuexam
select stuno ѧ�� ,writtenexam ����,labexam ���� from stuexam
select stuNO,stuName,stuAddress from stuinfo
select stuName+'@'+stuAddress as '����' from stuinfo
select stuno,writtenexam,labexam,writtenexam+labexam from stuexam
select distinct stuAddress from stuinfo
select distinct stuAge as '��������' from stuinfo
select top 3 * from  stuinfo
select top 4 stuName,stuSeat from stuinfo
select top 50 percent *from stuinfo
select*from stuinfo where stuAddress='�����人' or stuAge=20
select labexam  from stuexam where labexam>=60 and labexam<=80  order by labexam desc
select*from stuinfo where stuAddress='�����人'or stuAddress='���ϳ�ɳ'
select*from stuinfo where stuAddress in('�����人','���ϳ�ɳ')
select writtenexam from stuexam where writtenexam<70 or writtenexam>90 order by writtenexam asc
select*from stuinfo where stuAge is null
select*from stuinfo where stuAge is not null
select*from stuinfo where stuName like '��%'
select stuAddress from stuinfo where stuAddress like '%��%'
select*from stuinfo where stuName like '��_'
select*from stuinfo where stuName like '__��%'
select*from stuinfo order by stuAge desc
select*from stuinfo order by stuAge desc,stuSeat asc
select top 1 * from stuexam order by writtenexam desc
select top 1 * from stuexam order by labexam asc