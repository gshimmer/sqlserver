use master
go

create database Stundents

on
(
    name='Stundents',
	filename='D:\TEXT\Stundents.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
log on 
(
   name='Stundents_loig',
   filename='D:\TEXT\Stundents_log.ldf',
   size=5MB,
   maxsize=50MB,
   filegrowth=10%
)
go
use Stundents

go

create table StuInfo
(
  StuNo int primary key identity(1,1),
  StuName nchar(10) check(StuName>=2),
  StuAge int check(StuAge>=0 and StuAge<100) not null,
  StuAddress nchar(200),
  StuSeat char(2) not null,
  StuSex nchar(1) default('1') check(StuSex='1' or  StuSex='0')

)
go
use Stundents

go

create table ExamInfo
(
  ExamNo int primary key identity(1,1),
  StuNo int references StuInfo(StuNo),
  WrittenExam int check(WrittenExam>=0 and WrittenExam<=100) not null,
  LabExam int check(LabExam>=0 and LabExam<=100) not null
)
go

use Stundents
insert into StuInfo(StuNo,StuName ,StuAge ,StuAddress ,StuSeat ,StuSex) values('s2501','张秋力','20','美国硅谷','1'),('s2502','李斯文',18,'湖北武汉','0'),('s2503','马文才',22,'湖南长沙','1'),('s2504','欧阳俊雄',21,'湖北武汉','0'),
('s2505','梅超风',20,'湖北武汉','1'),('s2506','陈旋风',19,'美国硅谷','1'),('s2507','陈风',20,'美国硅谷','0')

go

use Stundents
insert into ExamInfo(stuNO,writtenExam,labExam) values ('s2501','50','70'),('s2502','60','65'),('s2503','86','65'),
('s2504','40','85'),('s2505','70','90'),('s2506','85','90')

go

select StuNo 学号,StuName 姓名, StuAge 年龄, StuAddress 地址,StuSeat 座位号,StuSex 性别 from StuInfo

select StuName,StuAge,StuAddress from StuInfo

select StuNO 学号,笔试 = WrittenExam,LabExam as 机试 from ExamInfo

select StuNo,stuName,StuAddress,邮箱 = StuName + '@' + StuAddress from StuInfo

select StuNo,WrittenExam,LabExam,总分 = WrittenExam + LabExam from ExamInfo

select distinct StuAddress from StuInfo

select stuAge, count(*) 所有年龄 from StuInfo group by StuAge

select top 3 * from StuInfo

select top 4 StuName,StuSeat from StuInfo

select top 50 percent * from StuInfo

select * from StuInfo where StuAddress = '湖北武汉' and StuAge = 20

select * from ExamInfo where LabExam >= 60 and LabExam <= 80 order by LabExam desc

select * from StuInfo where StuAddress = '湖北武汉' or StuAddress = '湖南长沙'
select * from StuInfo where StuAddress in ('湖北武汉' , '湖南长沙')

select * from ExamInfo where WrittenExam < 70 or WrittenExam > 90 order by WrittenExam asc

select * from StuInfo where StuAge is null

select * from StuInfo where StuAge is not null

select * from StuInfo where StuName like '张%'

select * from StuInfo where StuAddress like'%湖%'

select * from StuInfo where StuName like '张_'

select * from StuInfo where StuName like '__俊%'

select * from StuInfo order by StuAge desc

select * from StuInfo order by StuAge desc, stuSeat asc

select top 1 * from ExamInfo order by WrittenExam desc

select top 1 * from ExamInfo order by LabExam asc