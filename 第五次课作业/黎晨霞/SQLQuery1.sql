use master
go

create database	Student05
go

use Student05
go

create table StulS 
(
		StuNO nvarchar(10) unique not null,
		StuName nvarchar(10) not  null,
		StuAge int not null,
		StuAddress nvarchar(200),
		StuSeat nvarchar(8) not null,
		StuSex char(1) default('男') check (StuSex='女' or StuSex='男')
)

insert into StulS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2501','张秋利','20','美国硅谷','1','女')
insert into StulS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2502','李斯文','18','湖北武汉','2','男')
insert into StulS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2503','马文才','22','湖北长沙','3','男')
insert into StulS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2504','欧阳俊雄','21','湖北武汉','3','女')
insert into StulS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2505','梅超风','20','湖北武汉','4','女')
insert into StulS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2506','陈旋风','19','美国硅谷','5','女')
insert into StulS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2507','陈风','20','美国硅谷','7','女')

select *from StulS
select 学号=StuNO ,姓名=StuName,年龄=StuAge,地址=StuAddress,座位号=StuSeat,性别=StuSex from StulS
select Stuname,StuAge,StuAddress from StulS
select 学号=StuNO ,姓名=StuName,地址=StuAddress,邮箱=StuName+StuAddress from StulS
select distinct StuAddress from StulS
select distinct StuAge as 所有年龄  from StulS
select top 3 *from StulS 
select top 4 StuName,StuSeat from StulS 
select top 50 percent *from StulS
select * from StulS where StuAddress in ('湖北武汉','湖南长沙')
select * from StulS where StuAddress='湖北武汉' or StuAddress ='湖南长沙'
select * from StulS where StuAge is null 
select * from StulS where StuAge is not null 
select * from StulS where StuName like '张%' 
select * from StulS where StuAddress like '%湖%' 
select * from StulS where StuName like '张_' 
select * from StulS where StuName like '__俊%' 
select * from StulS order  by  StuAge DESC
select * from StulS order  by  StuAge DESC , StuSeat ASC


create table Grate
(
	ExamNo int primary key identity(1,1),
	StuNo nvarchar(10) references StulS(StuNO),
	WExam int not null,
	LabExam int not null
)
insert into Grate(StuNO,WExam,LabExam)
values('s2501','50','70')
insert into Grate(StuNO,WExam,LabExam)
values('s2502','60','65')
insert into Grate(StuNO,WExam,LabExam)
values('s2503','86','85')
insert into Grate(StuNO,WExam,LabExam)
values('s2504','40','80')
insert into Grate(StuNO,WExam,LabExam)
values('s2505','70','90')
insert into Grate(StuNO,WExam,LabExam)
values('s2506','85','90')

select *from Grate
select 学号=StuNO , 笔试=WExam,机试=LabExam from Grate
select StuNo as 学号 , WExam as 笔试, LabExam as 机试 from Grate
select StuNo 学号 ,WExam 笔试,LabExam 机试 from Grate
select StuNO, WExam ,LabExam,总分=WExam+LabExam from Grate
select * from Grate where  WExam<70 or WExam>90 order by WExam DESC
select top 1 * from Grate order by WExam DESC
select top 1 * from Grate order by WExam ASC