use master
go
create database task
go
use task
go
create table Student
(
stuid nvarchar(10) primary key,
stuname nvarchar(5) not null ,
stuage int ,
stuaddress nvarchar(200),
stuseat int identity(1,1),
stusex int check(stusex=1 or stusex=0)
)
create table result
(
examn0 int primary key identity(1,1),
stuid nvarchar(10) references Student(stuid),
writtenexam int check(writtenexam>=0 or writtenexam<=100),
labexam int check(labexam>=0 or labexam<=100)
)

insert into Student
select 's2501','张秋利','20','美国硅谷','1' union
select 's2502','李斯文','18','湖北武汉','0' union
select 's2503','马文才','22','湖南长沙','1' union
select 's2504','欧阳俊雄','21','湖北武汉','0' union
select 's2505','梅超风','20','湖北武汉','1'union
select 's2506','陈旋风','19','美国硅谷','1' union
select 's2507','陈风','20','美国硅谷','0'
select * from Student
insert into result
select 's2501','50','70' union
select 's2502','60','65' union
select 's2503','86','85' union
select 's2504','40','80' union
select 's2505','70','90' union
select 's2506','85','90' 
select * from result

--1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
select * from Student
select stuid as 学号, stuname as 姓名, stuage as 年龄, stuaddress as 地址,stuseat as 座号,stusex as 性别 from Student
--2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
select stuname,stuage,stuaddress from Student
--3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字
--  注意：要用三种方法
select stuid as  学号,  writtenexam as 笔试 ,labexam   as 机试 from result
select stuid 学号,writtenexam 笔试, labexam 机试 from result
--4.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
select stuid+stuname+'@'+ stuname+stuaddress as 邮箱 from Student
--5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分（笔试+机试）这四列的信息
select stuid,writtenexam,labexam,writtenexam +labexam as 总分 from result
--6.查询学生信息表（stuInfo）中学生来自哪几个地方 
select stuaddress from Student
--7.查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名'所有年龄'
select stuage as 所有年龄 from Student ORDER By stuage
--8.查询学生信息表（stuInfo）中前3行记录 
select TOP 3 * from Student 
--9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
SELECT TOP 4 stuname,stusex from Student
--10.查询学生信息表（stuInfo）中一半学生的信息
select top 50percent * from Student
--11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
select *from Student WHERE stuaddress='湖北武汉' or stuage='20'
--12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列
select labexam from result where labexam>=60 and labexam<=80 order by labexam
--13.查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现，提示
--；or和in）
select * from Student where stuaddress='湖北武汉' or stuaddress='湖南长沙'
select * from Student where stuaddress in('湖北武汉','湖南长沙')
--14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列
select writtenexam from result where not writtenexam>=70 and writtenexam<=90 order by writtenexam desc
--15.查询年龄没有写的学生所有信息
select * from Student where stuage is  null or stuage=''
--16.查询年龄写了的学生所有信息
select* from Student where stuage is not null or not stuage=''
--17.查询姓张的学生信息
select * from Student where stuname like '张%'
--18.查询学生地址中有‘湖’字的信息
select * from Student where stuaddress like '%湖%'
--19.查询姓张但名为一个字的学生信息
select * from Student where stuname like '张_'
--20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
select * from Student where  stuname like '__俊%'
--21.按学生的年龄降序显示所有学生信息
select * from Student order by stuage 
--22.按学生的年龄降序和座位号升序来显示所有学生的信息
select * from Student order by stuage desc ,  stuseat asc 
--23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from result order by writtenexam desc
--24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from result order by labexam 
