use master
go

create database Student2
on
(
	name='Student2',
	filename='D:\SQL作业\SQL作业4\Student2.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)

log on
(
	name='Student2_log',
	filename='D:\SQL作业\SQL作业4\Student2_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student2
go
create table StuInfo
(
	StuNo varchar(20) primary key ,
	StuName nchar(20) not null,
	StuAge int not null,
	StuAdress varchar(20) not null,
	StuSeat int not null,
	StuSex char(2) not null
)
go

insert into StuInfo(StuNo,StuName,StuAge,StuAdress,StuSeat,StuSex) values
('s2501','张秋利',20,'美国硅谷',1,1),('s2502','李斯文',18,'湖北武汉',2,0),
('s2503','马文才',22,'湖南长沙',3,1),('s2504','欧阳俊雄',21,'湖北武汉',4,0),
('s2505','梅超风',20,'湖北武汉',5,1),('s2506','陈旋风',19,'美国硅谷',6,1),
('s2507','陈风',20,'美国硅谷',7,0)
go

select * from StuInfo


use Student2
go
create table Score
(
	ExamNo int primary key identity(1,1),
	StuNO varchar(20) not null,
	WrittenExam int not null,
	LabExam int not null
)
go

insert into Score(StuNo,WrittenExam,LabExam) values
('s2501',50,70),('s2502',60,65),('s2503',86,85),
('s2504',40,80),('s2505',70,90),('s2506',85,90)
go

select * from  Score


--1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
select StuNo as 学号 ,StuName as 姓名,StuAge as 年龄,StuAdress as 地址,StuSeat as 座位号,StuSex as 性别 from StuInfo
--在本来的名字基础上，在后面+as 中文名称即可

--2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
select StuName,StuAge,StuAdress from StuInfo
--套用方法：select * from 表名 然后将*改为你需要的列即可，列名之间要逗号隔开

--3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字注意：要用三种方法
select StuNo as 学号,WrittenExam as 笔试,LabExam as 机试 from Score

--4.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
select StuNo+StuName+StuAdress +StuName+'@'+StuAdress as 邮箱 from StuInfo

--5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分（笔试+机试）这四列的信息
select StuNo,WrittenExam,LabExam,WrittenExam+LabExam as 总分 from Score
--套用方法：select * from 表名 然后将*改为你需要的列即可，列名之间要逗号隔开，将要求列相加as重命名即可

--6.查询学生信息表（stuInfo）中学生来自哪几个地方
select StuName, StuAdress from StuInfo

--7.查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名'所有年龄'
select StuAge as 所有年龄 from StuInfo

--8.查询学生信息表（stuInfo）中前3行记录 
select top 3 * from StuInfo
--套用方法：select * from 表名 在*前加上top 要几行就写几

--9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
select top 4 StuName as 姓名,StuSeat as 座位号 from StuInfo

--10.查询学生信息表（stuInfo）中一半学生的信息
select top 50 percent  * from StuInfo

--11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
select * from StuInfo where stuAdress='湖北武汉' and StuAge=20
--条件查询 where后面接所需的条件，多个条件用and隔开

--12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列
select * from Score where LabExam between 60 and 80 order by LabExam desc
--条件查询 取值范围用between...and...链接，order by进行排序，默认为升序排序(ASC)可不写，降序为（desc）

--13.查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现，提示；or和in）
select * from StuInfo where stuAdress='湖北武汉' or stuAdress= '湖南长沙'
--同一条件多个参数用or隔开
select * from StuInfo where stuAdress in ('湖北武汉','湖南长沙')

--14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列
select * from Score where WrittenExam not between 70 and 90 order by WrittenExam asc

--15.查询年龄没有写的学生所有信息
select * from StuInfo where StuAge=null or StuAge=' '

--16.查询年龄写了的学生所有信息
select * from StuInfo where StuAge is not null
--is not null为不是空的

--17.查询姓张的学生信息
select * from StuInfo where StuName like '%张%'
--模糊查询用like 单引号内为前后各一个百分号

--18.查询学生地址中有‘湖’字的信息
select * from StuInfo where StuAdress like '%湖%'

--19.查询姓张但名为一个字的学生信息
select * from StuInfo where StuName like '%张_'
--模糊查询中，_下划线为占位符，表达为后面一个字，或者前面几个字

--20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
select * from StuInfo where StuName like '%__俊%'

--21.按学生的年龄降序显示所有学生信息
select * from StuInfo  order by StuAge desc 

--22.按学生的年龄降序和座位号升序来显示所有学生的信息
select * from StuInfo order by StuAge desc,StuSeat 

--23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from Score order by WrittenExam desc
--将排名进行降序排序找到第一个就是第一名

--24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from Score order by LabExam  