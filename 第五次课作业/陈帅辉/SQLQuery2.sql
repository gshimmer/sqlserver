use master
go

create database Student
go 
use Student
create table stuinfo
(
StuNo char(5),
StuName nvarchar(5),
StuAge int,
StuAddress nvarchar(20),
StuSeat int,
StuSex nvarchar(1) default('1') check(StuSex in('1','0'))
)
insert into stuinfo values ('$2501','�����',20,'�������',1,1),('$2502','��˹',18,'�����人',2,0),
('$2503','����',22,'���ϳ�ɳ',3,1),('$2504','ŷ������',21,'�����人',4,0),('$2505','����',20,'�����人',5,1),
('$2506','�¿�',19,'�������',6,1),('$2507','�·�',20,'�������',7,1)
go 
use Student
create table StuMark
(
examNo int,
StuNO char(5),
writtenExam int,
labExam int
)
insert into StuMark values('1','2501','50','70'),('2','2502','60','65'),('3','2503','86','85'),('4','2504','40','80'),
('5','2505','70','90'),('6','2506','85','90')
select * from stuinfo
select * from StuMark
select StuNo ���,StuName ����,StuAge ����,StuAddress ��ַ,StuSeat ѧ��,StuSex �Ա�  from stuinfo
select StuAge,StuAddress,StuSex from stuinfo
select StuNo ѧ��, writtenExam ����, labExam ���� from StuMark
select ѧ��=StuNO ,����=writtenExam,����=labExam  from StuMark 
select StuNO as ѧ��,writtenExam as ����,labExam as ���� from StuMark
select StuNO+StuName+StuAddress ѧ�����ֵ�ַ from stuinfo
select StuName+'@'+StuAddress as ���� from stuinfo
select StuNo ѧ��, writtenExam ����, labExam ����,writtenExam + labExam �ܷ� from StuMark
select  distinct StuAddress from stuinfo
select  distinct StuAge,stuage ȫ������ from stuinfo
select * from stuinfo where StuSeat<=3
select StuNo ��λ��,StuName ���� from stuinfo where StuSeat<=4
select top 50 percent * from stuinfo
select * from stuinfo where StuAge=20 and StuAddress='�����人'
select * from StuMark where labExam>=60 and labExam<=80  order by labExam DESC
select * from stuinfo where StuAddress='�����人' or StuAddress='���ϳ�ɳ'
select * from stuinfo where stuAddress in('�����人','���ϳ�ɳ')
select * from StuMark where writtenExam<70 or writtenExam>90 order by writtenExam ASC
select * from stuinfo where StuAge is null or StuAge=''
select * from stuinfo where StuAge is not null
select * from stuinfo where StuName like '��%'
select * from stuinfo where StuAddress like '��%'
select * from stuinfo where StuName like '��_'
select * from stuinfo where StuName like '__��%'
select * from stuinfo  order by StuAge DESC
select * from stuinfo  order by StuAge DESC, StuNo ASC
select  top 1* from StuMark order by  writtenExam DESC
select  top 1* from StuMark order by  writtenExam ASC