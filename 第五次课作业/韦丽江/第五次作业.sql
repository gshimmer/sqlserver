use master
go

create database Student05
go

use Student05
go

create table StuIS
(
	StuNO nvarchar(10) unique not null,
	StuName nvarchar(10) not null,
	StuAge int not null,
	StuAddress nvarchar(200) ,
	StuSeat nvarchar(8) not null,
	StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女')
)

insert into StuIS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2501','张秋利','20','美国硅谷','1','女')
insert into StuIS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2502','张斯文','18','湖北武汉','2','男')
insert into StuIS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2503','马文才','22','湖南长沙','3','男')
insert into StuIS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2504','欧阳雄','21','湖北武汉','3','女')
insert into StuIS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2505','梅超风','20','湖北武汉','4','女')
insert into StuIS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2506','敖玉','19','美国硅谷','5','女')
insert into StuIS(StuNO,StuName,StuAge,StuAddress,StuSeat,StuSex)
values('s2507','陈梅','20','美国硅谷','7','女')

select *from StuIS
select 学号=StuNO ,姓名=StuName,年龄=StuAge,地址=StuAddress,座位号=StuSeat,性别=StuSex from StuIS
select Stuname,StuAge,StuAddress from StuIS
select 学号=StuNO ,姓名=StuName,地址=StuAddress,邮箱=StuName+StuAddress from StuIS
select distinct StuAddress from StuIS
select distinct StuAge as 所有年龄  from StuIS
select top 3 *from StuIS 
select top 4 StuName,StuSeat from StuIS 
select top 50 percent *from StuIS
select * from StuIS where StuAddress in ('湖北武汉','湖南长沙')
select * from StuIS where StuAddress='湖北武汉' or StuAddress ='湖南长沙'
select * from StuIS where StuAge is null 
select * from StuIS where StuAge is not null 
select * from StuIS where StuName like '张%' 
select * from StuIS where StuAddress like '%湖%' 
select * from StuIS where StuName like '张_' 
select * from StuIS where StuName like '__俊%' 
select * from StuIS order  by  StuAge DESC
select * from StuIS order  by  StuAge DESC , StuSeat ASC


create table Grate
(
	ExamNo int primary key identity(1,1),
	StuNo nvarchar(10) references StuIS(StuNO),
	WExam int not null,
	LabExam int not null
)
insert into Grate(StuNO,WExam,LabExam)
values('s2501','50','70')
insert into Grate(StuNO,WExam,LabExam)
values('s2502','60','65')
insert into Grate(StuNO,WExam,LabExam)
values('s2503','86','85')
insert into Grate(StuNO,WExam,LabExam)
values('s2504','40','80')
insert into Grate(StuNO,WExam,LabExam)
values('s2505','70','90')
insert into Grate(StuNO,WExam,LabExam)
values('s2506','85','90')

select *from Grate
select 学号=StuNO , 笔试=WExam,机试=LabExam from Grate
select StuNo as 学号 , WExam as 笔试, LabExam as 机试 from Grate
select StuNo 学号 ,WExam 笔试,LabExam 机试 from Grate
select StuNO, WExam ,LabExam,总分=WExam+LabExam from Grate
select * from Grate where  WExam<70 or WExam>90 order by WExam DESC
select top 1 * from Grate order by WExam DESC
select top 1 * from Grate order by WExam ASC