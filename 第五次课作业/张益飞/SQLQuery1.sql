use studentinfo 
go 
create  table  student5

(	stuno char(5) primary key ,  
	stuname  nvarchar(20),
	stuage   int,
	stuaddress nchar(4),
	stuseat  int,
	stusex   int
)
insert into student5 values('$2501','张秋利',20,'美国硅谷',1,1),('$2502','李斯文',18,'湖北武汉',2,0),('$2503','马文才',22,'湖南长沙',3,1),('$2504','欧阳俊雄',21,'湖北武汉',4,0),('$2505','梅超风',20,'湖北武汉',5,1),('$2506','陈旋风',19,'美国硅谷',6,1),('$2507','陈风',20,'美国硅谷',7,1)



create table student6

(	examno int primary key identity(1,1),
	stuno char(5) ,
	constraint Fk_student6_stuno foreign key(stuno) references student5(stuno),
	writtenexam int ,
	labxam int
)	
insert into student6 values('$2501',50,70),('$2502',60,65),('$2503',86,85),('$2504',40,70),('$2505',70,70),('$2506',85,90)


1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
	select stuno as '学号', stuage '年龄','姓名 和 地址'=stuname+stuaddress,stuseat as '座位号',stusex  as '性别 ' from student5
2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
    select  stuname,stuage,stuaddress from student5
3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字
  注意：要用三种方法
  1.select stuno as '学号',writtenexam as '笔试成绩',labxam as '机试成绩'from student6
  2.select stuno '学号',writtenexam '笔试成绩',labxam  '机试成绩'from student6
  --3.select stuno as '学号',  writtenexam+'0/600'+labxam as '笔试成绩 和 机试成绩'from student6  （数字的添加结果是运算的加）
4.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
	select stuno  '学号', '姓名 和 地址'=stuname+stuaddress from student5 
	select stuno  '学号', '姓名 和 地址'=stuname+stuaddress,  stuname+'@'+stuaddress as '邮箱'  from student5 
5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分（笔试+机试）这四列的信息
	select stuno as '学号',writtenexam as '笔试成绩',labxam as '机试成绩','总分'=writtenexam+labxam from student6
	
6.查询学生信息表（stuInfo）中学生来自哪几个地方 
  select distinct stuaddress  from student5

7.查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名'所有年龄'	
  select distinct stuage  as '所有年龄'   from  student5 
    
8.查询学生信息表（stuInfo）中前3行记录 
  select top 3 * from  student5  

9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
  select stuname,stuseat from student5 where stuno<$2505

10.查询学生信息表（stuInfo）中一半学生的信息
  select top 50 percent *from student5

11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
  select*from student5 where stuaddress='湖北武汉' or stuage=20

12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列
  select labxam  from student6 where labxam>=60 and labxam<=80  order by labxam desc

13.查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现，提示；or和in）
  select*from student5 where stuaddress='湖北武汉'or stuaddress='湖南长沙'
  select*from student5 where stuaddress in('湖北武汉','湖南长沙')

14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列
  select writtenexam from student6 where writtenexam<70 or writtenexam>90 order by writtenexam 
  select  writtenexam from student6 where  writtenexam  like '[^7-9]%'

15.查询年龄没有写的学生所有信息
  select*from student5 where stuage is null or stuage=''

16.查询年龄写了的学生所有信息
  select*from student5 where stuage is not null and not stuage='' 

17.查询姓张的学生信息
	select*from student5 where stuname like '张%'

18.查询学生地址中有‘湖’字的信息
  select stuaddress from  student5 where stuaddress like '%湖%'

19.查询姓张但名为一个字的学生信息
	select*from student5 where stuname like '张_'

20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
    select*from student5 where stuname like '__俊%'

21.按学生的年龄降序显示所有学生信息
   select*from student5   order by stuage desc

22.按学生的年龄降序和座位号升序来显示所有学生的信息
   select*from student5 order by stuage desc,stuseat asc

23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
 select  top 1 *from student6   order by writtenexam desc

24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
 select top 1*  from student6 order by labxam 


