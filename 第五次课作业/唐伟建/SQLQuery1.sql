create database Student
on
(
	FileName='F:\homework\Student.mdf',
	Name='Student',
	size=5MB,
	maxsize=5MB,
	filegrowth=3MB
)
log on
(
	FileName='F:\homework\Student_log.ldf',
	Name='Student_log',
	size=5MB,
	maxsize=5MB,
	filegrowth=3MB
)
go

use Student

go

create table Student
(
	StuNo varchar(10) primary key not null,
	StuName nvarchar(10) not null,
	StuAge int,
	StuAddress nvarchar(20),
	StuSeat int,
	StuSex int check(StuSex=1 or StuSex=0) not null
)

create table Score
(
	ExamNo int primary key identity(1,1) not null,
	StuNo varchar(10) not null,
	WrittenExam int default(0) not null,
	LabExam int not null
)


insert into Student values 
('s2501','张秋利',20,'美国硅谷',1,1),
('s2502','李斯文',18,'湖北武汉',2,0),
('s2503','马文才',22,'湖南长沙',3,1),
('s2504','欧阳俊雄',21,'湖北武汉',4,0),
('s2505','梅超风',20,'湖北武汉',5,1),
('s2506','陈旋风',19,'美国硅谷',6,1),
('s2507','陈风',20,'美国硅谷',7,0)

select * from Student


insert into Score values
('s2501',50,70),
('s2502',60,65),
('s2503',86,85),
('s2504',40,80),
('s2505',70,90),
('s2506',85,90)

alter table Score add constraint FK_Score_StuNo foreign key(StuNo) references Student(StuNo)


select * from Score
--1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
select StuNo 学号,StuName 姓名,StuAge 年龄,StuAddress 地址,StuSeat 座位号,StuSex 性别 from Student
--2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
select StuName,StuAge,StuAddress from Student
--3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字
--注意：要用三种方法
select StuNo 学号 from Score
--
select 笔试=WrittenExam from Score
--
select LabExam as 机试 from Score
--4.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
select StuNo 学号,StuName 姓名,StuAddress 地址, StuName+'@'+StuAddress 邮箱 from Student
--5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分（笔试+机试）这四列的信息
select StuNo,writtenExam,LabExam,writtenExam+LabExam as 总分 from Score
--6.查询学生信息表（stuInfo）中学生来自哪几个地方
select distinct StuAddress from Student
--7.查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名'所有年龄'
select distinct 所有年龄=StuAge from Student
--8.查询学生信息表（stuInfo）中前3行记录 
select top 3 * from Student
--9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
select top 4 StuName,StuSeat from Student
--10.查询学生信息表（stuInfo）中一半学生的信息
select top 50 percent * from Student
--11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
select * from Student where StuAge=20 and StuAddress='湖北武汉'
--12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列
select  * from Score where LabExam>=60 and LabExam<=80 order by LabExam DESC 
--13.查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现，提示；or和in）
select * from Student where StuAddress='湖北武汉' or StuAddress='湖南长沙'
select * from Student where StuAddress in ('湖北武汉','湖南长沙')
--14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列
select * from Score where WrittenExam<=70 or WrittenExam>=90 order by WrittenExam ASC
--15.查询年龄没有写的学生所有信息
select * from Student where StuAge is null
--16.查询年龄写了的学生所有信息
select * from Student where StuAge is not null
--17.查询姓张的学生信息
select * from Student where StuName like '张%'
--18.查询学生地址中有‘湖’字的信息
select * from Student where StuAddress like '%湖%'
--19.查询姓张但名为一个字的学生信息
select * from Student where StuName like '张_'
--20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
select * from Student where StuName like '__俊%'
--21.按学生的年龄降序显示所有学生信息
select * from Student order by StuAge DESC
--22.按学生的年龄降序和座位号升序来显示所有学生的信息       --?
select * from Student order by StuSeat,StuAge DESC          
--23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from Score order by WrittenExam DESC
--24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from Score order by WrittenExam ASC
