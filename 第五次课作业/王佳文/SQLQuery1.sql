use master
go
create database Students
on(
	name='Students',
	filename='C:\TEXT\students.maf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
log on(
	name='Students_log',
	filename='C:\TEXT\students_log.maf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
go
use students
go
create table stuinfo
(
	stuNO varchar(50) primary key not null,
	stuName nvarchar(20) not null,
	stuAge int not null,
	stuAddress nvarchar(50) not null,
	stuSeat int not null,
	stuSex int  check(stuSex=1 or stuSex=0) not null
)
create table stuexam
(
	examNo int primary key identity(1,1),
	stuNO varchar(50) references stuinfo(stuNO),
	writtenExam int not null,
	labExam int not null,
)
use students
go
insert into stuinfo values('s2501','������',20,'�������',1,1),
('s2502','��˹��',18,'�����人',2,0),
('s2503','���Ĳ�',22,'���ϳ�ɳ',3,1),
('s2504','ŷ������',21,'�����人',4,0),
('s2505','÷����',20,'�����人',5,1),
('s2506','������',19,'�������',6,1),
('s2507','�·�',20,'�������',7,0)
insert into stuexam(stuNO,writtenExam,labExam) values('s2501',50,70),
('s2502',60,65),
('s2503',86,85),
('s2504',40,80),
('s2505',70,90),
('s2506',85,90)
select * from stuinfo
select stuNO as ѧ��, stuName as ����,stuAge as ����, stuAddress as ��ַ,stuSeat as ��λ��,stuSex as �Ա� from stuinfo
select stuName,stuAge,StuAddress from stuinfo
select stuNO as ѧ��,writtenExam as ����,labExam as ���� from stuexam 
select stuNO  ѧ��,writtenExam  ����,labExam   ���� from stuexam  
select ѧ��=stuNO ,����=writtenExam,����=labExam  from stuexam  
select stuName+'@'+stuAddress as ���� from stuinfo
select stuNO as ѧ��,writtenExam as ����,labExam as ����,writtenExam+labExam as �ܷ� from stuexam 
select distinct stuAddress from stuinfo
select distinct stuAge as �������� from stuinfo
select top 3 *from stuinfo
select top 50 percent * from stuinfo
select * from stuexam where labExam>=60 and labExam<=80  order by labExam DESC
select * from stuinfo where stuAddress in('�����人','���ϳ�ɳ')
select * from stuinfo where stuAddress='�����人' or stuAddress='���ϳ�ɳ'
select * from stuexam where writtenExam<70 or writtenExam>90 order by writtenExam ASC
select * from stuinfo where stuAge is null
select * from stuinfo where stuAge is not  null
select * from stuinfo where stuName like '��%'
select * from stuinfo where stuAddress like '��%'
select * from stuinfo where stuName like '��'
select * from stuinfo where stuName like '__��'
select * from stuinfo  order by stuAge DESC
select * from stuinfo order by stuAge DESC,stuSeat ASC
select  top 1* from stuexam order by  writtenExam DESC
select top 1 * from stuexam order by labExam