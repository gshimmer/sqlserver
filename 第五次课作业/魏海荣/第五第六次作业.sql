use Student
go

create table StuInfo
(
	stuNo char(5) primary key(stuNo),
	stuName nvarchar(20),
	stuAge int,
	stuAddress text,
	stuSeat int identity(1,1),
	stuSex char(1) check(stuSex in(1,0))
)

create table StuExam
(
	examNo int identity(1,1),
	stuNo char(5),
	writtenExam int check(writtenExam>=0 and writtenExam<=100),
	labExam int check(labExam>=0 and labExam<=100)
)
 
 alter table StuExam add constraint RK_StuExam_stuNo foreign key(stuNo) references StuInfo(stuNo)

 insert into StuInfo values 
 ('s2501','张秋利',20,'美国硅谷',1),
 ('s2502','李斯文',18,'湖北武汉',0),
 ('s2503','马文才',22,'湖南长沙',1),
 ('s2504','欧阳俊雄',21,'湖北武汉',0),
 ('s2505','梅超风',20,'湖北武汉',1),
 ('s2506','陈旋风',19,'美国硅谷',1),
 ('s2507','陈风',20,'美国硅谷',0)

 delete from StuExam

 insert into StuExam values
 ('s2501',50,70),('s2502',60,65),('s2503',86,85),('s2504',40,80),('s2505',70,90),('s2506',85,90),('s2507',50,40)
 --1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
 select stuNo 学号,stuName 姓名,stuAge 年龄,stuAddress 地址,stuSeat 座位号,stuSex 性别 from StuInfo
 --2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
 select stuName,stuAge,stuAddress from StuInfo
 --3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字  注意：要用三种方法
 select examNo 学号,writtenExam 笔试,labExam 机试 from StuExam
 --
 --
 --4.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
 alter table StuInfo alter column stuAddress char(8)
 select stuName+'@'+stuAddress 邮箱 from StuInfo
 --5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分（笔试+机试）这四列的信息
 select examNo 学号,writtenExam 笔试,labExam 机试,writtenExam+labExam 总分 from StuExam
 --6.查询学生信息表（stuInfo）中学生来自哪几个地方 
 select distinct stuAddress from StuInfo
 --7.查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名'所有年龄'
 select distinct stuAge 所有年龄 from StuInfo
 --8.查询学生信息表（stuInfo）中前3行记录
 select top 3 * from StuInfo
 --9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
 select top 4 stuName,stuSeat from StuInfo
 --10.查询学生信息表（stuInfo）中一半学生的信息
 select top 4 * from stuInfo
 --11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
 select * from StuInfo where stuAge=20 and stuAddress='湖北武汉'
 --12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列
 select * from StuExam where labExam>=60 and labExam<=80 order by labExam DESC
 --13.查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现，提示；or和in）
 select * from StuInfo where stuAddress='湖北武汉' or stuAddress='湖南长沙'
 select * from StuInfo where stuAddress in('湖北武汉','湖南长沙')
 --14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列
 select * from StuExam where not writtenExam>=70 and writtenExam<=90 order by writtenExam ASC
 --15.查询年龄没有写的学生所有信息
 select * from StuInfo where stuAge is null or stuAge=''
 --16.查询年龄写了的学生所有信息
 select * from StuInfo where stuAge is not null and not stuAge=''
--17.查询姓张的学生信息18.查询学生地址中有‘湖’字的信息
 select * from StuInfo where stuName like '张%' and stuAddress like '湖%'
--19.查询姓张但名为一个字的学生信息
 select * from StuInfo where stuName like '张_'
--20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
 select * from StuInfo where stuName like '%俊_'
--21.按学生的年龄降序显示所有学生信息
 select * from StuInfo order by stuAge DESC
--22.按学生的年龄降序和座位号升序来显示所有学生的信息
 select * from StuInfo order by stuAge ASC
--23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
 select top 1 * from StuExam order by writtenExam DESC
--24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
 select top 1 * from StuExam order by labExam ASC

