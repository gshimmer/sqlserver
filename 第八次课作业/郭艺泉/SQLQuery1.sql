/**
*	作业3
*	之前的作业，因为要套用
**/
use master
go
create database bbs
on primary
(
	name = 'bbs',
	filename = 'D:\Document\MSSQLDatabase\bbs\bbs.mdf',--没有E盘，就丢这了
	size = 5MB,
	Maxsize = 50MB,
	filegrowth = 1MB
)
log on
(
	name = 'bbs_log',
	filename = 'D:\Document\MSSQLDatabase\bbs\bbs_log.ldf',--没有E盘，就丢这了
	size = 1MB,
	Maxsize = 10MB,
	filegrowth = 10%
)
go

use bbs
go
create table bbsUsers
(
	UID int identity(1,1),
	uName varchar(10),
	uSex varchar(2),
	uAge int,
	uPoint int
)
goto altBbsUsers
bbsTopic:
	create table bbsTopic
	(
		tID int primary key identity(1,1),
		tUID int constraint FK_bbsTopic_UID references bbsUsers(UID),
		tSID int constraint FK_bbsSection_sID references bbsSection(sID),
		tTitle varchar(100) not null,
		tMsg text not null,
		tTime datetime,
		tCount int
	)
	goto bbsReply
bbsReply:
	create table bbsReply
	(
		rID int primary key identity(1,1),
		rUID int constraint FK_bbsReply_UID foreign key references bbsUsers(UID),
		rTID int constraint FK_bbsSection_tID references bbsTopic(tID),
		rMsg text,
		rTime datetime
	)
	goto endCre
bbsSection:
	create table bbsSection
	(
		sID int identity(1,1),
		sName varchar(10),
		sUid int
	)
	goto altBbsSection

altBbsUsers:
	alter table bbsUsers add constraint PK_UID primary key(UID)
	alter table bbsUsers add constraint UQ_uName unique(uName)
	alter table bbsUsers alter column uSex varchar(2) not null
	alter table bbsUsers add constraint CK_uSex check(uSex = '男' or uSex = '女')
	alter table bbsUsers add constraint DF_uSex default('男') for uSex
	alter table bbsUsers add constraint CK_uAge check(uAge between 15 and 60)
	alter table bbsUsers add constraint CK_uPoint check(uPoint >= 0)
	goto bbsSection
altBbsSection:
	alter table bbsSection add constraint PK_sID primary key(sID)
	alter table bbsSection alter column sName varchar(10) not null
	alter table bbsUsers add constraint FK_bbsUsers_UID  foreign key(UID) references bbsUsers(UID)
	goto bbsTopic

endCre:
	goto insTable

insTable:
	use bbs
	insert into bbsUsers(uName,uSex,uAge,uPoint) values ('小雨点','女',20,0),
	('逍遥','男',18,4),('七年级生','男',19,2)--小雨点 1 逍遥 2 ， 七年生 3
	goto buBbsPoint
buBbsPoint:
	select uName,uPoint into bbsPoint from bbsUsers
	goto startSection
startSection:
	insert into bbsSection(sName,sUid) values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
	goto addReply
addReply:
	use bbs
	insert into bbsTopic(tUID,tSID,tTitle,tMSG,tTime,tCount) values (2,4,'范跑跑','谁是范跑跑','2008-07-08',1),
	(3,1,'.NET','与JAVA的区别是什么啊','2008-09-01',2),
	(2,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-09-10',0)
	insert into bbsReply(rUID,rTID,rMsg,rTime) values (1,1,'是傻逼',getdate()),
	(2,2,'平台和语言的区别',getdate())
	goto delUser
delUser:
	alter table bbsReply drop constraint FK_bbsReply_UID
	alter table bbsTopic drop constraint FK_bbsTopic_UID
	delete from bbsUsers where uID = 2
	goto addPoint
addPoint:
	insert into bbsPoint values ('小雨点',10)
	update bbsUsers set uPoint += 10 where uName = '小雨点'
	goto delSections
delSections:
	alter table bbsTopic drop constraint FK_bbsSection_sID
	delete from bbsSection where sID = 3
	goto cleReply
cleReply:
	delete from bbsReply
	goto sel
sel:
	select * from bbsPoint
	select * from bbsReply
	select * from bbsSection
	select * from bbsTopic
	select * from bbsUsers
	go
/**
*	后面的就是这次作业的作业3
**/

select tSID,count(*) from bbsTopic group by tSID--在主贴表中统计每个版块的发帖总数

select rTID,COUNT(rID)回复数量 from bbsReply group by rTID--在回帖表中统计每个主贴的回帖总数量

select tUID,COUNT(tID)发帖数量 from bbsTopic group by tUID--在主贴表中统计每个用户的发的主帖的总数

select tUID,sum(tCount) from bbsTopic group by tUID--在主贴表中统计每个用户发的主贴的回复数量总和

select tSID,tCount from bbsTopic group by tSID,tCount having AVG(tCount) > 3--在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量

select uName,uSex,uAge,uPoint from bbsUsers where uPoint = (select max(uPoint) from bbsUsers)--在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分

select * from bbsTopic where tTitle like '%快乐%' or tMsg like '%快乐%'--在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来

--在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
/*where*/select * from bbsUsers where (uAge between 15 and 20) and uPoint > 10
/*子查询*/select * from bbsUsers where uPoint in (select uPoint from bbsUsers where uPoint > 10 and uAge in (select uAge from bbsUsers where uAge between 15 and 20)) 
--在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select * from bbsUsers where uName like '小_大%'
--在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
select tTitle 标题,tMsg 内容 from bbsTopic where DATEDIFF(HH,'2008-9-10 12:00:00',tTime) < 0 and tCount > 10
--在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select tUID,tCount from bbsTopic where tTitle like '%!'

