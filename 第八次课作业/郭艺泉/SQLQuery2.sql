/**
*	作业2
**/

use master
go

create database store
go

use store
go

create table orders
(
	orderID int primary key identity(1,1),
	orderDate datetime
)
go

create table orderItem
(
	itemID int primary key identity,
	orderID int references orders(orderID),
	itemType nchar(5),
	itemName nchar(20),
	theNumber int,
	theMoney int
)

insert into orders values ('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-3-10')
go
insert into orderItem values (1,'文具','笔',72,2),(1,'文具','尺',10,1),(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),(2,'文具','固体胶',20,3),(2,'日常用品','透明胶',2,1),
(2,'体育用品','羽毛球',20,3),(3,'文具','订书机',20,3),(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),(4,'文具','笔',20,20),(4,'文具','信纸',50,1),(4,'日常用品','毛巾',4,5),
(4,'日常用品','透明胶',30,1),(4,'体育用品','羽毛球',20,3)
go

use store
go
--查询所有订单订购的所有物品数量总和
select SUM(theNumber) 所有订单物品数量和 from orderItem
--查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select SUM(theNumber) 物品数量, AVG(theMoney) 平均单价 from orderItem where orderID < 3 and theMoney < 10
--查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select COUNT(theNumber) 物品数量,AVG(theMoney) 平均单价 from orderItem where theMoney < 10 and theNumber > 50
--查询每种类别的产品分别订购了几次
select itemName,COUNT(theNumber) 订购次数 from orderItem group by itemName
--查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType,SUM(theNumber) 订购总数,AVG(theMoney) 平均单价 from orderItem group by itemType having SUM(theNumber) > 100
--查询每种产品的订购次数，订购总数量和订购的平均单价
select itemName,COUNT(itemName) 订购次数,SUM(theNumber) 订购总数,theMoney 平均单价 from orderItem group by itemName,theMoney