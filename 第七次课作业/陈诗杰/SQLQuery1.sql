use master 
go

create database ATM
on primary
(
	name = ATM,
	filename = 'D:\bank\ATM.mdf',
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 15%
)
log on
(
	name = ATM_log,
	filename = 'D:\bank\ATM_log.ldf',
	size = 1MB,
	maxsize = 10MB,
	filegrowth = 10%
)
go

use ATM
go

create table userInfo
(
	customerID int identity(1,1) primary key,
	customerName nvarchar(5) not null,
	PID char(18) check(len(PID) in(18,15)) unique(PID) not null,
	telephone char(13) check(telephone like ('____-_______') or len(telephone) = 13) not null,
	address nvarchar(50)
)
go

create table cardInfo
(
	cardID char(18) check(len(cardID) = 18 and cardID like ('1010 3576 ____ ___')) primary key,
	--cardID char(18) check(substring(cardID,1,9) = '1010 3576' and len(cardID) = 18)
	curType char(3) default('RMB') not null,
	savingType nvarchar(4) check(savingType in('活期','定期','定活两便')),
	openDate date default(getdate()) not null,
	balance money not null,
	pass int check(len(pass) = 6) default(888888) not null,
	isReportLoss nchar(1) check(isReportLoss in('是','否')) default('否') not null,
	custromerID int constraint FK_cardInfo_custromerID foreign key references userInfo(customerID) not null
)


go

create table transInfo
(
	transID int identity(1,1) primary key,
	transDate date default(getdate()) not null,
	cardID char(18) constraint FK_transInfo_cardID references cardInfo(cardID) not null,
	transType nchar(2) check(transType in('存入','支取')) not null,
	transMoney money check(transMoney > 0) not null,
	remark ntext
)
go

insert into userInfo(customerName,PID,telephone,address) values ('孙悟空','123456789012345','0716-78989783','背景海淀')
insert into userInfo(customerName,PID,telephone) values ('沙和尚','421345678912345678','0478-44223333'),
('唐僧','321245678912345678','0478-44443333')
insert into cardInfo(cardID,savingType,balance,custromerID) values ('1010 3576 1234 567','活期',1000.00,1),
('1010 3576 1212 117','定期',1.00,2),
('1010 3576 1212 113','定期',1.00,3)

update cardInfo set pass = 611234 where custromerID = 1 --改密码


insert into transInfo(cardID,transType,transMoney) values ((select cardID from cardInfo where custromerID = (select customerID from userInfo where customerName = '孙悟空')),'支取',300)
go
update cardInfo set balance -= 200 where custromerID = (select customerID from userInfo where customerName = '孙悟空')
go

insert into transInfo(cardID,transType,transMoney) values ((select cardID from cardInfo where custromerID = (select customerID from userInfo where customerName = '沙和尚')),'存入',300)
go
update cardInfo set balance += 300 where custromerID = (select customerID from userInfo where customerName = '沙和尚')
go

update cardInfo set isReportLoss = '是' where custromerID = 3
--最近10天开户的信息
select * from cardInfo where DATEDIFF(dd,getdate(),openDate) <= 10
--交易金额最大的银行卡信息
select * from cardInfo where cardID = (select top 1 cardID from transInfo where transMoney = (select top 1 max(transMoney) from transInfo))

select 总交易金额 = sum(transMoney) from transInfo  
select 支取交易金额 = sum(transMoney) from transInfo where transType = '支取'
select 存入交易金额 = sum(transMoney) from transInfo where transType = '存入'






