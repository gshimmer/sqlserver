use master
go

create database ATM
on
(
name='ATM',
filename='D:\bank\ATM.mdf',
size=5mb,
maxsize=50mb,
filegrowth=15mb
)
log on
(
name='ATM_log',
filename='D:\bank\ATM_log.ldf',
size=5mb,
maxsize=50mb,
filegrowth=15mb
)
go
use ATM
go
create table userInfo
(
customerID int identity(1,1) primary key,
customerName varchar(20) not null,
PID int not null check(len(PID)=15 or len(PID)=18) unique,
telephone char(13) not null check(len(telephone)=13 and telephone like '____-________'),
address varchar(20)
)
create table cardInfo
(
cardID char(13) not null primary key check(substring(cardID,1,9)='1010 3576' and len(cardID)=18),
curType varchar(20) default('RMB') not null,
savingType varchar(20) check(savingType='活期' and savingType='定活两便' and savingType='定期'),
openDate datetime not null default(getdate()),
balance money check(balance>=1) not null,
pass int not null default('888888') check(len(pass)=6),
IsReportLoss char(2) check(IsReportLoss='是' or IsReportLoss='否') default('否'),
customerID int foreign key references userInfo(customerID)
)
create table transInfo
(
transId int  identity(1,1) primary key,
transDate datetime default(getdate()) not null,
cardID  char(13) not null foreign key references cardInfo(cardID),
transType varchar(20) check(transType='存入' or transType='支取') not null,
transMoney money check(transMoney>0),
remark text
)

insert into userInfo(customername,PID,telephone,address) values
('孙悟空','123456789012345','0716-78989783','北京海滨')
insert into userInfo(customername,PID,telephone) values
('沙和尚','421345678912345678','0478-44223333')
insert into userInfo(customername,PID,telephone) values
('唐僧','32145678912345678','0478-44443333')

insert into cardInfo(customerID,savingType,cardID) values
(1,'1000','活期','1010 3576 1234 567'),
(2,'1','定期','1010 3576 1212 117'),
(3,'1','定期','1010 3576 1212 113')

update cardInfo set pass='611234' where customerID=1

select * from userInfo
select * from cardInfo 
select * from transInfo 
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','支取',200)
update cardInfo set balance=balance-200 where customerID=1

insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where customerID=2

update cardInfo set IsReportLoss='是' where customerID=3

select * from cardInfo where openDate>='2021-03-09' and openDate<='2021-03-19' 
