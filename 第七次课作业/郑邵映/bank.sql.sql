use master
go
create database bank
on 
(
name='bank',
filename='D:\test\bank.mdf',
size=5,
maxsize=50,
filegrowth=15%
)
log on
(
name='bank_log',
filename='D:\test\bank_log.ldf',
size=5,
maxsize=50,
filegrowth=10%
)
use bank
go
create table userInfo 
(
customerID int primary key identity(1,1),
customerName nvarchar(10) not null,
PID nvarchar(20) not null unique check(len(PID)in (15,18)),
telephone nvarchar(15) not null check(len(telephone)=13 and telephone like '____-________'),
address nvarchar(50)
)
create table cardInfo
(
cardID nvarchar(20) not null check(substring(cardID,1,9)='1010 3576' and len(cardID)=18) primary key,
curType nvarchar(10) not null default ('RMB'),
savingType nvarchar(10) check(savingType in ('活期','定活两便','定期')),
openDate datetime default GETDATE() NOT NULL,
balance money check (balance>=1),
pass char (6) not null default (888888),
IsReportLoss nvarchar(1) not null default ('否') check (IsReportLoss in('是','否')),
customerID int not null references userInfo (customerID)
)
 create table transInfo 
 (
 transId int primary key identity(1,1),
 transDate datetime default GETDATE() not null,
 cardID nvarchar(20) not null references cardInfo(cardID),
 transType nvarchar(4) not null check(transType='存入' or transType='支取'),
 transMoney money not null check (transMoney>0),
 remark nvarchar(50)
 )
-- C. 根据下列条件插入和更新测试数据
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
insert into userInfo(customerName,PID,telephone,address) values('孙悟空','123456789012345','0716-78989783','北京海淀')
--   开户金额：1000 活期   卡号：1010 3576 1234 567
insert into cardInfo (customerID,balance,savingType,cardID) values('1','1000','活期','1010 3576 1234 567')
--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
insert into userInfo (customerName,PID,telephone) values ('沙和尚','421345678912345678','0478-44223333')
--   开户金额： 1  定期 卡号：1010 3576 1212 117
insert into cardInfo (customerID, balance,savingType,cardID) values ('2','1','定期','1010 3576 1212 117')
--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
insert into userInfo (customerName,PID,telephone) values ('唐僧','321245678912345678','0478-44443333')
--   开户金额： 1  定期 卡号：1010 3576 1212 113
insert into cardInfo (customerID, balance,savingType,cardID) values ('3','1','定期','1010 3576 1212 113')

select * from userInfo
select * from cardInfo
select * from transInfo
--第二阶段：增、删、改、查

--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass='611234' where cardID='1010 3576 1234 567'

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1234 567','支取',200.00)
update cardInfo set balance=balance-200 where customerID=1
--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1212 117','存入',300.00)
update cardInfo set balance=balance+300 where customerID=2
update cardInfo set IsReportLoss='是' where customerID=3
--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 113'
--5.	查询出最近10天开户的银行卡的信息

--6.	查询交易金额最大的银行卡信息，子查询实现
select max(balance)最大金额  from cardInfo
select sum(transMoney)总支出金额 from transInfo where transType='支取'
--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
select * from transInfo 
--  显示效果：
--  总交易金额：1400.00
--  支取交易金额：200.00
--  存入交易金额：1200.00
