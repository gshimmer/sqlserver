create database ATMSystem

on

(

	FileName='D:\bank\ATMSystem.mdf',

	Name='ATMSystem',

	size=5MB,

	MAXsize=10MB,

	filegrowth=15%

)

log on

(

	FileName='D:\bank\ATMSystem_log.ldf',

	Name='ATMSystem_log',

	size=5MB,

	MAXsize=10MB,

	filegrowth=15%

)

go



use ATMSystem

go



create table userInfo

(

	customerID int identity(1,1) primary key,

	customerName nvarchar(10) not null,

	PID nvarchar(18) not null check(len(PID)=18 or len(PID)=15),

	telephone nvarchar(15) not null check(telephone like '____-________' and len(telephone)=13),

	address nvarchar(20)

)





create table cardInfo

(

	cardID nvarchar(18) not null primary key check(substring(cardID,1,9)='1010 3576' and len(cardID)=18) ,--1010 3576 xxxx xxx

	curType nvarchar(10) not null default('RMB'),

	savingType nvarchar(10) check(savingType in('活期','定活两便','定期')),

	openDate datetime not null default(getdate()),

	balance int not null check(balance>=1),--?

	pass varchar(6) not null check(len(pass)=6) default('888888'),

	IsReportLoss nvarchar(1) not null default('否'),

	customerID int not null foreign key references userInfo(customerID)

)





create table transInfo

(

	transID int identity(1,1) primary key,

	transDate datetime not null default(getdate()),

	cardID nvarchar(18) not null foreign key references cardInfo(cardID),--?

	transType nvarchar(2) not null check(transType in ('存入','支取')),

	transMoney int not null check(transMoney>0),

	remark text

)



insert into userInfo values

('孙悟空','123456789012345','0716-78989783','北京海淀'),

('沙和尚','421345678912345678','0478-44223333',''),

('唐僧','321245678912345678','0478-44443333','')





insert into cardInfo (cardID,balance,customerID) values

('1010 3576 1234 567',1000,1),

('1010 3576 1212 117',1,2),

('1010 3576 1212 113',1,3)











select * from userInfo



select * from cardInfo

--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”

update cardInfo set pass=611234 where customerID=1

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200

--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额

--select customerID from userInfo where customerName='孙悟空'用户编号

--select cardID from cardInfo where customerID=(select customerID from userInfo where customerName='孙悟空')卡号

update cardInfo set balance=balance-200 where cardID=(select cardID from cardInfo where customerID=(select customerID from userInfo where customerName='孙悟空'))

--用同上题一样的方法实现沙和尚存钱的操作(存300)

--select customerID from userInfo where customerName='沙和尚'

--select cardID form cardInfo where customerID=(select customerID from userInfo where customerName='沙和尚')

update cardInfo set balance=balance+300 where cardID=(select cardID from cardInfo where customerID=(select customerID from userInfo where customerName='沙和尚'))

--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”

update cardInfo set IsReportLoss='是' where customerID=3

--5.查询出最近10天开户的银行卡的信息

select * from  cardInfo where openDate>=dateadd(dd,-10,getdate())

--6.查询交易金额最大的银行卡信息，子查询实现
insert into transInfo (cardID,transType,transMoney) values
('1010 3576 1234 567','支取',200),
('1010 3576 1212 117','存入',300)

--select MAX(transMoney) from transInfo最大金额
--select cardID from transInfo where transMoney=(select MAX(transMoney) from transInfo)最大金额卡号

select * from cardInfo where cardID=(select cardID from transInfo where transMoney=(select MAX(transMoney) from transInfo))

--7.再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)显示效果：
--总交易金额：1400.00
--支取交易金额：200.00
--存入交易金额：1200.00

select sum(transMoney) 总交易金额 from transInfo

select transMoney 支取的交易金额 from transInfo where transType='存入'

select transMoney 存入的交易金额 from transInfo where transType='支取'
