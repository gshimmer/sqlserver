create database ATMsystem
on
(
 name='ATMsystem',
 filename='D:\bank.mdf',
 filegrowth=15%
)
log on
(
 name='ATMsystem_log',
 filename='D:\bank_log.mdf',
 filegrowth=15%
)
go
use ATMsystem
go

create table userInfo 
(
	customerID int identity(1,1) primary key,
	customerName varchar(32) not null,
	PID varchar(18) unique check(len(PID)=18 or len(PID)=15),
	telephone char(13) check(telephone like '____-________' or len(telephone)=13),
	address varchar(200)
)

create table cardInfo
(
	cardID char(18) check(cardID like '1010 3576 ____ ___') primary key,
	curType varchar(30) not null default('RMB'),
	savingType nvarchar(4) check(savingType in('活期','定活两便','定期')) not null,
	openDate date default(getdate()),
	balance money check(balance>0) not null,
	pass char(6) not null default('888888'),
	IsReportLoss char(2) check(IsReportLoss in('是','否')) default('否') not null,
	customerID int references userInfo(customerID)
)
create table transInfo 
(
	transId int identity(1,1) primary key,
	transDate datetime default(getdate()) not null,
	cardID char(18) check(cardID like '1010 3576 ____ ___')
	references cardInfo(cardID) not null,
	transType nvarchar(2) check(transType in('存入','支取')) not null,
	transMoney money check(transMoney>0),
	remark varchar(200)
)
insert into userInfo values ('孙悟空','123456789012345','0716-78989783','北京海淀')
insert into cardInfo(customerID,balance,savingType,cardID) values(1,1000,'活期','1010 3576 1234 567')
select * from userInfo
select * from cardInfo 
insert into userInfo values ('沙和尚','421345678912345678','0478-44223333','')
insert into cardInfo(customerID,balance,savingType,cardID) values(2,1,'定期','1010 3576 1212 117')
select * from userInfo
select * from cardInfo 
insert into userInfo values ('唐僧','321245678912345678','0478-44443333','')
insert into cardInfo(customerID,balance,savingType,cardID) values(3,1,'定期','1010 3576 1212 113')
select * from userInfo
select * from cardInfo 

update cardInfo set pass='611234' where customerID=1

select * from userInfo
select * from cardInfo 
select * from transInfo 
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','支取',200)
update cardInfo set balance=balance-200 where customerID=1
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where customerID=2

update cardInfo set IsReportLoss='是' where customerID=3

select * from cardInfo where openDate>='2021-03-09' and openDate<='2021-03-19'

select * from cardInfo where cardID=(select cardID from transInfo where transMoney=(select max(transMoney) from transInfo))

select sum(transMoney) from transInfo
select sum(transMoney) from transInfo where transType='存入'
select sum(transMoney) from transInfo where transType='支取'
