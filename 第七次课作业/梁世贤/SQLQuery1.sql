use master
go

create database ATM
on primary
(
name='ATM',
filename='D:\SQL作业，mdf',
size=5MB,
filegrowth=1MB,
maxsize=5MB
)
log on
(
name='ATM_log',
filename='D:\SQL作业_log.ldf',
size=5MB,
filegrowth=1MB,
maxsize=5MB
)
go

use ATM

create table userInfo
( --建立用户信息表
customerID  int identity primary key,
customerName varchar(10) not null,
PID varchar(18) check(len(PID)=18 or len(PID)=15) unique,
telephone varchar(13) not null check(len(telephone)=13 or telephone like'____-________'),
address varchar(30) 
)
go

create table cardInfo --建立银行卡信息表
(
cardID varchar(18) primary key check(len(cardID)=18 or cardID like'1010 3576 ____ ___'),
curType varchar(3) check(curType='RMB'),
savingType varchar(8) check(savingType='活期'or savingType='定活两便' or savingType='定期'),
openDate date default getdate(),
balance varchar(25) not null check(balance>=1),
pass int not null check(len(pass)=6) default('888888'),
IsReportLoss  varchar(2)  check(IsReportLoss='是' or IsReportLoss='否') default('否'),
customerID int  not null foreign key(customerID) references userInfo(customerID)
)
go

create table transInfo  --建立交易信息表
(
transId int identity primary key,
transDate date default getdate() not null,
cardID  varchar(18)  foreign key(cardID) references cardInfo(cardID),
transType varchar(4) check(transType='存入' or transType='支取') not null,
transMoney int check(transMoney>0) not null,
remark varchar(50)
)
go

--孙悟空开户,身份证:123456789012345,电话:0716-78989783,地址北京海淀 开户金额1000  活期 卡号1010 3576 1234 567 
select * from userInfo
select * from cardInfo

insert into userInfo(customerName,PID,telephone,address) values ('孙悟空','123456789012345','0716-78989783','北京海淀')
insert into cardInfo(balance,savingType,cardID,customerID) values ('1000','活期','1010 3576 1234 567',1)

--沙和尚开户，身份证:421345678912345678，电话：0478-44223333，开户金额1 定期 卡号：1010 3576 1212 117

insert into userInfo(customerName,PID,telephone) values ('沙和尚','421345678912345678','0478-44223333')
insert into cardInfo(balance,savingType,cardID,customerID) values ('1','定期','1010 3576 1212 117',2)

--唐僧开户，身份证:321245678912345678，电话：0478-44443333,开户金额1  定期 卡号1010 3576 1212 113

insert into userInfo(customerName,PID,telephone) values ('唐僧','321245678912345678','0478-44443333')
insert into cardInfo(balance,savingType,cardID,customerID) values ('1','定期','1010 3576 1212 113',3)

--1.将用户“孙悟空”开卡时的初始密码更改为“611234”

update cardInfo set pass=611234 where customerID=1
select * from cardInfo

--2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额

select * from transInfo
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1234 567','支取','200')
update cardInfo set balance=balance-200 where customerID=1

--3.用同上题一样的方法实现沙和尚存钱的操作(存300)

insert into transInfo(cardID,transType,transMoney) values('1010 3576 1212 117','存入','300')
update cardInfo set balance=balance+300 where customerID=2

--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”

update cardInfo set IsReportLoss='是' where customerID=3

--5.查询出最近10天开户的银行卡的信息

select count(*) from cardInfo where date_sub(curdate(),interval 10 day)<=date(openDate)

--6.查询交易金额最大的银行卡信息

select max(transMoney)最大交易金额 from transInfo

--7.再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
--显示效果：
--总交易金额：1400.00

