use master

go
create database ATM
on
(
 name='ATM',
 filename='D:\DATA\ATM.mdf',
 size=5MB,
 maxsize=50MB,
 filegrowth=15%
)
log on
(
name='ATM_log',
 filename='D:\DATA\ATM_log.ldf',
 size=5MB,
 maxsize=50MB,
 filegrowth=15%
)

go
use ATM

go

create table userInfo
(
 customerID int primary key identity(1,1),
 customerName nvarchar(10) not null,
 PID char(18) check(len(PID) in(18,15)) unique(PID) not null,
 telephone varchar(13) check(len(telephone)=13 and telephone like '____-________') not null,
 address nvarchar(200)
)
go
use ATM

go

create table cardInfo
(
 cardID varchar(20) primary key check(substring(cardID,1,9)='1010 3576' and len(cardID)=18) not null,
 curType nvarchar(10) default('人民币') not null,
 savingType varchar(10) check(savingType in('活期','定活两期','定期')),
 openDate date default(getdate()) not null,
 balance money not null,
 pass int check(len(pass)=6) default(888888) not null,
 IsReportLoss nvarchar(2) check(IsReportLoss='是' and IsReportLoss='否') default('否'),
 customerID int constraint FK_cardInfo_custromerID foreign key references userInfo(customerID)
)
go
use ATM

go

create table transInfo
(
 transId int primary key identity(1,1),
 transDate date default(getdate()),
 cardID varchar(20) references cardInfo(cardID) not null,
 transType nvarchar(2) check(transType='存入' or transType='支取') not null,
 transMoney money check(transMoney>0) not null,
 remark text
)



--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567
insert into userInfo values('孙悟空','123456789012345','0716-78998783','北京海滨')
insert into cardInfo(balance,savingType,cardID) values(1000,'活期','1010 3576 1234 567')

--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：1010 3576 1212 117
insert into userInfo values('沙和尚','421345678912345678','0478-44223333','四川成都')
insert into cardInfo(balance,savingType,cardID) values(1,'定期','1010 3576 1212 117')

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：1010 3576 1212 113
insert into userInfo values('唐僧','321245678912345678','0478-44443333','陕西西安')
insert into cardInfo(balance,savingType,cardID) values(1,'定期','1010 3576 1212 113')

--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”

update cardInfo set pass='611234' where cardID='1010 3576 1212 113'

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额

insert into transInfo(cardID,transType,transMoney) values('1010 3576 1234 567','支取',200)
update cardInfo set balance -= 200 where cardID = 1

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1212 117','存入',300)
update cardInfo set balance +=300 where cardID = 2
--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where cardID = 3
--5.	查询出最近10天开户的银行卡的信息
select * from cardInfo where DateDiff(dd,openDate,getdate())<=10
--6.	查询交易金额最大的银行卡信息，子查询实现
select max(transMoney) from transInfo
--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
--  显示效果：
--  总交易金额：1400.00
--  支取交易金额：200.00
--  存入交易金额：1200.00
select sum(transMoney) from transInfo
select transType from transInfo where transId=1
select transType from transInfo where transId=2

select*from userInfo
select*from cardInfo
select*from transInfo 