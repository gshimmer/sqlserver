create database ATM
go
use ATM
go


--用户信息表
create table userInfo
(
 customerID int identity,
 customerName char(10) not null,
 PID varchar(18) not null check(len(PID)=15 or len(PID)=18),
 telephone char(13) not null check(len(telephone)=13 and telephone like'____-________'),
 address text ,
)
--添加主键
alter table userInfo add constraint PK_userInfo_customerID primary key(customerID)
--唯一约束
alter table userInfo add constraint UK_userInfo_PID unique(PID)

--银行卡信息表
create table cardInfo
(
 cardID varchar(20) not null,
 curType varchar(10) not null default('RMB'),
 savingType varchar(10) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
 openDate datetime not null default(Getdate()),
 balance int not null check(balance>=1),
 pass int not null default('888888') check(len(pass)=6),
 IsReportLoss char(4) not null default('否') check(IsReportLoss='是' or IsReportLoss='否'),
 customerID int  not null,
)
--添加主键
alter table cardInfo add constraint PK_cardInfo_cardID primary key(cardID)
--添加外键约束关联用户信息表的顾客编号
alter table cardInfo add constraint FK_cardInfo_customerID foreign key(customerID) references userInfo(customerID)

--交易信息表

create table transInfo 
(
 transId int identity,
 transDate time not null default('2021-3-19'),
 cardID varchar(18) not null,
 transType char(4) not null check(transType='存入'  transType='支取'),
 transMoney int not null check(transMoney>0),
 remark text ,
)
--插入外键与银行卡信息表卡号关联
alter table transInfo add constraint FK_transInfo_cardID foreign key(cardID) references cardInfo(cardID)

--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--开户金额：1000 活期   卡号：1010 3576 1234 567
insert into userInfo(customerName,PID,telephone,address) values ('孙悟空','123456789012345','0716-78989783','北京海淀')
insert into cardInfo(balance,savingType,cardID,customerID) values(1000,'活期','1010 3576 1234 567',1)

--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--开户金额： 1  定期 卡号：1010 3576 1212 117
insert into userInfo(customerName,PID,telephone) values('沙和尚','421345678912345678','0478-44223333')
insert into cardInfo(balance,savingType,cardID,customerID) values(1,'定期','1010 3576 1212 117',2)

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--开户金额： 1  定期 卡号：1010 3576 1212 113
insert into  userInfo(customerName,PID,telephone) values('唐僧','321245678912345678','0478-44443333')
insert into cardInfo(balance,savingType,cardID,customerID) values(1,'定期','1010 3576 1212 113',3)
select * from cardInfo 

--1.将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass='611234' where customerID=1

--2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','支取',200)
update cardInfo set balance=balance-200 where customerID=1

--3.用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance-200 where customerID=2

--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where customerID=3

--5.查询出最近10天开户的银行卡的信息
select top 10 *  from cardInfo

--6.查询交易金额最大的银行卡信息，子查询实现
select max(transMoney)交易金额最大 from transInfo 