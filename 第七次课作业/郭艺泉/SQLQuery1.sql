use master
go
create database bank
on
(
name='bank',
filename='D:\ljcbank\bank.mdf',
size=5,
maxsize=20,
filegrowth=15%
)
log on
(
name='bank_log',
filename='D:\ljcbank\bank_log.ldf',
size=5,
maxsize=20,
filegrowth=15%
)
go
use bank
go
create table userInfo
(
customerID int primary key identity(1,1),
customerName text not null,
PID int check(PID>=15 or PID<=18) unique not null,
telephone char(13) not null check(len(telephone)=13 and telephone like'____-________'),
address text
)
create table cardInfo
(
cardID int primary key not null check(cardID like'1010 3576 ____ ___'),
curType monEY not null default('RMB'),
savingType NVARCHAR(20) check(savingType='定活两便' or savingType='定期' or savingType='活期'),
openDate DATETIME not null default (getdate()),
balance int not null check(balance>=1),
pass int not null check(len(pass)=6) default('888888'),
IsReportLoss nchar(1) not null check(IsReportloss='是' or IsReportloss='否') default('否'),
customerID int not null references userInfo(customerID)
)
create table transInfo
(
transId int primary key identity(1,1),
transDate datetime not null default(getdate()),
cardID nvarchar(20),
transType nchar(2) not null check(transType='存入' or transType='支取'),
transMoney int not null check(transMoney>0),
remark text
)
insert into userInfo
select '孙悟空','123456789012345','0716-78989783','北京海淀' union
select '沙和尚','421345678912345678','0478-44223333',''union
select '唐僧','321245678912345678','0478-44443333',''

select * from userInfo
insert into cardInfo(cardID,savingType,balance,customerID)
select '1010 3576 1234 567','活期','1000','1' union
select '1010 3576 1212 117','定期','1','2' union
select '1010 3576 1212 113','定期','1','3' 
select * from cardInfo

--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”

update cardInfo set pass=611234 where savingType='活期'

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额

select customerID from userInfo where customerName='孙悟空'
select cardID from cardInfo where customerID=2
insert into transInfo(transDate,cardID,transType,transMoney)
SELECT getdate(),'1010 3576 1234 567','存入','200'
update transInfo set transMoney=1 where cardID ='1010 3576 1234 567'
select * from transInfo

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)

select customerID from userInfo where customerName='沙和尚'
select cardID from cardInfo where customerID=1
insert into transInfo(transDate,cardID,transType,transMoney)
select getdate(),'1010 3576 1212 117','存入','200'
update transInfo set  transMoney=1 where cardID='1010 3576 1212 117' 

--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”

update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 113'

--5.	查询出最近10天开户的银行卡的信息

select * from cardInfo where DateDiff(dd,openDate,getdate())<=10
