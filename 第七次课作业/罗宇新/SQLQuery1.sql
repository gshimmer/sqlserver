use master
go

create database ATM
on
(
	name='bank',
	filename='D:\bank\bank.mdf',
	size=10MB,
	Maxsize=50mb,
	filegrowth=15%
)
log on
(
	name='bank_log',
	filename='D:\bank\bank+log.ldf',
	size=10MB,
	Maxsize=50mb,
	filegrowth=15%
)
go

use ATM
go

create table userInfo
(
	customerID int primary key identity,
	customerName nvarchar(5) not null,
	PID varchar(18) check(len(PID)>=15) unique,
	telephone varchar(13) check(len(telephone)=13 and telephone like '____ ________') not null,
	address varchar(20)
)

create table cardInfo
(
	cardID varchar(20) primary key check(len(cardID)=18 and substring(cardID,1,9)='1010 3576'),
	curType varchar(3) not null default('RMB'),
	savingType nvarchar(10) check(savingType='活期' or savingType='定期' or savingType='定活两变'),
	openDate datetime default(getdate()) not null,
	balance money check(balance>=1) not null,
	pass varchar(6) default('888888') not null,
	IsReportLoss varchar(2) default('否') check(IsReportLoss='是' or IsReportLoss='否') not null,
	customerID int foreign key references userInfo(customerID)
)
create table transInfo
(
	transId int primary key identity,
	transDate datetime not null default(getdate()),
	cardID varchar(20) not null references cardInfo(cardID),
	transType nvarchar(4) not null check(transType='存入' or transType='取出'),
	transMoney money check(transMoney>0) not null,
	remark nvarchar(50)
)
  insert into userInfo values('孙悟空','123456789012345','0716 78989783','北京海淀'),
  ('沙和尚','421345678912345678','0478 44223333',''),
  ('唐僧','321245678912345678','0478 44443333','')

insert into cardInfo (cardID,savingType,balance) values('1010 3576 1234 567','活期','1000'),
('1010 3576 1212 117','定期','1'),
('1010 3576 1212 113','定期','1')

update cardInfo set pass=611234 where cardID='1010 3576 1234 567'

update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'
insert into transInfo values(getdate(),'1010 3576 1234 567','取出','200',null)

update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'
insert into transInfo values(getdate(),'1010 3576 1212 117','存入','300',null)

update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 113'


select * from cardInfo where DateDiff(dd,openDate,getdate())<=10