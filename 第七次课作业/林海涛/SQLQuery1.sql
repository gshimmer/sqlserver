use master 
go 

create database atm1
on 
(
	name = 'atm',
	filename = 'D:\bank\atm.mdf',
	filegrowth = 15%
)

log on
(
	name = 'atm_log',
	filename = 'D:\bank\atm_log.ldf'
)

use atm
go

create table userInfo
(
	customerID int identity primary key,
	customerName nvarchar(10) not null,
	PID varchar(18) not null unique check(len(PID)=18 or len(PID)=15),
	telephone varchar(20) not null check(len(telephone)=13 or telephone like '____-________'),
	address text
)

create table cardInfo
(
	cardID varchar(20) not null primary key check(cardID like '1010 3576 ____ ____'),
	curType varchar(10) not null default('RMB'),
	savingType nvarchar(4) check(savingType in ('活期','定活两便','定期')),
	openDate datetime not null default(getdate()),
	balance varchar(13) not null check (balance>1),
	pass int not null check (len(pass)=6) default(888888),
	IsReportLoss nvarchar(1) not null default('否') check (IsReportLoss in('是','否')),
	customerID int references userInfo(customerID) not null
)

create table transInfo
(
	transId int identity primary key,
	transDate datetime not null default(getdate()),
	cardID varchar(20) not null references cardInfo(cardID),
	transType nvarchar(2) check (transType in ('存入','支取')),
	transMoney varchar(15) not null check(transMoney>=1),
	remark text 
)

insert userInfo values 
('孙悟空','123456789012345','0716-78989783','北京海淀'),
('沙和尚','421345678912345678','0478-44223333',''),
('唐僧','321245678912345678','0478-44443333','')

select * from userInfo

insert into  cardInfo values
('1010 3576 1234 5671','RMB','活期','','1000','888888','否',1),
('1010 3576 1212 1127','RMB','定期','','2','888888','否',2),
('1010 3576 1212 1137','RMB','定期','','2','888888','否',3)
