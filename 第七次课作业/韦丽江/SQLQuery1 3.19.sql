use master 
go

create database ATM
on(
	name='ATM',
	filename='D:\bank\ATM.mdf',
	size=8mb,
	maxsize=80mb,
	filegrowth=15%
)

log on(
	name='ATM_log',
	filename='D:\bank\ATM_log.ldf',
	size=8mb,
	maxsize=80mb,
	filegrowth=15%
)

go
use ATM
go

create table userInfo 
(
	customerID int primary key identity(1,1),
	customerName varchar(20) not null,
	PID varchar(20) check(len(PID)=15 or len(PID)=18) unique not null,
	telephone varchar(13) not null check (len(telephone)=13 and telephone   like '____-________'),
	address varchar(20) not null
)

create table cardInfo
(
	cardID  varchar(18) primary key not null check(substring(cardID ,1,9)='1010 3576' and len(cardID)=18),
	curType varchar(20) not null default('RMB'),
	savingType char(20),check(savingType='活期' or savingType='定活两便' or savingType='定期'),
	openDate datetime not null default (getdate()),--getedate()当前日期
	balance varchar(60) not null check(balance>=1),
	pass  varchar(60) not null default('888888'),
	IsReportLoss char(20) default('否') check(IsReportLoss='是' or IsReportLoss='否'),
	customerID int references userInfo(customerID) not null,

)
drop table cardInfo
alter table  transInfo drop constraint FK__transInfo__cardI__34C8D9D1
alter table transInfo add constraint fk foreign key(cardID) references cardInfo(cardID)
create table transInfo
(
	transId int primary key identity(1,1),
	transDate datetime default(getdate()) not null,
	cardID varchar(18) not null references cardInfo(cardID) ,
	transType varchar(10) not null check(transType='存入' or transType='支取'),
	transMoney int not null check(transMoney>=0),
	remark text
)

--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567
select*from userInfo
insert into  userInfo  values('孙悟空','123456789012345','0716-78989783','北京海淀')

--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：1010 3576 1212 117
insert into  userInfo(customerName,PID,telephone,address)  values('沙和尚','421345678912345678','0478-44223333',' ')
--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
insert into  userInfo(customerName,PID,telephone,address)  values('唐僧','321245678912345678','0478-44443333',' ')
--   开户金额： 1  定期 卡号：1010 3576 1212 113
select * from cardInfo
insert into cardInfo  values ('1010 3576 1234 567',default,'活期',default,1000,default,default,1)
insert into cardInfo  values ('1010 3576 1212 117',default,'定期',default,1,default,default,2)
insert into cardInfo  values ('1010 3576 1212 113',default,'定期',default,1,default,default,3)






--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update  cardInfo  set pass='611234' where customerID=1

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
select * from transInfo
insert into transInfo(cardID,transType,transMoney)
values ('1010 3576 1234 567','支取','200')
update cardInfo set balance=balance-200 where customerID=1
--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
select * from transInfo
insert into transInfo(cardID,transType,transMoney)
values ('1010 3576 1212 117','存入','300')
update cardInfo set balance=balance+300 where customerID=2

--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where customerID=3
--5.	查询出最近10天开户的银行卡的信息
select top 10 * from  cardInfo where openDate>='2021-3-10'
--6.	查询交易金额最大的银行卡信息，子查询实现
select  top 1 * from cardInfo where balance>1 order by balance DESC
--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
--  显示效果：
--  总交易金额：1400.00
--  支取交易金额：200.00
--  存入交易金额：1200.00
select sum (transMoney)总交易金额 from transInfo
select sum (transMoney)支取交易金额 from transInfo where transType='支取'
select sum (transMoney)存入交易金额 from transInfo where transType='存入'
 No newline at end of file