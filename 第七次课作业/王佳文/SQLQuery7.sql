use master
go 
create database bank
on(
	name='bank',
	filename='C:\bank\bank.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
log on(
	name='bank_log',
	filename='C:\bank\bank_log.ldf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
go
use bank
go
create table userInfo 
(
	customerID int primary key identity (1,1),
	customerName nvarchar(10) not null,
	PID varchar(20) unique check(len(PID)=15 or len(PID)=18) not null,
	telephone varchar(20) check(len(telephone)=13 and telephone like '____-________') not null,
	address nvarchar(50),
)
create table cardInfo
(
	--cardID varchar(20) primary key check(substring(cardID,1,10)='1010 3576 ' and len(cardID)=18 ) not null,
	cardID varchar(20) primary key check(cardID like '1010 3576 ____ ___' ) not null,
	curType varchar(4) default('RMB') not null,
	savingType nvarchar(10) check(savingType in ('活期','定活两便','定期')),
	openDate datetime default(getdate()) not null,
	balance money check(balance>=1) not null,
	pass varchar(6) default('888888') not null,
	IsReportLoss nvarchar(1) default('否') check(IsReportLoss in ('是','否')) not null,
	customerID int references userInfo(customerID) not null,

)
create table transInfo
(
	transId int primary key identity not null,
	transDate datetime default(getdate() ) not null,
	cardID varchar(20) references cardInfo(cardID ) not null,
	transType nvarchar(2) check(transType in ('存入','支出')) not null,
	transMoney money check(transMoney>0) not null,
	remark text,
)
use bank
go
select * from userInfo
select * from cardInfo
select * from transInfo
insert into userInfo values('孙悟空','123456789012345','0716-78989783','北京海淀')
insert into userInfo(customerName,PID,telephone) values('沙和尚','421345678912345678','0478-44223333'),('唐僧','321245678912345678','0478-44443333')
insert into cardInfo(cardID,savingType,balance,customerID) values('1010 3576 1234 567','活期',1000.00,1),('1010 3576 1212 117','定期',1.00,2),('1010 3576 1212 113','定期',1.00,3)
update  cardInfo set pass=611234 where customerID=1
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1234 567','支出',200.00)
update cardInfo set balance=balance-200 where customerID=1
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1212 117','存入',300.00)
update cardInfo set balance=balance+300 where customerID=2
update cardInfo set IsReportLoss='是' where customerID=3
select * from cardInfo where DATEDIFF(dd,openDate, getdate())<=10
select max(balance)最大金额  from cardInfo
select sum(transMoney)总支出金额 from transInfo where transType='支出'