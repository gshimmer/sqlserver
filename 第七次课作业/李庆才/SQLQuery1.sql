use master
go
create database bank
on
(
	name ='bank',
	filename='D:\bank.mdf',
	size=5,
	maxsize=1500,
	filegrowth=15%	
) 
log on
(
	name ='bank_log',
	filename='D:\bank_log.ldf',
	size=5,
	maxsize=1500,
	filegrowth=15%	
)
go
use bank
go
create table userInfo
(
	customerID int identity(1,1) primary key,
	customerName nvarchar(10) not null,
	PID char(18) unique check(len(PID) = 15 or len(PID) = 18),
	telephone char(13) check(len(telephone)=13 or (telephone like '____-________')),
	address nvarchar(80)
)
create table cardInfo
(
    cardID  nvarchar(18) primary key check(len(cardID)=18 and cardID like '1010 3576 %'),
	curType CHAR(3) default('RMB') not null,
	savingType nvarchar(4) check(savingType in('活期','定活两便','定期' )),
	openDate datetime not null default(getdate()),
	balance money not null ,
	pass int check(len(pass)=6) default(888888),
	IsReportLoss varchar(2) not null check(IsReportLoss in('是','否')) default('否'),
	customerID int references userInfo(customerID)
)
create table transInfo
(
	transId int primary key identity,
	transDate datetime not null default(getdate()),
	cardID nvarchar(18) not null references cardInfo(cardID),
	transType nchar(2) not null check(transType in ('存入','支出')),
	transMoney int not null check(transMoney>0),
	remark text
)
insert into userInfo
(customerName,PID,telephone,address)
values
('孙悟空','123456789012345','0716-78989783','北京海淀'),
('沙和尚','421345678912345678','0478-44223333',null),
('唐僧','321245678912345678','0478-44443333',null)
insert into cardInfo
(balance,savingType,cardID,customerID)
values
(1000,'活期','1010 3576 1234 567',1),
(1,'定期','1010 3576 1212 117',2),
(1,'定期','1010 3576 1212 113',3)
select *from userInfo 
select *from transInfo 

update cardInfo set pass ='611234' where customerID='1'


select * from cardInfo 
insert into transInfo(cardID,transType ,transMoney)
select '1010 3576 1234 567','支出',200
update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'

select * from  cardInfo
insert into transInfo (cardID,transType,transMoney)
select '1010 3576 1212 117','存入',300
update cardInfo set balance=balance+300 where cardID='1010 3576 1212 113'

update cardInfo set IsReportLoss='是' where customerID='3'

select * from cardInfo where  openDate>='2021-03-10' or openDate>= '2021-03-20'

select max(transMoney) from transInfo
select*from transInfo where transMoney in(select max(transMoney) from transInfo)

select sum(transMoney) from transInfo
select sum(transMoney) from transInfo where transType='存入'
select sum(transMoney) from transInfo where transType='支出'
