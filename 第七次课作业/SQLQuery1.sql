use master
go
create database bank
on
(
	name=bank,
	filename='D:\SQL\bank.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=bank_log,
	filename='D:\SQL\bank_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=15%
)
use bank
go
create table userInfo2
(
	customerID int primary key identity(1,1),
	customerName nvarchar(15) not null,
	PID varchar(18) not null unique check(len (PID)=15 or len (PID)=18),
	telephone char(13) not null check (len (telephone)=13 and telephone like'____-________'),
	address nvarchar(20),
)
go
create table cardInfo
(
	cardID char(18) primary key   check(cardID like '1010 3576 ____ ___'),
	curType varchar(30) not null default ( 'RMB'),
	savingType nvarchar(4) check(savingType='活期'or savingType='定活两便' or savingType='定期' ),
	openDate datetime not null default(getdate()),
	balance money not null check(balance>=1),
	pass char(6) default (88888888) ,
	IsReportLoss char(2) check(IsReportLoss ='是'or IsReportLoss='否')default('否') ,
	customerID int references userInfo2(customerID),
)
create table transInfo
(
	transId int primary key identity,
	transDate datetime not null default(getdate()), 
	cardID char(18) not null check(cardID like '1010 3576 ____ ___') references cardInfo(cardID),--这里
	transType nvarchar(2) not null check(transType='存入'or transType='支出'),
	transMoney money not null check(transMoney>0),
	remark text,
)
select*from userInfo2
insert into userInfo2(customerName,PID,telephone,address) values('孙悟空开户','123456789012345' ,'0716-78989783','北京海淀'),
('沙和尚开户','421345678912345678' ,'0478-44223333',''),('唐僧开户','321245678912345678' ,'0478-44443333','')
insert into  cardInfo (cardID,savingType,balance,customerID)values('1010 3576 1234 567','活期',1000,1),('1010 3576 1212 117','定期',1,2)
,('1010 3576 1212 113','定期',1,3)
select *from cardInfo
--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass='611234' where customerID=1
--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1234 567','支出',200)
update cardInfo set balance=balance-200 where customerID=1

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where customerID=2
--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是'where customerID=3
--5.	查询出最近10天开户的银行卡的信息
select*from cardInfo where openDate>='2021-3-20'and openDate<=2021-3-30
--6.	查询交易金额最大的银行卡信息，子查询实现
select max (transMoney) from transInfo

--7.再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
--  显示效果：
--  总交易金额：1400.00
--  支取交易金额：200.00
--  存入交易金额：1200.00
select sum (transMoney) from transInfo
select sum (transMoney) from transInfo where transType='支出'
select sum (transMoney) from transInfo where transType='存入'