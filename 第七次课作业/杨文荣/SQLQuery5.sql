use master
go

create database ATM
on
(	
	name='ATM',
	filename='D:\bank\ATM.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=15%
)
log on
(	
	name='ATM_log',
	filename='D:\bank\ATM_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=15%
)
go
use ATM
go

create table userInfo
(
	customerID int identity(1,1) primary key,
	customerName nvarchar(10) not null,
	PID varchar(18) check(len(PID)=18 or len(PID)=15)not null,
	telephone char(13) not null check(len(telephone)=13 and telephone like '____-________'),
	address nvarchar(20)
)
create table cardInfo
(
	cardID varchar(20) primary key check(cardID like '1010 3576 ____ ___') ,
	curType nvarchar(10) not null default('RMB'),
	savingType nvarchar(4) check(savingType='活期' or savingType='定活两便'or savingType='定期' ),
	openDate datetime default(getdate()),
	balance decimal(38,2) not null check(balance>=1),
	pass char(6) not null default(888888),
	IsReportLoss varchar(2) default('否') check(IsReportLoss='是' or IsReportLoss='否' ),
	customerID int foreign key references userInfo(customerID)
)
create table transInfo
(
	transID int primary key identity(1,1),
	transDate datetime not null default(getdate()),
	cardID varchar(20) foreign key references cardInfo(cardID) not null,
	transType nvarchar(2) check(transType='存入' or transType='支取') not null,
	transMoney decimal(38,2) not null check(transMoney>0),
	remark text 
)

--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567
--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：1010 3576 1212 117

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：1010 3576 1212 113

 insert into userInfo
 select '孙悟空','123456789012345','0716-78989783','北京海淀' union
 select'沙和尚','421345678912345678','0478-44223333',null union
 select'唐僧','321245678912345678','0478-44443333',null

 insert into cardInfo values
 ('1010 3576 1234 567',default,'活期',default,1000,default,default,1),
 ('1010 3576 1212 117',default,'定期',default,1,default,default,2),
 ('1010 3576 1212 113',default,'定期',default,1,default,default,3)

 update cardInfo set pass='611234' where cardID='1010 3576 1234 567'

------ 2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，
------然后在孙悟空账上的余额减200
------注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，
----再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo values
(default,'1010 3576 1234 567','支取',200,null)

update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'

insert into transInfo values
(default,'1010 3576 1212 117','存入',300,null)

update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'

update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 113'

select * from cardInfo where DateDiff(dd,openDate,getdate())<=10

select max(transMoney) 交易金额最大 from transInfo 

select sum(transMoney) 总交易金额 from transInfo

select sum(transMoney) 支取交易金额 from transInfo where transType='支取'
select sum(transMoney) 存入交易金额 from transInfo where transType='存入'



 