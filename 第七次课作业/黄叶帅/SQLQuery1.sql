create database ATM
on
(
	name='ATM',
	filename='D:\bank\ATM.mdf',
	size=10mb,
	filegrowth=15mb
)
log on
(
	name='ATM_log',
	filename='D:\bank\ATM_log.ldf',
	size=10mb,
	filegrowth=15mb
)
use ATM
go
create table userInfo
(
	customerID int identity(1,1) primary key,
	customerName nchar(10) not null,
	PID CHAR(18)  check(len(PID) =15 or len(PID)=18) not null unique,
	telephone char(13) not null check(len(telephone)=13 and telephone like'____-________'),
	address nchar(20)

)
create table cardInfo
(
	cardID  varchar(40) primary key not null check(cardID like'1010 3576 ____ ___')  ,
	curType varchar(10) not null default('RMB'),
	savingType varchar(10),check(savingType in('活期','定期','定活两期')),
	openDate datetime not null  default(getdate()) ,
	balance int not null check(balance>=1),
	pass varchar(10) check(len(pass)=6) default('888888') not null,
	IsReportLoss char(2) not null check(IsReportLoss in('是','否')) default('否'),
	customerID int  references userInfo(customerID),
)

create table transInfo
(
	transId int primary key identity,
	transDate date not null default(getdate()) ,
	cardID nchar(20) not null,
	transType varchar(4) not null check(transType in('存入','支出')),
	transMoney int not null check(transMoney>0) ,
	remark text

)
--C. 根据下列条件插入和更新测试数据
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
  -- 开户金额：1000 活期   卡号：1010 3576 1234 567
  insert into userInfo values ('孙悟空','123456789012345','0716-78989783','北京海淀')
  insert into cardInfo(balance,savingType,cardID) values( 1000,'活期','1010 3576 1234 567')
  select * from cardInfo
--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
  -- 开户金额： 1  定期 卡号：1010 3576 1212 117
   insert into userInfo values ('沙和尚','421345678912345678','0478-44223333',' ')
   insert into cardInfo (balance,savingType,cardID)values(1,' 活期','1010 3576 1234 567')
--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
 --  开户金额： 1  定期 卡号：1010 3576 1212 113
 insert into userInfo values('唐僧','321245678912345678','0478-44443333','')
 insert into cardInfo(balance,savingType,cardID) values(1 ,'定期','1010 3576 1212 113')

 select * from userInfo

--第二阶段：增alter、删drop、改update、查select

--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
select * from cardInfo
update cardInfo set pass='611234' where cardID='1010 3576 1234 567'

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','支出',200)
select *  from transInfo 
update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'
select *  from  cardInfo
--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','存入',300)
select *  from transInfo 
--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update  cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 113'
select *  from  cardInfo
--5.	查询出最近10天开户的银行卡的信息

--6.	查询交易金额最大的银行卡信息，子查询实现

select cardID ,sum(balance) as balance from cardInfo
group by cardID

--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示
 -- 显示效果：
 -- 总交易金额：1400.00
