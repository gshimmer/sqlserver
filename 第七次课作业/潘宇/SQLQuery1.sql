use master
go
create database ATM
on
(
name='bank',
size=5,
filename='D:\bank.mdf',
Maxsize=100,
filegrowth=1
)
log on
(
name='bank_log',
size=5,
filename='D:\bank_log.ldf',
Maxsize=100,
filegrowth=1
)
go
use ATM
go
create table userInfol
(
customerID int identity(1,1) primary key,
customerName nvarchar(5) not null,
PID varchar(18) not null check(len(PID)=15 or len(PID)=18) unique,
telephone varchar(20) not null check(len(telephone)=13 and telephone like'____-________'),
address text 
)
go
create table cardInfo
(
cardID varchar(20) not null primary key check(substring(cardID,1,9)='1010 3576'),
curType varchar(20) not null default('RMB'),
savingType varchar(20),
openDate datetime  default(getdate()) not null,
balance int not null check(balance>1 ),
pass varchar(20) not null check(len(pass)=6) default('888888'),
IsReportLoss varchar(4) not null check(IsReportLoss='是' or IsReportLoss='否') default('否'),
customerID int foreign key references userInfol(customerID)
)
go
create table transInfo 
(
transId int primary key identity(1,1),
transDate datetime default(getdate()) not null,
cardID varchar(20) not null foreign key references cardInfo(cardID),
transType  varchar(4) not null check(transType='存入' or transType ='取出'),
transMoney int not null check(transMoney>0),
remark nvarchar(20) 
)
go
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567
--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
  -- 开户金额： 1  定期 卡号：1010 3576 1212 117

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：1010 3576 1212 113


insert into userInfol(customerName,PID,telephone,address)
select'孙悟空','123456789012345','0716-78989783','北京海淀'
insert into userInfol(customerName,PID,telephone)
select'沙和尚','421345678912345678','0478-44223333'union
select'唐僧','321245678912345678','0478-44443333'

select * from userInfol

insert into cardInfo(customerID,savingType,cardID,balance)
select 1,'活期','1010 3576 1212 113',1000 
insert into cardInfo(customerID,savingType,cardID,balance)
select 2,'活期','1010 3576 1212 117',1000 union
select 3,'活期','1010 3576 1212 114',1000
select * from cardInfo
--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234 where cardID='1010 3576 1212 113'
--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
update cardInfo set balance=balance-200 where cardID='1010 3576 1212 113'
insert into transInfo values(getdate(),'1010 3576 1212 113','取出',200,null)
 

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'
insert into transInfo values(getdate(),'1010 3576 1212 117','存入',300,null)
select * from cardInfo
--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 114'

--5.	查询出最近10天开户的银行卡的信息
select * from cardInfo where DateDiff(dd,opendate,getdate())<=10
--6.	查询交易金额最大的银行卡信息，子查询实现
select max(transMoney)最大金额 from transInfo 
--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
select sum(transMoney)交易中和 from transInfo 
--  显示效果：
--  总交易金额：1400.00
