use master
go

create database  ATM
on
(
name='ATM',
filename='D:\bank.mdf',
size=5MB,
filegrowth=15MB,
maxsize=50MB
)
log on
(
name='ATM_log',
filename='D:\bank.ldf',
size=5MB,
filegrowth=15MB,
maxsize=50MB
)
go

use ATM
go

create table userInfo
(
customerID int primary key identity(1,1),
customerName varchar(20) not null,
PID varchar(20) check(len(PID)=15 or len(PID)=18) unique not null,
telephone varchar(20) check(telephone like'____-________' and len(telephone)=13),
address varchar(20) 
)

create table cardInfo
(
cardID varchar(20) primary key check(substring(cardID,1,9)='1010 3576' and len(cardID)=18),
curType varchar(20) default('RMB') not null,
savingType varchar(20),
openDate datetime default(getdate()) not null,
balance money check(balance>=1) not null,
pass varchar(10) default('888888') check(len(pass)=6),
IsReportLoss char(2) check(IsReportLoss='是' or IsReportLoss='否') default('否') not null,
customerID int foreign key references userInfo(customerID)
)

create table transInfo
(
transId int primary key identity(1,1),
transDate datetime default(getdate()) not null,
cardID varchar(20) foreign key references cardInfo(cardID) not null,
transType varchar(20) check(transType='存入' or transType='支取') not null,
transMoney money check(transMoney>0) not null,
remark text  
)

insert into userInfo(customerName,PID,telephone,address) values
('孙悟空','123456789012345','0716-78989783','北京海淀')
insert into userInfo(customerName,PID,telephone) values
('沙和尚','421345678912345678','0478-44223333'),
('唐僧','321245678912345678','0478-44443333')

insert into cardInfo(customerID,balance,savingType,cardID) values
(1,'1000','活期','1010 3576 1234 567'),
(2,'1','定期','1010 3576 1212 117'),
(3,'1','定期','1010 3576 1212 113')

update cardInfo set pass='611234' where balance=1000.00 

select customerID from userInfo where customerName='孙悟空'
select cardID from cardInfo where customerID=1
update cardInfo set balance=800 where cardID='1010 3576 1234 567'
alter table transInfo drop constraint FK__transInfo__cardI__20C1E124
insert into transInfo(cardID,transType,transMoney) values(1,'支取',200)

select customerID from userInfo where customerName='沙和尚'
select cardID from cardInfo where customerID=2
update cardInfo set balance=301 where cardID='1010 3576 1212 117'
insert into transInfo(cardID,transType,transMoney) values(3,'存入',300)

update cardInfo set IsReportLoss='是' where customerID=3


select * from cardInfo where openDate>=2021-03-09 or openDate>=2021-03-19
 
select max(balance) from cardInfo

select sum(transMoney) from transInfo
