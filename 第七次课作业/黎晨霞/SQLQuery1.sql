use master
go

create database ATM
on
(
   name='ATM',
   filename='D:\bank\ATM.mdf',
   size=10mb,
   maxsize=100mb,
   filegrowth=15mb
)
log on
(
   name='ATM_log',
   filename='D:\bank\ATM_log.ldf',
   size=10mb,
   maxsize=100mb,
   filegrowth=15mb
)
go

use ATM
go

create table userInfo
(
    customerID int primary key identity(1,1),
	customerName nvarchar(20) not null,
	PID char(20) check(len(PID)=15 or len(PID)=18) unique not null,
	telephone varchar(13) check(len(telephone)=13 and telephone like '____-________') not null,
	address nvarchar(200)
)

create table cardInfo
(
   cardID varchar(18) primary key check(substring(cardID,1,9)='1010 3576' and len(cardID)=18 ) not null,
   curType varchar(20) not null default('RMB'),
   savingType char(20) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
   openDate datetime not null default(getdate()),--getdate()当前日期
   balance int not null check(balance>=1),
   pass char(6) not null check(len(pass)=6) default('888888'),
   IsReportLoss char(2) not null default('否') check(IsReportLoss='是' or IsReportLoss='否'),
   customerID int references userInfo(customerID) not null
)

create table transInfo
(
   transId int primary key identity(1,1),
   trabsDate datetime not null default(getdate()),
   cardID varchar(18) not null references cardInfo(cardID),
   transType char(4) not null check(transType='支取' or transType='存入'),
   transMoney int check(transMoney>0) not null,
   remark text
)

--插入数据开户
select * from userInfo
insert into userInfo values('孙悟空开户','123456789012345','0716-78989783','北京海淀')
insert into userInfo(customerName,PID,telephone) values('沙和尚开户','421345678912345678','0478-44223333')
insert into userInfo(customerName,PID,telephone) values('唐僧开户','321245678912345678','0478-44443333')

--插入银行数据
select * from cardInfo
insert into cardInfo(balance,savingType,cardID,customerID)
values (1000,'活期','1010 3576 1234 567',1)
insert into cardInfo(balance,savingType,cardID,customerID)
values (1,'定期','1010 3576 1212 117',2)
insert into cardInfo(balance,savingType,cardID,customerID)
values (1,'定期','1010 3576 1212 113',3)

--1.将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass='611234' where customerID=1

--2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
select * from transInfo
insert into transInfo(cardID,transType,transMoney)
values ('1010 3576 1234 567','支取','200')
update cardInfo set balance=balance-200 where customerID=1

--3.用同上题一样的方法实现沙和尚存钱的操作(存300)
select * from transInfo
insert into transInfo(cardID,transType,transMoney)
values ('1010 3576 1212 117','存入','300')
update cardInfo set balance=balance+300 where customerID=2

--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where customerID=3

--5.查询出最近10天开户的银行卡的信息
select top 10 * from  cardInfo where openDate>='2021-3-10'

--查询交易金额最大的银行卡信息
select  top 1 * from cardInfo where balance>1 order by balance DESC

--7.再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
  --显示效果：
  --总交易金额：1400.00
  --支取交易金额：200.00
  --存入交易金额：1200.00
select sum (transMoney)总交易金额 from transInfo
select sum (transMoney)支取交易金额 from transInfo where transType='支取'
select sum (transMoney)存入交易金额 from transInfo where transType='存入'