create database ATM
on
(
	name='AMT',
	filename='D:\bank\AMT.mfd',
	size=10MB,
	filegrowth=15%
)
log on
(
	name='AMT_log',
	filename='D:\bank\AMT_log.ldf',
	size=10MB,
	filegrowth=15%
)
go

use ATM
go
--用户信息表：userInfo ：
create table userInfo 
(
	customerID int identity(1,1) primary key,
	customerName varchar(32) not null,
	PID varchar(18) unique check(len(PID)=18 or len(PID)=15),
	telephone char(13) check(telephone like '____-________' or len(telephone)=13),
	address varchar(200)
)
--银行卡信息表：cardInfo
create table cardInfo
(
	cardID char(18) check(cardID like '1010 3576 ____ ___') primary key,
	--cardID char(18) check(substring(cardID,1,10)='1010 3576 ' and len(cardID)=18)
	curType varchar(30) not null default('RMB'),
	savingType nvarchar(4) check(savingType in('活期','定活两便','定期')) not null,
	openDate date default(getdate()),
	balance money check(balance>0) not null,
	pass char(6) not null default('888888'),
	IsReportLoss char(2) check(IsReportLoss in('是','否')) default('否') not null,
	customerID int references userInfo(customerID)
)
--交易信息表：transInfo
create table transInfo 
(
	transId int identity(1,1) primary key,
	transDate datetime default(getdate()) not null,
	cardID char(18) check(cardID like '1010 3576 ____ ___')
	references cardInfo(cardID) not null,--not unique
	transType nvarchar(2) check(transType in('存入','支取')) not null,
	transMoney money check(transMoney>0),
	remark varchar(200)
)
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567
insert into userInfo values ('孙悟空','123456789012345','0716-78989783','北京海淀')
insert into cardInfo(cardID,savingType,balance,customerID) values ('1010 3576 1234 567','活期',1000,1)
select * from userInfo
select * from cardInfo 
--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：1010 3576 1212 117
insert into userInfo values ('沙和尚','421345678912345678','0478-44223333',' ')
insert into cardInfo(cardID,savingType,balance,customerID) values ('1010 3576 1212 117','定期',1,2)

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：1010 3576 1212 113
insert into userInfo values ('唐僧','321245678912345678','0478-44443333',' ')
insert into cardInfo(cardID,savingType,balance,customerID) values ('1010 3576 1212 113','定期',1,3)
update userInfo set address=' ' where customerID=2
--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass='611234' where customerID=1
--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
select * from userInfo
select * from cardInfo 
select * from transInfo 
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','支取',200)
update cardInfo set balance=balance-200 where customerID=1

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where customerID=2

--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where customerID=3

--5.	查询出最近10天开户的银行卡的信息
select * from cardInfo where openDate>='2021-03-09' and openDate<='2021-03-19' 


--6.	查询交易金额最大的银行卡信息，子查询实现
select * from cardInfo where cardID=(select cardID from transInfo where transMoney=(select max(transMoney) from transInfo))

--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
select sum(transMoney) from transInfo
select sum(transMoney) from transInfo where transType='存入'
select sum(transMoney) from transInfo where transType='支取'
--  显示效果：
--  总交易金额：1400.00
--  支取交易金额：200.00
--  存入交易金额：1200.00