use master
go
create database ATM
on
( name='ATM',
  filename='C:\test\ATM.mdf',
  size=20,
  maxsize=200,
  filegrowth=10%
  )
  log on
  ( name='ATM_log',
  filename='C:\test\ATM_log.mdf',
  size=20,
  maxsize=200,
  filegrowth=10%
  )
  go 
  use ATM
  go
  create table userInfo
 ( 
    customerID int primary key identity,
    customerName varchar(20) not null,
	PID varchar(20) check(len(PID)=15 or len(PID)=18) not null,
	telephone char(13) check(len(telephone)=13 and telephone like '____-________') not null,
	address text
  )

  create table cardInfo
 ( 
    cardID char(18) primary key  check(len(cardID)=18 and cardID like '1010 3576 ____ ___') not null ,
     curType varchar(20) default('RMB')not null,
	savingType nchar(4) check(savingType='活期'or savingType='定活两便'or  savingType='定期'),
	openDate date default(getdate()) not null,
	balance money check(balance>=1) not null,
	pass  char(6) default('888888') check(len(pass)=6 ) not null ,
	IsReportLoss  nchar(1) default('否') check (IsReportLoss='是'or IsReportLoss='否') not null,
	customerID int foreign key (customerID) references  userInfo (customerID)
  )
  create table transInfo 
 ( 
    transId int primary key identity(1,1),
	transDate  date default(getdate()) not null,
	cardID char(18) foreign key references cardInfo(cardID), 
	transType int  check (transType='存入'or transType='支取') not null,
	transMoney Money check(transMoney>0) not null,
	remark text
	)

	insert into userInfo(customerName,PID,telephone,address) values('孙悟空','123456789012345','0716-78989783','北京海淀')
    insert into userInfo(customerName,PID,telephone,address) values('沙和尚','421345678912345678','0478-44223333','流沙河')
	insert into userInfo(customerName,PID,telephone,address) values('唐僧','321245678912345678','0478-44443333','东土大唐')

	insert into cardInfo(balance,savingType,cardID,customerID) values('1000 ','活期','1010 3576 1234 567','1')
	insert into cardInfo(balance,savingType,cardID,customerID) values('1 ','定期','1010 3576 1212 117','2')
	insert into cardInfo(balance,savingType,cardID,customerID) values('1 ','定期','1010 3576 1212 113','3')

	Update cardInfo set pass='611234' where customerID=1

    insert into transInfo(cardID,transType,transMoney)values ('1010 3576 1234 567','支取','200')
	Update cardInfo set balance=balance-200 where customerID=1

	insert into transInfo(cardID,transType,transMoney)values ('1010 3576 1212 117','存入','300')
	Update cardInfo set balance=balance+300 where customerID=2

	Update cardInfo set IsReportLoss='是' where customerID=3

   select*from cardInfo where DATEDIFF(DD,openDate,getdate())<10

   select max(balance) from cardInfo
   select sum(transMoney) from transInfo where transType='支出'