create database bank
on
(
	name='bank',
	size=5,
	filename='D:\bank.mdf',
	maxsize=100,
	filegrowth=15%
)
log on
(
	name='bank_log',
	size=5,
	filename='D:\bank_log.ldf',
	maxsize=100,
	filegrowth=15%
)
go 
use bank
go
create table userInfo
(
	customerID int identity(1,1) primary key,
	customerName varchar(32) not null,
	PID varchar(18) unique check(len(PID)=18 or len(PID)=15),
	telephone char(13) check(telephone like '____-________' or len(telephone)=13),
	address varchar(200)
)
create table cardInfo
(
	cardID nvarchar(20) primary key check(substring(cardID,1,9)='1010 3576' and len(cardID)=18),
	curType varchar(30) not null default('rmb'),
	savingType nvarchar(10) check(savingType in('活期','定活两便','定期')),
	openDate date not null default(getdate()),
	balance money check(balance>=1) not null,
	pass nvarchar(8) check(len(pass)=6) default('888888') not null,
	IsReportLoss char(2) default('否') check(IsReportLoss='是' or IsReportLoss='否') not null,
	customerID int foreign key references userInfo(customerID) not null
)
create table transInfo 
(
	transId int primary key identity ,
	transDate date default(getdate()) not null,
	cardID nvarchar(20) foreign key references cardInfo(cardID)not null,
	transType nvarchar(2) check(transType='存入' or transType='支取') not null,
	transMoney money check(transMoney>0) not null,
	remark text
)
insert into userInfo values ('孙悟空','123456789012345','0716-78989783','北京海淀'),('唐僧','321245678912345678','0478-44443333',' '),('沙和尚','421345678912345678','0478-44223333',' ')
insert into cardInfo(cardID,savingType,balance,customerID) values ('1010 3576 1234 567','活期',1000,1),('1010 3576 1212 117','定期',1,2),('1010 3576 1212 113','定期',1,3)
update cardInfo set pass='611234' where cardID='1010 3576 1234 567'
insert into transInfo(cardID,transType,transMoney) values('1010 3576 1234 567','支取',200)
update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'
update cardInfo set IsReportLoss='是' where  cardID='1010 3576 1212 113'
select * from cardInfo where openDate>=dateadd(day,-10,getdate())
select * from cardInfo where cardID=(select cardID from transInfo where transMoney=(select max(transMoney) from transInfo))
select sum(transMoney)总交易金额 from transInfo
select sum(transMoney)支取交易金额 from transInfo where transType='存入'
select sum(transMoney)存入交易金额 from transInfo where transType='支取'
