use master
go

create database ATM
on
(
name='ATM',
filename='C:\SQL\ATM.mdf',
size=10MB,
maxsize=100MB,
filegrowth=15%
)
log on
(
name='ATM_log',
filename='C:\SQL\ATM_log.ldf',
size=10MB,
maxsize=100MB,
filegrowth=15%
)
go
use ATM
go
create table UserInfo
(
CustomerID int primary key identity(1,1),
CustomerName nvarchar(10) not null,
PID varchar(20) check(len(PID)=15 or len(PID)=18) unique not null,
TelePhone char(13) check(len(TelePhone)=13 and TelePhone like '____-________') not null,
Address text
)
go
use ATM
go
create table CardInfo
(
CardID char(18) primary key  check(len(CardID)=18 and CardID like '1010 3576 ____ ___') not null,
CurType char(10) default('RMB') not null,
SavingType char(8) check(SavingType='活期' or SavingType='定活两便' or SavingType='定期'),
OpenDate datetime default(getdate()) not null,
BaLance int check(BaLance>=1) not null,
Pass char(6) default('888888') not null,
IsReportLoss char(2) check(IsReportLoss='是' or IsReportLoss='否') default('否') not null,
CustomerID int references UserInfo(CustomerID) not null,
)
go
use ATM
go
create table TransInfo
(
TransId int primary key identity(1,1) ,
TransDate datetime default(getdate()) not null,
CardID char(18) references CardInfo(CardID) not null,
TransType char(4) check(TransType='存入' or TransType='支出') not null,
TransMoney int check(TransMoney>0) not null,
Remark text
)

select * from UserInfo
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567
insert into UserInfo (CustomerName , PID , TelePhone , Address) values('孙悟空','123456789012345','0716-78989783','北京海定')
insert into CardInfo (CardID , SavingType , BaLance , CustomerID) values('1010 3576 1234 567','活期','1000',1)


--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，河底下
--   开户金额： 1  定期 卡号：1010 3576 1212 117
insert into UserInfo (CustomerName , PID , TelePhone , Address) values('沙和尚','421345678912345678','0478-44223333','河底下')
insert into CardInfo (CardID , SavingType , BaLance , CustomerID) values('1010 3576 1212 117','定期','1',2)

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，东土大唐
--   开户金额： 1  定期 卡号：1010 3576 1212 113
insert into UserInfo (CustomerName , PID , TelePhone , Address) values('唐僧','321245678912345678','0478-44443333','东土大唐')
insert into CardInfo (CardID , SavingType , BaLance , CustomerID) values('1010 3576 1212 113','定期','1',3)


--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update CardInfo SET Pass='611234' where CustomerID=1

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
select * from TransInfo
insert into TransInfo(CardID,TransType,TransMoney) values ('1010 3576 1234 567','支出','200')
update CardInfo SET BaLance=BaLance-200 where CustomerID=1

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into TransInfo(CardID,TransType,TransMoney) values ('1010 3576 1212 117','存入','300')
update CardInfo SET BaLance=BaLance+300 where CustomerID=2

--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update CardInfo SET IsReportLoss='是' where CustomerID=3

--5.	查询出最近10天开户的银行卡的信息
select top 10 * from  CardInfo where OpenDate>='2021-3-10'