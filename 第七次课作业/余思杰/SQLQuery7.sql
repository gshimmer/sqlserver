use master 
go

create database ATM
on
(
	name='ATM',
	filename='D:\bank\ATM.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=15%
)

log on
(
	name='ATM_log',
	filename='D:\bank\ATM_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=15%
)
go

use ATM
go
create table userInfo
(
	customerID int primary key identity(1,1),
	customerName nchar(10) not null,
	PID char(20) check(len(PID)=15 or len(PID)=18) unique(PID) not null,
	telephone char(13) check(len(telephone)=13 and telephone like '____-________') not null,
	address nchar(25) 
)
go

use ATM
create table cardInfo
(
	cardID char(20) primary key check(substring(cardID,1,9)='1010 3576' and len(cardID)=18) not null,
	curType nchar(10) default('RMB') not null,
	savingType char(8) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
	openDate date default(getdate()) not null,
	balance money check(balance>=1) not null,
	pass char(6) default(888888) not null,
	IsReportLoss char(2) check(IsReportLoss='是' or IsReportLoss='否') default('否') not null,
	customerID  int constraint FK_cardInfo_customerID foreign key references userInfo(customerID) not null
)
go

use ATM
create table transInfo 
(
	transId int primary key identity(1,1),
	transDate date default(getdate()) not null,
	cardID char(20) constraint FK_transInfo_cardID references cardInfo(cardID) not null,
	transType char(4) check(transType='存入'or transType='支取') not null,
	transMoney money check(transMoney>0) not null,
	remark text
)
go

--根据下列条件插入和更新测试数据
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567

--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：1010 3576 1212 117

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：1010 3576 1212 113

insert into userInfo values ('孙悟空',123456789012345,'0716-78989783','北京海淀')
insert into userInfo(customerName,PID,telephone) 
select '沙和尚',421345678912345678,'0478-44223333' union
select '唐僧',321245678912345678,'0478-44443333'

insert into cardInfo(balance,savingType,cardID,customerID)
select 1000,'活期','1010 3576 1234 567',1 union
select 1,'定期','1010 3576 1212 117',2 union
select 1,'定期','1010 3576 1212 113',3

--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234 where cardID='1010 3576 1234 567'

--2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo(transDate,cardID,transType,transMoney)
select getdate(),'1010 3576 1234 567','支取',200
update cardInfo set balance -= 200 where cardID='1010 3576 1234 567'

--3.用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(transDate,cardID,transType,transMoney)
select getdate(),'1010 3576 1212 117','存入',300
update cardInfo set balance += 300 where cardID='1010 3576 1212 117'

--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的
update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 113'

--5.查询出最近10天开户的银行卡的信息
select * from cardInfo where DateDiff(dd,openDate,getdate())<=10

--6.查询交易金额最大的银行卡信息，
select max(transMoney)交易金额最大 from transInfo

--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
--  显示效果：
--  总交易金额：1400.00
--  支取交易金额：200.00
--  存入交易金额：1200.00
select sum(transMoney)总交易金额 from transInfo
select transMoney 支取交易金额 from transInfo where transId=1 
select transMoney 存入交易金额 from transInfo where transId=2

select * from cardInfo--银行卡信息表
select * from userInfo--用户信息表
select * from transInfo--交易信息表
