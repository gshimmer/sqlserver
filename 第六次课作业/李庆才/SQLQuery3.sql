use master 
go 
create database bbs
on
(	name='bbs', 
	filename='D:\bbs.mdf',
	size=5mb,
	maxsize=5000mb,
	filegrowth=1mb
)
log on
(	name='bbs_log', 
	filename='D:\bbs_log.ldf',
	size=5mb,
	maxsize=5000mb,
	filegrowth=1mb
)go
use bbs
go 
create table bbsUsers 
(	UID int  identity(1,1),
	uName varchar(10) not null,
    uSex  varchar(2) not null,
	uAge  int  not null,
	uPoint  int not null
)
select*from bbsUsers
alter table bbsUsers add constraint Pk_bbsUsers_UID  primary key(UID)
alter table bbsUsers add constraint Uk_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint Ck_bbsUsers_uSex check(uSex in('男','女'))
alter table bbsUsers add constraint Ck_bbsUsers_uAge check(uAge>=15 and uAge<=60) 
alter table bbsUsers add constraint Ck_bbsUsers_uSex check(uPoint>=0)

create table bbsSection
(	sID  int identity(1,1),
    sName  varchar(10) not null,
    sUid   int 
)

insert into bbsSection values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

alter table bbsSection add constraint Pk_bbsSection_sID primary key(sID)
alter table bbsSection add constraint Fk_bbsSection_sUid  foreign key(sUid) references bbsUsers(UID)
select*from bbsSection


create table bbsTopic
(	 tID  int primary key identity(1,1),
     tUID  int references bbsUsers(UID),
     tSID  int references bbsSection( sID ),
     tTitle  varchar(100) not null,
     tMsg  text not null,
     tTime  datetime , 
	 tCount  int
)

select*from  bbsTopic
create table bbsReply
(	  rID  int primary key identity(1,1),
	  rUID  int references bbsUsers(UID),
	  rTID  int references bbsTopic(tID),
	  rMsg  text not null,
	  rTime  datetime 
)

--1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--	  小雨点  女  20  0
--	  逍遥    男  18  4
--	  七年级生  男  19  2

insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)


--	2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名

select uName, upoint  into bbsPoint from bbsUsers


--	3.给论坛开设4个板块
	  名称        版主名
	  技术交流    小雨点
	  读书世界    七年级生
	  生活百科     小雨点
	  八卦区       七年级生
--	 4.向主贴和回帖表中添加几条记录 
insert into bbsTopic values
                           (2,4,'范跑跑 ',' 谁是范跑跑  ','2008-7-8','1'),
						   (3,1,'.NET  ',' 与JAVA的区别是什么呀？  ','2008-9-1 ','2'),
						   (1,3,'今年夏天最流行什么 ',' 有谁知道今年夏天最流行  ','2008-9-10','0')

--人数不够，外键有约束
update bbsTopic  set tSID=3 where tSID=1

select*from bbsUsers
select*from bbsTopic
--	   回帖：
--	   分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定

insert into  bbsReply values(3,4,'谁问的谁就是范跑跑','20210316'),
							(1,3,'这个更简单','20210316'),
							(1,3,'今年夏天最流行内裤外穿','20210316')
select*from bbsReply

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
select*from  bbsUsers 
select*from bbsSection  --sUid 
select*from bbsTopic    --tUID
delete from bbsTopic where tUID=2
select*from bbsReply    --rUID

delete from  bbsUsers  where uName='逍遥'

--	6.因为小雨点发帖较多，将其积分增加10分
    select uPoint from bbsUsers where uName='小雨点'  
    update    bbsUsers  set uPoint=12  where uName='小雨点'  
    
--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
 select*from bbsTopic   
 alter table bbsReply drop constraint FK__bbsReply__rTID__1DE57479
 delete from bbsTopic where tSID=3

--	8.因回帖积累太多，现需要将所有的回帖删除
select*from bbsReply
truncate table bbsReply