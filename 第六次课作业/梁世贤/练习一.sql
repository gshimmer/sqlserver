create database TestDB--创建数据库
on primary
(
name='TestDB',
filename='D:SQL作业.mdf',
size=5MB,
filegrowth=5MB,
maxsize=unlimited
)
log on
(
name='TestDB_log',
filename='D:SQL作业_log.ldf',
size=5MB,
maxsize=5MB
)
go

use TestDB

--类别编号：主键 自动编号
-- 类别名(typeName): varchar(10)  不能为空
create table typeInfo  --创建会员类别表
(
typeId int identity(1,1) primary key,
typeName varchar(10) not null
)
go

-- 编号(LoginId)，数据类型(int)，主键、自动编号
-- 账户(LoginName)，文本，长度为10，非空，必须唯一，不能重复
--密码(LoginPwd)，文本，长度为20，非空、默认值为‘123456’
create table loginInfo  --创建登入用户表
(
LoginId int identity(1,1) primary key,
LoginName varchar(10) not null unique,
Loginpwd varchar(20) not null default('123456'),
sex varchar(2),
birthday varchar(15),
hycategory varchar(20)
)
go



create database company
on primary
(
name='company',
filename='D:SQL作业.mdf',
size=5MB,
filegrowth=1MB,
maxsize=5MB
)
log on
(
name='company_log',
filename='D:SQL作业.mdf',
size=5MB,
filegrowth=1MB,
maxsize=5MB
)
go

use company

--部门编号  sectionID  int 标识列  主键
--部门名称  sectionName  varchar(10)  不能为空

create table sectionInfo  --建立部门信息表
(
sectionID  int primary key,
sectionName varchar(10) not null
)
go

--员工编号  userNo  int  标识列 主键  不允许为空
--员工姓名  userName  varchar(10)  唯一约束 不允许为空 长度必须大于4
--员工性别  userSex   varchar(2)  不允许为空  只能是男或女
--员工年龄  userAge   int  不能为空  范围在1-100之间
--员工地址  userAddress  varchar(50)  默认值为“湖北”
--员工部门   userSection  int  外键，引用部门信息表的部门编号

create table userInfo --建立员工信息表
(
userNo int primary key not null,
userName varchar(10) unique check(userName>4),
userSex varchar(2) check(usersex = '男' or usersex = '女'),
userAge int check(userAge between 1 and 100) not null,
userAddress varchar(50) default('湖北'),
 userSection int  foreign key(userSection) references sectionInfo(sectionID)
)
go

--考勤编号  workId int 标识列  主键  不能为空
--考勤员工  userId  int 外键 引用员工信息表的员工编号 不能为空  
--考勤时间  workTime datetime 不能为空
--考勤说明  workDescription  varchar(40) 不能为空 内容只能是“迟到”，“早退”，“旷工”，“病假”，“事假”中的一种
create table workInfo
(
workId int primary key not null,
userId int foreign key(userId) references userInfo(userNo) not null,
workTime datetime not null,
workDescription varchar(40) check(workDescription='迟到'or workDescription='早退'or workDescription='旷工'or workDescription='病假'or workDescription='事假')
)
go



create database stuMS
on primary
(
name='stuMS',
filename='D:SQL作业.mdf',
size=5MB,
filegrowth=1MB,
maxsize=5MB
)
log on
(
name='stuMS_log',
filename='D:SQL作业_log.ldf',
size=5MB,
filegrowth=1MB,
maxsize=5MB
)
go


use stuMS
--班级编号 classid (主键、标识列
--班级名称(例如：T1、T2、D09等等):不能为空，不能重复
--开办时间：不能为空
--班级描述
create table classInfo  --建立班级信息表
(
classid int primary key,
classname varchar(10) unique not null,
crankuptime varchar(16) not null,
classdescription nvarchar(100)
)
go

--学号：主键、标识列
--姓名：长度大于2，不能重复
--性别：只能是‘男’或‘女’，默认为男，不能为空
--年龄：在15-40之间，不能为空
--家庭地址：默认为“湖北武汉”
--所在的班级编号
create table stuInfo
(
StudentID int primary key not null,
stuname  nvarchar(15) unique check(stuname>2) not null,
stusex varchar(2) check(stusex='男'or stusex='女') default('男') not null,
stuage varchar(4) check(stuage between 15 and 40) not null,
studress nvarchar(40) default('湖北武汉'),
classnumber int 
)
go


--编号：主键、标识列
--课程名：不能为空，不能重复
--课程描述：
create table courseInfo
(
serialnumber int primary key,
CourseName nvarchar(10) unique not null,
coursedescription nvarchar(50)
)
go

--成绩编号：主键、标识列
--成绩所属于的学生编号，不能为空
--成绩所属的课程编号，不能为空
--成绩：在0-100之间

create table achievementInfo --建立成绩表
(
ScoreId int primary key,
StudentNO int not null,
coursenumber int not null
)
go



create database CZ
on primary
(
name='CZ',
filename='D:SQL作业.mdf',
size=5MB,
filegrowth=1MB,
maxsize=5MB
)
log on
(
name='CZ_log',
filename='D:SQL作业_log.ldf',
size=5MB,
filegrowth=1MB,
maxsize=5MB
)
go

--tblUser 发布人信息表
--userId     userName    userTel
create table tblUser
(
userId varchar(10) not null,
userName nvarchar(5) not null,
userTel nvarchar(15) not null
)
go 

--tblHouseType 房屋的类型
--typeId
--typName

create table tblHouseType  --建立房屋类型表
(
typeId varchar(10) not null,
typeName varchar(10) not null
)
go 

--tblQx 区县
--qxId
--qxName

create table tblQx
(
qxId varchar(10) not null,
qxName varchar(20) not null
)
go 


--tblHouseInfo--房屋信息表
--id
--desc
--userId 
--zj
--shi
--ting
--typeId
--qxId 

create table tblHouseInfo
(
id int,
userId varchar(10),
zj int not null,
shi int not null,
ting int not null,
typeId varchar(10),
qxId varchar(15)
)