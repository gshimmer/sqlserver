use master
go

create database Xsglxt--学生管理系统
on(
	name='Xsglxt',
	filename='D:\数据库\Xsglxt.mdf',
	size=5mb,
	maxsize=100mb,
	filegrowth=10%
)

log on
(
	name='Xsglxt_log',
	filename='D:\数据库\Xsglxt_log.ldf',
	size=5mb,
	maxsize=100mb,
	filegrowth=10%
)

use Xsglxt
go

create table ClassInfo
(
	ClassNum char(10) primary key not null,
	ClassName char(10)unique not null,
	Opentime datetime not null,
	ClassRemark text
)

create table StuInfo
(
	StuName varchar(10) unique not null check(len(StuName)>2),
	StuSex varchar(2) default('男') check(StuSex='男' or StuSex='女') not null,
	StuAge varchar(4) not null check(StuAge>=15 and StuAge<=40),
	homeaddress nvarchar(20) default('湖北武汉'),
	szdClassid char(10) not null
)

create table CourseInfo
(
	LoginId int primary key identity(1,1),
	CurriculumName varchar(10) unique not null,--unique 唯一约束，不能重复
	Description text
)

create table StuGrade
(
	Recordnumber int primary key identity(1,1),
	Student int not null,
	CourseNumber varchar(10) not null,
	grade int check(grade>=0 and grade<=100)
)

create database bbs
on(
	name='bbs',
	filename='E:\数据库\bbs.mdf',
	size=5mb,
	maxsize=100mb,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='E:\数据库\bbs_log.ldf',
	size=5mb,
	maxsize=100mb,
	filegrowth=10%
)

use bbs
go

create table bbsUser
(
	UID int primary key identity(1,1),
	uName varchar(10) unique not null,
	uSex varchar(2) not null check(uSex='男' or uSex='女'),
	uAge int not null check(uAge>=15 and uAge<=60),
	uPoint int not null check(uPoint>=0)
)

create table bbsSection
(
	sID int primary key identity(1,1),
	sName varchar(10) not null,
	sUid int references bbsUser(UID)
)

create table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int references bbsUser(UID),
	tSID int references bbsSection(sID),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)

create table bbsReply
(
	rID int primary key identity(1,1),
	rUID int references bbsUser(UID),
	rTID int references bbsTopic(tID),
	rMsg text not null,
	rTime datetime
)

select * from bbsUsers
select * from bbsSection
select *from bbsTopic
select * from bbsReply

insert into bbsUsers(uName, uSex,uAge,uPoint)
values ('小雨点','女',20,0)
insert into bbsUsers(uName, uSex,uAge,uPoint)
values ('逍遥','男',18,4)
insert into bbsUsers(uName, uSex,uAge,uPoint)
values ('七年级生','男',19,2)

insert into bbsSection(sName , sUid)
values('技术交流',1)
insert into bbsSection(sName , sUid)
values('读书世界',3)
insert into bbsSection(sName , sUid)
values('生活百科',1)
insert into bbsSection(sName , sUid)
values('八卦区',3)

insert into bbsTopic(tUID ,tSID,tTitle,rMsg,rTime,tCount)
values (2,5,'范跑跑','谁是范跑跑','2008-7-8',1)
insert into bbsTopic(tUID ,tSID,tTitle,rMsg,rTime,tCount)
values (3,2,'.NET','与JAVA的区别是什么','2008-9-1',2)
insert into bbsTopic(tUID ,tSID,tTitle,rMsg,rTime,tCount)
values (1,4,'今年夏天流行什么','有谁知道今年夏天流行什么呀？','2008-9-10',0)

insert into bbsReply(rUID,rTID,rMsg,rTime)
values(1,3,'范跑跑','2008-10-2')
insert into bbsReply(rUID,rTID,rMsg,rTime)
values(2,2,'区别不大','2008-10-2')
insert into bbsReply(rUID,rTID,rMsg,rTime)
values(3,1,'蓝色','2008-10-2')

select uName,uPoint into bbsPoint from bbsUsers --备份uName uPoint 到新表bbsPoint

alter  table bbsTopic drop constraint FK__bbsTopic__tUID__2D27B809
alter  table bbsReply drop constraint FK__bbsReply__rUID__30F848ED
delete from bbsUsers where uName='逍遥'
update bbsUsers set upoint=10 where uName='小雨点'

alter table bbsTopic drop FK__bbsTopic__tSID__2E1BDC42
delete bbsSection where sName='生活百科'

truncate table bbsReply