--4. 为一个房屋出租系统创建一个数据库，数据库中主要存放房屋的信息，
--包括：房屋的编号，房屋的描述，发布人的信息(姓名和联系电话)，房屋的租金，
--房屋的室数，房屋的厅数，房屋的类型(别墅、普通住宅、平房、地下室)，房屋所属的区县(武昌、汉阳、汉口)，
--请根据上面的描述设计表，并设计表的关系，以及列的约束
create database HouseRental
on
(
	name='HouseRental',
	filename='D:\test\HouseRental.mdf'
)
log on
(
	name='HouseRental_log',
	filename='D:\test\HouseRental_log.ldf'
)
--发布人信息表
use HouseRental
go
create table tblUser
(
	userId int primary key(userId) identity(1,1),
	userName varchar(10) not null,
	userTel char(11) not null
)
--房屋的类型
create table tblHouseType
(
	typeId int primary key(typeId) identity(1,1),
	typName varchar(10) not null,
)

--区县
create table tblQx
(
	qxId int primary key(qxId) identity(1,1),
	qxName varchar(10) not null,
)
--房屋信息表
create table tblHouseInfo
(
	tblHouseid int primary key(tblHouseid) identity(1,1),
	describe text,
	userId int foreign key(userId) references tblUser(userId),
	rent int,
	shishu int,
	tingshu int,
	typeId int foreign key(typeId) references tblHouseType(typeId),
	qxId int foreign key(qxId) references tblQx(qxId)
)