use master
go 

--1. 先创建一个数据库，数据库名为TestDB，要求有两个数据文件，一个日志文件，注意命名规范，文件存放在E盘下
--   再在上面的数据库下创建表，结构如下：
create database TestDB
on
(
	name='TestDB',
	filename='D:\test\TestDB.mdf'
)
log on
(
	name='TestDB_log',
	filename='D:\test\TestDB_log.ldf'
)
go

use TestDB
go
--   会员类别表(typeInfo)：
create table TypeInfo
(
--   类别编号(typeId)：主键、自动编号
	TypeID int primary key(TypeID) identity(1,1),
--   类别名(typeName): varchar(10)  不能为空
	TypeName varchar(10) not null
)
--   登录用户表(loginInfo)：
create table LoginInfo
(
--   编号(LoginId)，数据类型(int)，主键、自动编号
	LoginID int primary key(LoginID) identity(1,1),
--   账户(LoginName)，文本，长度为10，非空，必须唯一，不能重复
	LoginName nvarchar(10) unique not null,
--   密码(LoginPwd)，文本，长度为20，非空、默认值为‘123456’
	LoginPwd nvarchar(20) not null default('123456'),
--   性别(自定类型)
	LoginSex varchar(1) default('男') check(LoginSex in('男','女')),
--   生日(自定类型)
	Birthday date,
--   会员类别(自定类型)
	VIPType nvarchar(10)
)