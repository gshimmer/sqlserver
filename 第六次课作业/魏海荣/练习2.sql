--一、先创建数据库和表以及约束

--	1.创建一个数据库用来存放某论坛的用户和发帖信息，数据库的名称为bbs，包含1个数据文件1个日志
--	文件，数据文件和日志文件全部存放在E盘中，初始大小，增长和最大大小自己设定
create database bbs
on
(
	name='bbs',
	filename='D:\test\bbs.mdf'
)
log on
(
	name='bbs_log',
	filename='D:\test\bbs_log.ldf'
)
--	2.创建表
--	注意以下4张表的创建顺序，要求在创建bbsUser和bbsSection表时不添加约束，创建完后单独添加约束，其它的表创建时添加约束
--	用户信息表（bbsUsers）
use bbs
go
create table bbsUsers
(
--	用户编号  UID int 主键  标识列
	UID int identity(1,1),
--	用户名    uName varchar(10)  唯一约束 不能为空
	uName varchar(10) not null,
--	性别      uSex  varchar(2)  不能为空 只能是男或女
	uSex varchar(2) not null,
--	年龄      uAge  int  不能为空 范围15-60
	uAge int not null,
--	积分      uPoint  int 不能为空  范围 >= 0
	uPoint int not null,
)
alter table bbsUsers add constraint PK_bbsUsers_UID primary key(UID)
alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in('男','女'))
alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>=0)

--	版块表（bbsSection）
create table bbsSection
(
--	版块编号  sID  int 标识列 主键
	sID int  identity(1,1),
--	版块名称  sName  varchar(10)  不能为空
	sName  varchar(10) not null,
--	版主编号  sUid   int 外键  引用用户信息表的用户编号
	sUid   int
)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(UID)

--+	主贴表（bbsTopic）
create table bbsTopic
(
--	主贴编号  tID  int 主键  标识列，
	tID int primary key(tID) identity(1,1),
--	发帖人编号  tUID  int 外键  引用用户信息表的用户编号
	tUID int,
--	版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
	tSID int,
--	贴子的标题  tTitle  varchar(100) 不能为空
	tTitle varchar(100) not null,
--	帖子的内容  tMsg  text  不能为空
	tMsg text not null,
--	发帖时间    tTime  datetime  
	tTime datetime,
--	回复数量    tCount  int
	tCount  int
)
alter table bbsTopic add constraint FK_bbsTopic_tUID foreign key(tUID) references bbsUsers(UID)
alter table bbsTopic add constraint FK_bbsTopic_tSID foreign key(tSID) references bbsSection(sID)

--+	回帖表（bbsReply）
create table bbsReply
(
--	回贴编号  rID  int 主键  标识列，
	rID int primary key(rID) identity(1,1),
--	回帖人编号  rUID  int 外键  引用用户信息表的用户编号
	rUID  int,
--	对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
	rTID  int,
--	回帖的内容  rMsg  text  不能为空
	rMsg  text not null,
--	回帖时间    rTime  datetime 
	rTime  datetime,
)
alter table bbsReply add constraint FK_bbsReply_rUID foreign key(rUID) references bbsUsers(UID)
alter table bbsReply add constraint FK_bbsReply_rTID foreign key(rTID) references bbsSection(sID)
	
--二、在上面的数据库、表的基础上完成下列题目：
use bbs
go

--	1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--	  小雨点  女  20  0
--	  逍遥    男  18  4
--	  七年级生  男  19  2
insert into bbsUsers values ('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)

--	2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
select uName, upoint  into bbsPoint from bbsUsers
--	3.给论坛开设4个板块
--	  名称        版主名
--	  技术交流    小雨点
--	  读书世界    七年级生
--	  生活百科     小雨点
--	  八卦区       七年级生
insert into bbsSection values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

--	4.向主贴和回帖表中添加几条记录
	  
--	   主贴：

--	  发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
--	  逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
--	  七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
--	  小雨点   生活百科    今年夏天最流行什么     有谁知道今年夏天最流行  2008-9-10  0
--						      什么呀？
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values
(2,4,'范跑跑','谁是范跑跑','2008-7-8',1),(3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10',0)
--	   回帖：
--	   分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定
insert into bbsReply values(1,1,'不知道','2008-7-9'),(1,2,'不知道','2008-9-2'),(1,2,'问百度','2008-9-2')
--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
select * from bbsUsers --查看逍遥的用户ID
delete from bbsUsers where UID=2
--	6.因为小雨点发帖较多，将其积分增加10分
update bbsUsers set uPoint=10 where uName='小雨点'
--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
alter table bbsTopic drop constraint FK_bbsTopic_tSID
delete from bbsSection where sName='生活百科'
--	8.因回帖积累太多，现需要将所有的回帖删除
delete from bbsReply
