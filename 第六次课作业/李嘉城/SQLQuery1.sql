use master
go
create database TestDB
on(
name='typeInfo',
filename='D:\318\typeInfo.mdf',
size=5,
maxsize=10,
filegrowth=1
)
log on(
name='typeInfo_log',
filename='D:\318\typeInfo_log.ldf',
size=5,
maxsize=10,
filegrowth=1
)
go
use TestDB
go
create database typeInfo
(
typeId int primary key identity(1,1),
typeName varchar(10) not null
)
create database loginInfo
(
LoginId int primary key identity(1,1),
LoginName text not null unique,
LoginPwd text  not null default('123456'),
Logsex nvarchar(1) default('��'),
birthday datetime,
member nvarchar
)