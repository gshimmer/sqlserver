use master
go
create database bbs
on
(
name='bbs',
filename='D:\ljc\bbs.mdf',
size=10Mb,
maxsize=50Mb,
filegrowth=10%
)
log on
(
name='bbs_log',
filename='D:\ljc\bbs_log.ldf',
size=10Mb,
maxsize=50Mb,
filegrowth=10%
)
go
use bbs
go
create table BbsUsers
(
UserId int identity(1,1),
UserName varchar(10) not null,
UserSex varchar(2) not null,
UserAge int not null,
UserPoint int not null
)
alter table BbsUsers add constraint PK_BbsUser_UserId primary key(UserId)
alter table BbsUsers add constraint UK_BbsUser_UserName unique(UserName)
alter table BbsUsers add constraint CK_BbsUser_UserSex check(UserSex='男' or UserSex='女')
alter table BbsUsers add constraint CK_BbsUser_UserAge check(UserAge>=15 and UserAge<=60)
alter table BbsUsers add constraint CK_BbsUser_UserPoint check(UserPoint>=0)

create table BbsTopic
(
TopicId int primary key identity(1,1),
TopicUserId int foreign key references BbsUsers(UserId),
TopicSId int foreign key references BbsSection(SectionId),
TopicTitle varchar(100) not null,
TopicMsg text not null,
TopicTime datetime,
TopicCount int
)
create table BbsReply
(
ReplyId int primary key identity(1,1),
RepluUid int foreign key references BbsUsers(UserId),
ReplyTid int foreign key references BbsTopic(TopicId),
ReplyMsg text not null,
ReplyTime datetime
)
create table BbsSection
(
SectionId int Identity(1,1),
SectionName varchar(10) not null,
SectionUid int
)
alter table BbsSection add constraint PK_BbsSection_SectionId primary key(SectionId)
alter table BbsSection add constraint FK_BbsSection_SectionUid foreign key(SectionUid) references BbsUsers(UserId)

insert into BbsUsers values 
('小雨点','女','20','0'),
('逍遥','男','18','4'),
('七年级生','男','19','2')

select UserName,UserPoint into BbsPoint from BbsUsers

insert into BbsSection values
('技术交流',3),
('读书世界',5),
('生活百科',3),
('八卦区',5)
select * from BbsTopic
insert into BbsTopic values
(4,6,'范跑跑','谁是范跑跑','2008-7-8',1),
(5,3,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),
(3,5,'今年夏天最流行什么','有谁知道及那年夏天最流行什么呀？','2008-9-10',0)
insert into BbsReply values
(3,5,'你是飯桶','2008-7-9'),
(3,6,'本質一樣','2008-9-2'),
(4,7,'最流行吃飯','2008-9-11')
alter table BbsReply drop constraint FK__BbsReply__RepluU__1FCDBCEB
alter table BbsTopic drop constraint FK__BbsTopic__TopicU__1BFD2C07
delete BbsUsers where UserName='逍遥'

update bbsUsers set UserPoint=12 where UserName='小雨点'

alter table BbsTopic drop constraint FK__BbsTopic__TopicS__1CF15040
delete BbsSection where SectionName='生活百科'

delete  BbsReply 