use master
go
create database Class
on
(
name='Class',
filename='D:\ljc\Class.mdf',
size=10,
maxsize=50,
filegrowth=10%
)
log on
(
name='Class_log',
filename='D:\ljc\Class_log.ldf',
size=10,
maxsize=50,
filegrowth=10%
)
go
use Class
go
create table ClassInfo
(
ClassId int primary key identity(1,1),
ClassName varchar(4) unique not null,
OpenTime datetime not null,
ClassDescribe nvarchar(50)
)

create table StuInfo
(
StuNo int primary key identity(1,1),
StuSex nvarchar(1) default('��') check(StuSex='��' or StuSex='Ů') not null,
StuAge int check(StuAge>=15 and StuAge<=40) not null,
StuAdress nvarchar(4) default('�����人'),
ClassId int foreign key references ClassInfo(ClassId)
)
create table Course
(
CourseNo int primary key identity(1,1),
CourseName nvarchar(4) unique not null,
CourseDescribe nvarchar(50)
)
create table Credit
(
CreditNo int primary key identity(1,1),
StuNo int foreign key references StuInfo(StuNo),
CourseNo int foreign key references Course(CourseNo),
Credit int check(Credit>=0 and Credit<=100)
)
