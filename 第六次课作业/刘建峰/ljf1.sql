use master
go
create database TestDB
go
create database TestDB
on
(
name='students',
filename='d:\students.mdf'
)
log on
(
name='students_log',
filename='d:\studnts_log.ldf'
)
create table typelnfo
(
typeld int primary key identity(1,1),
typeName varchar(10) not null
)

create table loginlnfo
(
Loginld int  primary key identity(1,1),
Loginname char(10) not null unique,
LoginPwd char(20) not null default('123456'),
xingbei bit,
shengre datetime,
leibei nvarchar(10)
)
use master
go
create database company
go
create database company
on
(
name='home1',
filename='d:\home1.mdf',
size=3mb,
filegrowth=10%,
maxsize=50mb
)
log on
(
name='home1_log',
filename='d:\home1_log.ldf',
size=3mb,
filegrwth=10%,
maxsize=50mb
)
create table sectionlnfo
(
sectionID INT IDENTITY(1,1) primary key,

sectionName varchar(10) not null
)
create table userlnfo
(
userNo int identity(1,1) primary key not null,
userName  VARCHAR(10) check(len(userName)>4),
userSex varchar(2) not null check(userSex='��' or userSex='Ů'),
userAge int not null check(userAge>=1 and userAge<=100),
userAddress varchar(50) default('����'),
userSection int foreign key references sectionlnfo(sectionID)
)
create table worklnfo
(
workld int identity(1,1) primary key not null,
userld int foreign key references userlnfo(userNo),
workTime datetime not null,
workDescription varchar(40) not null check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription= '����' or workDescription='�¼�')
)
use master
go
create database guanli
go
create database guanli
on
(
name='guanli',
filename='d:\guanli.mdf',
size=3mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name='guanli_log',
filename='d:\guanli_log.ldf',
size=3md,
maxsize=50mb,
filegrowth=10%
)
create table banji
(
classid int identity(1,1) primary key,
t1 int not null unique,
shijian int not null
)
create table xsheng
(
xhao int identity(1,1) primary key,
xingming int unique,
xingbe varchar(2) not null check(xingbe='��' or xingbe='Ů')default('��'),
nianling int check(nianling>=15 and nianling<=40) not null,
dizhi char default('�����人'),
xshengxinxi int foreign key references banji(classid)
)
create table kecheng
(
bianhao int primary key identity(1,1),
kechenming char not null unique,
miaoshu varchar(50)
)
create table chengji
(
bianhao int primary key identity(1,1),
number int foreign key references xsheng(xhao) not null,
course int foreign key references kecheng(bianhao),
performance int check(performance>=0 and performance<=100)
)

create database fangwuxing
on
(
name='fangwu',
filename='d:\fangwu.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name='fangwu_log',
filename='d:\fangwu_log.ldf',
size=5mb,
maxsize=50mb,
filegrowth=10%
)
use fangwuxing
go
create table tblUser 
(
userId int primary key identity(1,1),
userName varchar,
userTel  nvarchar(10)
)
create table tblHouseType 
(
typeId  int primary key identity(1,1),
typName varchar check(typName='����'or typName='��ͨסլ'or typName='ƽ��' or typName='������') not null
)
create table tblQx 
(
qxId int primary key identity(1,1),
qxName varchar check(qxName='���'or qxName='����' or qxName='����') not null
)
create table tblHouseInfo
(
id int primary key identity(1,1),
userId int foreign key references tblUser (userId),
zj money,
shi varchar,
ting int,
typeId int foreign key references tblHouseType (typeId),
qxId int foreign key references tblQx (qxId)
)

