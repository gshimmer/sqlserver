use master
go

create database TestDB
on
(
	name='TestDB',
	filename='D:\11\TestDB.mdf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
log on
(

	name='TestDB_log',
	filename='D:\11\TestDB_log.ldf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
go

use TestDB
go

create table TypeInfo
(
	TypeId int primary key identity(1,1),
	TypeName varchar(10) not null 
)

create  table LoginInfo
(
	LoginId int primary key identity(1,1),
	LoginName nvarchar(10) unique not null,
	LoginPwd nvarchar(20) default('123456') not null,
	LoginSex varchar(1) default('男') check(LoginSex='男' or LoginSex='女'),
	LoginBirthday datetime,
	LoginType nvarchar(10)
)

use master
go

create database Company
on
(
	name='Company',
	filename='D:\11\Company.mdf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
log on
(

	name='Company_log',
	filename='D:\11\Company_log.ldf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
go

use Company
go

create table SectionInfo
(
	SectionId int primary key identity(1,1),
	SectionName varchar(10) not null
)

create table UserInfo
(
	UserNo int primary key identity(1,1) not null,
	UserName varchar(10) unique not null check(len(UserName)>4), 
	UserSex varchar(2) check(UserSex='男' or UserSex='女') not null,
	UserAge int check(UserAge>=1 and UserAge<=100) not null,
	UserAddress varchar(50) default('湖北'),
	UserSection int foreign key references SectionInfo(SectionId)
)

create table WorkInfo
(
	WorkId int primary key identity(1,1) not null,
	UserId int foreign key references UserInfo(UserNo) not null,
	WorkTime datetime not null,
	WorkDescription varchar(40) not null check(WorkDescription='迟到' or WorkDescription='早退'or WorkDescription='旷工' or WorkDescription='病假' or WorkDescription='事假')
)

use master
go

create database Class
on
(
	name='Class',
	filename='D:\11\Class.mdf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
log on
(

	name='Class_log',
	filename='D:\11\Class_log.ldf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
go

use Class
go

create table ClassInfo
(
	ClassId int primary key identity(1,1),
	ClassName varchar(4) unique not null,
	OpenTime datetime not null,
	ClassDescribe nvarchar(50)
)

create table StuInfo
(
	StuNo int primary key identity(1,1),
	StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女') not null,
	StuAge int check(StuAge>=15 and StuAge<=40) not null,
	StuAdress nvarchar(4) default('湖北武汉'),
	ClassId int foreign key references ClassInfo(ClassId)
)

create table Course
(
	CourseNo int primary key identity(1,1),
	CourseName nvarchar(4) unique not null,
	CourseDescribe nvarchar(50)
)

create table Credit
(
	CreditNo int primary key identity(1,1),
	StuNo int foreign key references StuInfo(StuNo),
	CourseNo int foreign key references Course(CourseNo),
	Credit int check(Credit>=0 and Credit<=100)
)

use master
go

create database House
on
(
	name='House',
	filename='D:\11\House.mdf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
log on
(

	name='House_log',
	filename='D:\11\House_log.ldf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
go

use House
go

create table TblUser
(
	UserId int primary key identity(1,1),
	UserName nvarchar(4) unique not null,
	UserTel varchar(11) unique not null
)

create table TblHouseType
(
	TypeId int primary key identity(1,1),
	TypeName nvarchar(4) unique not null
)

create table TblQx
(
	QxId int primary key identity(1,1),
	QxName nvarchar(4) unique not null
)

create table HouseInfo
(
	Id int primary key identity(1,1),
	Desc1 nvarchar(50),
	UserId int foreign key references TblUser(UserID),
	zj money,
	shi int,
	ting int,
	TypeId int foreign key references TblHouseType(TypeId),
	QxId int foreign key references TblQx(QxId)
)

use master
go

create database bbs
on
(
	name='bbs',
	filename='D:\11\bbs.mdf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
log on
(

	name='bbs_log',
	filename='D:\11\bbs_log.ldf',
	size=10Mb,
	maxsize=50Mb,
	filegrowth=10%
)
go

use bbs
go

create table BbsUsers
(
	UserId int identity(1,1),
	UserName varchar(10) not null,
	UserSex varchar(2) not null,
	UserAge int not null,
	UserPoint int not null
)

alter table BbsUsers add constraint PK_BbsUser_UserId primary key(UserId)
alter table BbsUsers add constraint UK_BbsUser_UserName unique(UserName)
alter table BbsUsers add constraint CK_BbsUser_UserSex check(UserSex='男' or UserSex='女')
alter table BbsUsers add constraint CK_BbsUser_UserAge check(UserAge>=15 and UserAge<=60)
alter table BbsUsers add constraint CK_BbsUser_UserPoint check(UserPoint>=0)

create table BbsTopic
(
	TopicId int primary key identity(1,1),
	TopicUserId int foreign key references BbsUsers(UserId),
	TopicSId int foreign key references BbsSection(SectionId),
	TopicTitle varchar(100) not null,
	TopicMsg text not null,
	TopicTime datetime,
	TopicCount int
)

create table BbsReply
(
	ReplyId int primary key identity(1,1),
	RepluUid int foreign key references BbsUsers(UserId),
	ReplyTid int foreign key references BbsTopic(TopicId),
	ReplyMsg text not null,
	ReplyTime datetime
)

create table BbsSection
(
	SectionId int Identity(1,1),
	SectionName varchar(10) not null,
	SectionUid int
)

alter table BbsSection add constraint PK_BbsSection_SectionId primary key(SectionId)
alter table BbsSection add constraint FK_BbsSection_SectionUid foreign key(SectionUid) references BbsUsers(UserId)

insert into BbsUsers values 
('小雨点','女','20','0'),
('逍遥','男','18','4'),
('七年级生','男','19','2')


select UserName,UserPoint into BbsPoint from BbsUsers

insert into BbsSection values
('技术交流',3),
('读书世界',5),
('生活百科',3),
('八卦区',5)
select * from BbsTopic
insert into BbsTopic values
(4,6,'范跑跑','谁是范跑跑','2008-7-8',1),
(5,3,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),
(3,5,'今年夏天最流行什么','有谁知道及那年夏天最流行什么呀？','2008-9-10',0)

insert into BbsReply values
(3,5,'计划司法机会大','2008-9-1'),
(3,6,'家私房价','2008-10-1'),
(4,7,'山东矿机拉速度快','2008-10-10')

alter table BbsReply drop constraint FK__BbsReply__RepluU__1FCDBCEB
alter table BbsTopic drop constraint FK__BbsTopic__TopicU__1BFD2C07
delete BbsUsers where UserName='逍遥'

update bbsUsers set UserPoint=12 where UserName='小雨点'

alter table BbsTopic drop constraint FK__BbsTopic__TopicS__1CF15040
delete BbsSection where SectionName='生活百科'

delete  BbsReply 