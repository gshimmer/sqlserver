create database TestDB
on
(
   name='TestDB',
   filename='D:\TestDB.mdf'
)

log on
(
   name='TestDB_log',
   filename='F:\TestDB_log.ldf'
)
use TestDB
go

--1��
create table typeinfo
(
   typeId  int primary key identity(1,1),
   typeName varchar(10) not null,
)

create table logininfo 
(
  LoginId int primary key identity(1,1),
  LoginName  varchar(10) not null unique,
  LoginPwd varchar(20) not null default('1,2,3,4,5,6'),
  Loginsex char(1),
  Loginbrithday datetime,
  LoginVIP  varchar
)

-- ����Ŀ
create database company
on
(
  name='company',
  filename='D:\company.mdf'
)

log on
(
  name='company_log',
  filename='F:\company_log.mdf'
)
use company
go

create table sectioninfo
(
   sectionID  int primary key,
   sectionName  varchar(10) not null
)
create table userinfo
(
userNo  int primary key not null,
userName  varchar(10) unique not null check(userName>4),
userSex   varchar(2) not null check(userSex='��' or userSex='Ů' ),
userAge   int  not null check(userAge>=1 or userAge<=100),
userAddress  varchar(50) default('����'),
userSection  int foreign key references sectionInfo(sectionID)
)
create table workinfo
(
workId int primary key not null,
userId  int foreign key references userInfo(userNo),
workTime datetime not null,
 workDescription varchar(40)  not null check(workDescription='�ٵ�'or workDescription='����'or workDescription='��' or workDescription='����'or workDescription='�¼�')
)

-- ����Ŀ
create database School
on
(
  name='School',
  filename='D:\School.mdf'
)
log on
(
  name='School_log',
  filename='D:\School_log.ldf'
)
use School
go
create table Classinfo
(
  classid int primary key,
  classname varchar(10) not null unique,
  classremark ntext
)
create table studentinfo
(
 stunum int primary key,
 stuname varchar check (len(stuname)>2) unique,
 stusex  varchar(2) default('��') check(stusex='��' or stusex='Ů'),
 stuage  int check(stuage>=15 or stuage<=40) not null,
 stuaddress nvarchar(50) default('�����人'),
 stuclassid int references Classinfo(classid)
)
create table information
(
 number  int primary key,
 kechenname varchar unique not null,
 miaoshu  ntext
)
create table score
(
  scorenum int  primary key,
  scorestuid int references studentinfo(stunum) not null,
  scoreid int references information(number) not null,
  score int check(score>=0 or score<=100)
)
--�ķ�Ŀ
create database house
on
(
 name='house',
  filename='D:\house.mdf'
)
log on
(
  name='house_log',
  filename='D:\house_log.ldf'
)
use house
go

create table tblUser 
(
 userId int primary key identity(1,1),
 userName varchar,
 userTel  nvarchar(10)
)
create table tblHouseType 
(
 typeId  int primary key identity(1,1),
 typName varchar check(typName='����'or typName='��ͨסլ'or typName='ƽ��' or typName='������') not null
)
create table tblQx 
(
 qxId int primary key identity(1,1),
 qxName varchar check(qxName='���'or qxName='����' or qxName='����') not null
)

create table tblHouseInfo
(
  id int primary key identity(1,1),
  userId int foreign key references tblUser (userId),
  zj money,
  shi varchar,
  ting int,
  typeId int foreign key references tblHouseType (typeId),
  qxId int foreign key references tblQx (qxId)
)

