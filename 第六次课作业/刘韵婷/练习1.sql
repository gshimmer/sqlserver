use master
go

create database TestDB
on
(
    name='TestDB',
    filename='C:\SQL\TestDB.mdf',
    size=10MB,
    maxsize=100MB,
    filegrowth=10MB
)
log on
(
    name='TestDB_log',
    filename='C:\SQL\TestDB_log.ldf',
    size=10MB,
    maxsize=100MB,
    filegrowth=10MB
)
go
use TestDB
go
create table TypInfo
(
TypedID int primary key identity(1,1) not null,
TypedName varchar(10) not null
)
go
create table LoginInfo
(
LoginID int primary key identity(1,1) not null,
LoginName varchar(10) unique not null,
LoginPwd varchar(20) default('123456') not null,
LoginSex char(2) check(LoginSex='��' or LoginSex='Ů') not null,
LoginDate varchar(10) not null
)


create database ComPany
on
(
name='ComPany',
filename='C:\SQL\ComPany.mdf',
size=10MB,
maxsize=100MB,
filegrowth=10MB
)
log on
(
name='ComPany_log',
filename='C:\SQL\ComPany_log.ldf',
size=10MB,
maxsize=100MB,
filegrowth=10MB
)
go
use ComPany
go
create table SectionInfo
(
SectionID int primary key identity(1,1) not null,
SectionName varchar(10) not null,
)
go
use ComPany
go
create table UserInfo
(
UserNO int primary key identity(1,1) not null,
UserName varchar(10) unique check(UserName>4) not null,
UserSex varchar(2) check(UserSex='��' or UserSex='Ů') not null,
UserAge int check(UserAge>=1 and  UserAge<=100) not null,
UserAddress varchar(50) default('����') not null,
UesrSection int references SectionInfo(SectionID) not null
)
go
use ComPany
go
create table WorkInfo
(
WorkID int primary key identity(1,1) not null,
UseID int references UserInfo(UserNO) not null,
WorkTime datetime not null,
WorkDescription varchar(40) check(WorkDescription='�ٵ�' or WorkDescription='����' or WorkDescription='����' or WorkDescription='����' or WorkDescription='�¼�') not null
)

create database GuanLi
on
(
name='GuanLi',
filename='C:\SQL\GuanLi.mdf',
size=10MB,
maxsize=100MB,
filegrowth=10MB
)
log on
(
name='GuanLi_log',
filename='C:\SQL\GuanLi_log.ldf',
size=10MB,
maxsize=100MB,
filegrowth=10MB
)
go
use GuanLi
go
create table ClassInfo
(
ClassID int primary key identity(1,1) not null,
ClassName varchar(10) unique not null,
ClassDate datetime not null,
)
go
use GuanLi
go
create table StudenInfo
(
StuID int primary key identity(1,1) not null,
StuName varchar(10)  unique check(StuName>2) not null,
StuSex char(2) default('��') check(StuSex='��' or StuSex='Ů') not null,
StuAge int check(StuAge>=15 and  StuAge<=40) not null,
StuAddress varchar(50) default('�����人') not null
)
go
use GuanLi
go
create table CourInfo
(
CourID int primary key identity(1,1) not null,
CourName  varchar(10) unique not null,
)
go
use GuanLi
go
create table AchInfo
(
AchID int primary key identity(1,1) not null,
AchNO int references StudenInfo(StuID) not null,
AchCour int references CourInfo(CourID) not null,
AchIeve int check(AchIeve>=0 and AchIeve<=100) not null,
)
