use master
go
create database company
on(
name='company',
filename='D:\新建文件夹\company.mdf',
size = 5,
maxsize=10,
filegrowth = 1

)
log on 
(
name='company_log',
filename='D:\新建文件夹\company_log.ldf',
size = 5,
maxsize=10,
filegrowth = 1
)
create table sectionInfo
(
sectionID int primary key,
sectionName varchar(10) not null
) 
create table userInfo
(
userNo int primary key not null,
userName varchar(10) unique not null,
userSex varchar(2) not null check(userSex='男' or userSex='女'),
userAge int  not null check(userAge>=0 and userAge<=100),
userAddress varchar(50) default('湖北'),
userSection int foreign key references sectionInfo(sectionID)
)
create table workInfo
(
workId int primary key not null,
userId int foreign key references userInfo(userNo),
workTime datetime not null,
workDescription varchar(40) not null check(workDescription='迟到' or workDescription='早退' or workDescription='矿工' or workDescription='事假' )
)
