use master
go
create database TestDB
on(
name='typeInfo',
filename='D:\新建文件夹\typeInfo.mdf',
size=5,
maxsize=10,
filegrowth=1
)
log on(
name='typeInfo_LOG',
filename='D:\新建文件夹\typeInfo.ldf',
size=5,
maxsize=10,
filegrowth=1
)
go
use TestDB
go
create table typeInfo
(
typeId int primary key identity(1,1),
typeName varchar(10) not null
)
create table loginInfo
(
LoginId int primary key identity(1,1),
LoginName text not null unique ,
LoginPwd text  not null default('123456'),
Logsex nvarchar(1) default('男'),
birthday datetime,
member nvarchar  
)

