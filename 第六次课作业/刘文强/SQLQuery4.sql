use master
go
create database bbs
on(
name='bbs',
filename='C:\Users\Administrator\Desktop\zy\bbs.mdf',
size=5,
maxsize=10,
filegrowth=1

)
log on(
name='bbs_log',
filename='C:\Users\Administrator\Desktop\zy\bbs_log.ldf',
size=5,
maxsize=10,
filegrowth=1
)
go
use bbs
go
create table bbsUsers
(
bbsUID int,
uName varchar(10),
uSex  varchar(2),
uAge  int,
uPoint  int
)
alter table bbsUsers add constraint dk primary key(bbsUID)
alter table bbsUsers alter column bbsUID int not null
alter table bbsUsers add constraint dw unique(uName)
alter table bbsUsers alter column uName varchar(10) not null
alter table bbsUsers add constraint de check(uSex='男' or uSex='女')
alter table bbsUsers add constraint dr check(uAge>15 or uAge<=60)
alter table bbsUsers alter column uAge int not null
alter table bbsUsers add constraint dd check(uPoint>=0)
alter table bbsUsers alter column uPoint int not null
create table bbsTopic
(
tID  int primary key identity(1,1),
tUID  int foreign key references bbsUsers(bbsUID),
tSID  int foreign key references bbsSection(bbssID),
tTitle  varchar(100) not null,
tMsg  text not null,
tTime  datetime,
tCount  int,
)
create table bbsReply
(
rID  int primary key identity(1,1),
rUID  int foreign key references bbsUsers(bbsUID),
rTID  int foreign key references bbsSection(bbssID),
rMsg  text not null,
rTime  datetime 
)
create table bbsSection
(
bbssID  int,
sName  varchar(10),
sUid   int 
)
alter table bbssID add constraint da  primary key(bbssID)
alter table sName alter column sName varchar(10) not null
alter table sUid add constraint daa foreign key references bbsUsers(bbsUID)

insert into bbsUsers
select '小雨点' , '女 ' ,'20',' 0' union
select '逍遥' , '男', ' 18','  4'union
select '七年级生' , '男', ' 19','  2'
backup database bbs to disk='C:\Users\Administrator\Desktop' 
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection(sName, bbssID)
select '  技术交流 ','   小雨点'union
select ' 读书世界 ','   七年级生'union
select '  生活百科  ','   小雨点'union
select ' 八卦区 ','      七年级生'
--主贴编号  tID  int 主键  标识列，
--	发帖人编号  tUID  int 外键  引用用户信息表的用户编号
	--版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
	--贴子的标题  tTitle  varchar(100) 不能为空
--	帖子的内容  tMsg  text  不能为空
	--发帖时间    tTime  datetime  
	--回复数量    tCount  int
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
select '逍遥','八卦区','范跑跑','谁是范跑跑',' 2008-7-8','1'union
select '七年级生','技术交流','.NET','与JAVA的区别是什么呀？','2008-9-1','2'union
select '小雨点','生活百科','今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10','0'
ALTER table bbsTopic drop constraint dk 
alter table bbsTopic drop constraint FK_bbsTopic
alter table bbsTopic drop column sUID 
update bbsUsers set  uPoint= 30 WHERE   uPoint =20
ALTER table bbsUsers alter Column uPoint int
ALTER TABLE bbsSection DROP CONSTRAINT FK_bbsReply
delete from bbsSection where sName='生活百科'
alter table bbsTopic drop column tCount