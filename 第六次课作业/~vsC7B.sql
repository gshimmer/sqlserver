use TestDB


create database TestDB

on

(

    name='TestDB1',

	filename='E:\TestDB1.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

),--第二个主数据文件
(

    name='TestDB2',

	filename='E:\TestDB2.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)



log on 

(

    name='TestDB_log',

	filename='E:\TestDB_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)

go
select * from typeInfo

create table typeInfo
(
typeId int not null  primary key identity(1,1),
tyoeName varchar(10) not null ,
)
create table loginlnfo
(
Loginld int not null primary key identity(1,1),
LoginName varchar(10) not null unique,
LoginPwd varchar(20) not null default(123456),
LoginSex varchar(2),
LoginBir int ,
LoginType int,
)



create database company
on
(
 name='company',

	filename='E:\company.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)


log on 

(

    name='company_log',

	filename='E:\company_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)

go

create table sectionlnfo

(
sectionID int primary key  not null,
sectionName varchar(10) not null,
)
create table userinfo 
(
serNo int primary key not null ,
userName varchar(10) not null unique check(userName>4),
userSex varchar(2) not null  default('男') check(userSex ='男'or userSex='女'),
userAge int not null check(userAge >=1 and userAge <=100),
userAddress varchar(50) check(userAddress='贵州'),
userSection int foreign key references sectionlnfo(sectionID)
)
create table worklnfo
(
workld int primary key not null ,
userld int foreign key references userinfo (serNo) not null,
workTime datetime not null ,
workDescription varchar(50) check(workDescription ='迟到' or workDescription = '早退'or workDescription ='矿工' or workDescription ='病假' or workDescription ='事假' ),

)
--学校数据库
create database studentlnfo
on
(
 name='studentlnfo',

	filename='E:\studentlnfo.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB
	)


log on 

(

    name='studentlnfo_log',

	filename='E:\studentlnfo_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)

go
--班级表
create table classlnfo
(
classid int primary key not null ,
  classname varchar(5) not null unique,
  classremark ntext
)
--学生信息表
create table information
(
infNumber int primary key not null,
infName varchar(10)check(len(infName)>2) unique,
infSex varchar(2) not null  default('男') check(infSex ='男'or infSex='女'),
infAge int check(infAge>=15 and infAge<=40) not null,
infHome nvarchar(50) default('贵州毕节'),
infclass int references classlnfo(classid)
)
--课程表
create table course
(couNumber int primary key not null,
couName nchar not null unique,
couDescribe text,
)
--成绩表
create table mark
(
marNumber int primary key not null,
marclass int not null,
marCourse int not null,
marGrade int check(marGrade>=0 and marGrade<=100),
)
--房屋数据库
create database house
on
(
 name='house',

	filename='E:\house.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB
	)


log on 

(

    name='house_log',

	filename='E:\house_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)

go
create table tblUser
(
 userID int primary key not null, 
 urtName nvarchar(10) default ('梦林'),
 userCall varchar(11) default('123456'),

 )
 create table tblQx
 (
 qxId int primary key not null,
 qxName nvarchar check (qxName ='武昌' and  qxName ='汉阳' and  qxName ='汉口'),
 )
 create table tbHouseType
 (
 typeID int primary key not null,
 typName nvarchar check(typName  ='别墅' and typName  ='普通住宅' and typName  ='平方' and typName  ='地下室'),
 )

