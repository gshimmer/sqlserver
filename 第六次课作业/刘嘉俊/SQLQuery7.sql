create database bbs
on
(
name='bbs',
filename='D:\sql4.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
log on
(
name='bbs_log',
filename='D:\sql4.ldf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
go

use bbs
go

create table bbsUsers
(
UID int identity(1,1),
uName varchar(10) not null,
uSex varchar(2) not null,
uAge int not null,
uPoint int not null
)

alter table bbsUsers add constraint PK_UID primary key(UID) 
alter table bbsUsers add constraint uName unique(uName)
alter table bbsUsers add constraint uSex check(uSex='男' or uSex='女')
alter table bbsUsers add constraint uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint uPoint check(uPoint>=0)

create table bbsTopic 
(
tID int primary key identity(1,1),
tUID int foreign key references bbsUsers(UID),
tSID int foreign key references bbsSection(sID),
tTitle varchar(100) not null,
tMsg text not null,
tTime datetime ,
tCount int 
)

create table bbsReply
(
rID int primary key identity(1,1),
rUID int foreign key references bbsUsers(UID),
rTID int foreign key references bbsTopic(tID),
rMsg text not null,
rTime datetime 
)

create table bbsSection
(
sID int identity(1,1),
sName varchar(10) not null,
sUid int 
)

alter table bbsSection add constraint Pk_sID primary key (sID) 
alter table bbsSection add constraint CK_sUid foreign key (sUid) references bbsUsers(UID)

insert into bbsUsers values('小雨点','女','20','0')
insert into bbsUsers values('逍遥','男','18','4')
insert into bbsUsers values('七年级生','男','19','2')

select uName,uSex from bbsUsers

select uName,uPoint into bbsPoint from bbsUsers

select * from bbsSection

insert into bbsSection (sName,sUid)	
values('技术交流',1),
	('读书世界',3),
	('生活百科',1),
	('八卦区',3)

insert into bbsTopic( tUID,tSID,tTitle,tMsg,tTime,tCount )
values (2,4,'范跑跑','谁是范跑跑','2008-7-8','1'),(3,1,'.NET','与JAVA的区别是什么','2008-9-1','2'),
(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10','0')

insert into bbsReply(rMsg,rTime,rUID) values
('他是范跑跑','2008-7-9',1),
('不知道','2008-9-2',2),
('不知道','2008-9-11',3)

alter table bbsReply drop constraint FK__bbsReply__rUID__30F848ED
alter table bbsTopic drop constraint FK__bbsTopic__tUID__2D27B809
delete bbsUsers where uName='逍遥'

update bbsUsers set upoint =12 where uName='小雨点'

alter table bbsTopic drop constraint FK__bbsTopic__tSID__2E1BDC42
delete bbsSection where sName = '生活百科'

delete bbsReply