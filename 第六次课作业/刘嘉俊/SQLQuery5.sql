create database TestDB
on
(
name='TestDB',
filename='D:\sql.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
log on
(
name='TestDB_log',
filename='D:\sql.ldf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
go

use TestDB
go

create table typeInfo
(
typeld int primary key identity(1,1),
typeName varchar(10) not null,
)

create table loginInfo
(
LoginId int primary key identity(1,1),
LoginName varchar(10)unique not null,
LoginPwd varchar(20) default('123456') not null,
LoginSex char(2),
LoginBirth varchar(20),
LoginType varchar(20)
)




create database company
on
(
name='company',
filename='D:\sql1.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
log on
(
name='company_log',
filename='D:\sql1.ldf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
go

use company 
go

create table sectionInfo
(
sectionID int primary key,
sectionName varchar(10) unique not null
)

create table userInfo
(
userNo int primary key not null,
userName varchar(10) unique  not null,
userSex varchar(2) check(userSex='��'or userSex='Ů') not null,
userAge int check(userAge>=1 and userAge<=100) not null,
userAddress varchar(50) default('����'),
userSection int foreign key references sectionInfo(sectionID) 
)

create table workInfo
(
workId int primary key not null,
userId int foreign key references userInfo(userNo) not null,
workTime datetime not null,
workDescription varchar(40) check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�') not null 
)

create database Student
on
(
name='Student',
filename='D:\sql2.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
log on
(
name='Student_log',
filename='D:\sql2.ldf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
go

use Student
go

create table class
(
classid int primary key identity(1,1),
classtime datetime not null,
)

create table student
(
stuName varchar(20) unique,
stuSex char(2) default('��') check(stuSex='��' or stuSex='Ů') not null,
stuAge int check(stuAge>=15 and stuAge<=40) not null,
stuAddress varchar(20) default('�����人'),
classNO int 
)

create table course
(
courseID int primary key ,
courseName varchar(20) unique not null,
coursedescribe varchar(20)
)

create table score
(
scoreID int primary key,
stuID int not null,
courseID int not null,
score int check(score>=0 and score<=100)
)




create database tbl 
on
(
name='tbl',
filename='D:\sql3.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
log on
(
name='tbl_log',
filename='D:\sql3.ldf',
size=5mb,
maxsize=50mb,
filegrowth=10mb
)
go

use tbl 
go

create table tblUser
(
userId int primary key,
userName varchar(20) not null,
userTel int unique not null,
)

create table tblHouseType
(
typeld money check(typeld>=0 and typeld<=2000000) not null,
typName varchar(20) not null
)

create table tblQX
(
qxld  varchar(20) not null,
qxName varchar(20) unique not null,
)

create table tblHouseInfo
(
id int primary key,
userid int unique not null,
typeid int unique not null,
qxld varchar(20) not null
)
