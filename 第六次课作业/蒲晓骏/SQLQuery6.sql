create database TestDB
on
(
	FileName='F:\homework\TestDB.mdf',
	Name='TestDB',
	size=5MB,
	maxsize=5MB,
	filegrowth=3MB
)
log on
(
	FileName='F:\homework\TestDB_log.ldf',
	Name='TestDB_log',
	size=5MB,
	maxsize=5MB,
	filegrowth=3MB
)
go

use TestDB

go

create table typeInfo
(
	typeID int primary key identity(1,1),
	typeName varchar(10) not null,
)

create table loginInfo
(
	LoginID int primary key identity(1,1),
	LoginName varchar(10) not null unique,
	LoginPwd varchar(20) not null default(123456),
	LogSex nvarchar(1),
	Logbirthday date,
	LogVIP nvarchar(10),
)


create database Company
on
(
	FileName='F:\homework\Company.mdf',
	Name='Company',
	size=5MB,
	maxsize=5MB,
	filegrowth=3MB
)
log on
(
	FileName='F:\homework\Company_log.ldf',
	Name='Company_log',
	size=5MB,
	maxsize=5MB,
	filegrowth=3MB
)
go
use Company
go


create table sectionInfo
(
	sectionID int identity(1,1) primary key  ,
	sectionName varchar(10) not null,
)

create table userInfo
(
	userNo int identity(1,1) primary key not null,
	userName varchar(10) unique not null check(len(userName)>4),
	userSex varchar(2) not null check(userSex in('��','Ů')),
	userAge int not null check(userAge>=1 or userAge<=100),
	userAddress varchar(50) default('����'),
	userSection int foreign key references sectionInfo(sectionID)
)

create table workInfo
(
	workID int identity(1,1) primary key not null,
	userID int foreign key references userInfo(userNo),
	workTime datetime not null,
	workDescription varchar(40) not null check(workDescription in ('�ٵ�','����','����','����','�¼�'))
)



create database SMS
on
(
	FileName='F:\homework\SMS.mdf',
	Name='SMS',
	size=5MB,
	maxsize=5MB,
	filegrowth=3MB
)
log on
(
	FileName='F:\homework\SMS_log.mdf',
	Name='SMS_log',
	size=5MB,
	maxsize=5MB,
	filegrowth=3MB
)
go
use SMS
go

create table ClassInfo
(	
	ClassID int primary key identity(1,1),
	ClassName nvarchar(10) not null unique,
	StartTime date not null,
	ClassDescribe text,
)

create table StudentInfo
(
	StuID int primary key identity(1,1),
	StuName nvarchar(10) unique check(len(StuName)>2),
	StuSex nvarchar(1) check(StuSex in ('��','Ů')) default('��') not null,
	StuAge int check(StuAge>=15 or StuAge<=40) not null,
	StuAddress nvarchar(30) default('�����人'),
	ClassID int
)


create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar not null unique,
	CourseS text
)

create table Score
(
	ScoreID int primary key identity(1,1),
	StudentID int not null,
	Course int not null,
	Score int check(Score>=0 or Score<=100)	
)


create database HouseRental
on
(
	fileName='F:\homework\HouseRental.mdf',
	Name='HouseRental',
	size=5MB,
	Maxsize=5MB,
	filegrowth=1MB
)
log on
(
	fileName='F:\homework\HouseRental_log.mdf',
	Name='HouseRental_log',
	size=5MB,
	Maxsize=5MB,
	filegrowth=1MB
)

go

use HouseRental

go


create table tblHouseInfo
(
	HouseID int primary key identity(1,1),
	Housedesc text ,
	UserID int not null,
	HouseRental money not null,
	Houseshi int not null,
	Houseting int not null,
	TypeID int not null,
	QxUD int not null
)

create table tblUser
(
	UserID int,
	UserName nvarchar(10) not null,
	UserTel nvarchar(11)
)

create table tblHouseType
(
	TypeID int not null,
	TypeName nvarchar(10)
)

create table tblQx
(
	QxUD int not null,
	QxName nvarchar(10)
)


