use master
go
create database TestDB
on
(
	name='TestDB',
	filename='D:\TestDB.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth =10%
)
log on
(
	name='TestDB_log',
	filename='D:\TestDB.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth =10%
)
use TestDB
go

create table typeInfo
(
	typeId int primary key identity (1,1),
	typeName varchar(10) not null,
)
create table loginInfo
(
	LoginId int primary key identity (1,1),
	LoginName nvarchar(10) unique not null,
	LoginPwd nvarchar(20) default 123456 not null,
	sex nvarchar(1) check (sex='��' or sex='Ů'),
	Birthday datetime not null,
	Member int not null,
)
create database company
on
(
	name='company',
	filename='D:\company.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth =10%
)
log on
(
	name='company_log',
	filename='D:\company.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth =10%
)
create table sectionInfo
(
	sectionID int primary key identity (1,1),
	sectionName varchar(10) not null,
)
create table userInfo
(
	userNo int primary key identity (1,1) not null,
	userName  varchar(10) unique check( userName>4) not null,
	userSex varchar(2) check(userSex='��'or userSex='Ů') not null,
	userAge int check (userAge>1and userAge<100) not null,
	userAddress varchar(50) default'����',
	userSection int references sectionInfo(sectionID),
)
create table workInfo
(
	workId int primary key identity (1,1) not null,
	userId int references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription varchar(40) not null check (workDescription= '����'or workDescription='�ٵ�'or workDescription='����' or workDescription='����' or workDescription='�¼�')
	
)
go
create database  Student
use Student
go
create table Class
(
	classid int primary key identity  (1,1),
	classname nvarchar(20) unique not null,
	Opentime datetime not null,
	classdescription ntext,
)
create table Students
(
	stunumber int primary key identity (1,1),
	stuname nvarchar(10) unique check(stuname>2),
	stusex nvarchar(1) default'��' check(stusex='��'or stusex='Ů') not null,
	stuage int check(stuage>=15and stuage<=40) not null,
	stuaddress nvarchar(20) default'�����人',
	classid int references Class(classid),
)
create table course
(
	courseid int primary key identity (1,1),
	coursename  nvarchar(10) unique not null,
	course text ,
)
create table performance
(
	performanceid int primary key identity (1,1),
	stunumber int references  Students(stunumber) not null,
	courseid int references  course(courseid) not null,
	score int check (score>=0 and score<=100),
)
create database house
go
create table tblUser
(
	userId int primary key identity(1,1),
	userName nvarchar(20),
	userTel char(20),
)
create table tblHouseType
(
	typeId int primary key identity(1,1),
	typName nvarchar(10) check(typName='����'or typName='��ͨסլ' or typName='ƽ��'or typName='������' ),
)
create table tblQx
(
	qxId int primary key identity(1,1),
	qxName nvarchar(20) check (qxName='���'or qxName='����'or qxName='����'),
)
create table tblHouseInfo
(
	tblHouseid int primary key identity(1,1),
	describe text,
	userId int foreign key references tblUser(userId),
	rent int,
	shishu int,
	tingshu int,
	typeId int foreign key references tblHouseType(typeId),
	qxId int foreign key references tblQx(qxId)
)