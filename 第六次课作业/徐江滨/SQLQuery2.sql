-- 一、
create database bbs
on
(
 name='bbs',
  filename='F:\bbs.mdf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
log on
(
  name='bbs_log',
  filename='F:\bbs_log.ldf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
use bbs
go

create table bbsUsers
(
  uuID int identity(1,1) ,
  uName varchar(10) not null,
  uSex  varchar(2) not null,
  uAge  int not null,
  uPoint  int not null 
)

alter table bbsUsers add constraint PK primary key (uuID) 
alter table bbsUsers add constraint UK unique(uName) 
alter table bbsUsers add constraint CK check(uSex='男' or uSex='女' )
alter table bbsUsers add constraint CK1 check(uAge>=15 or uAge<=60 )
alter table bbsUsers add constraint CK2 check(upoint>=0)

create table bbsSection
(
  sID  int not null ,
  sName  varchar(10) not null,
  sUid   int 
) 
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign  key (sUid)references bbsUsers(uuID)



create table bbsTopic
(
 tID  int primary key,
 tUID  int foreign key references bbsUsers(uuID),
 tSID int foreign key references bbsSection(sID),

)
alter table bbsTopic add  tTitle varchar(100) not null
alter table bbsTopic add  tMsg  text not null
alter table bbsTopic add  tTime  datetime
alter table bbsTopic add  tCount  int

create table bbsReply
(
  rID  int primary key,
  rUID  int foreign key references bbsUsers(uuID),
  rTID  int foreign key references bbsTopic(tID),
  rMsg  text not null,
  rTime  datetime 
)
insert into bbsUsers(uName,uSex,uAge,uPoint)
select'小雨点','女',20,0 union
select'逍遥','男',18,4 union
select'七年级生','男',19,2
select * from bbsUsers

--2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，
--提示查询部分列:select 列名1，列名2 from 表名
select uName,uPoint into bbsPoint  from bbsUsers

insert into bbsSection(sName, sID)
select '技术交流'  , 1union
select '读书世界' ,2 union
select '生活百科' ,3 union
select ' 八卦区' ,4
select * from  bbsSection

-- 4.向主贴和回帖表中添加几条记录
--   发帖人    板块名  帖子标题
-- 帖子内容 帖时间   回复数量
insert into bbsTopic( tid,tUID,tSID,tTitle,tMsg,tTime,tCount )
select 1,3, 4,'范跑跑','谁是范跑跑','2008-7-8',1 union
select 2,2, 1,' .NET','与JAVA的区别是什么呀？','2008-9-1',2 union
select 3,1, 3,' 今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0 
select * from bbsTopic

select * from  bbsReply
insert into bbsReply(rid,rUID,rMsg,rTime)
select 1,2,'钟老师是范跑跑','2008-7-8'union
select 2,1,'没有区别就是最大的区别','2008-7-8'union
select 3,3,'今年流行上海滩','2008-7-8'

delete bbsUsers where uName='小雨点' --深感抱歉 删错人了 
alter table  bbsReply drop  FK__bbsReply__rUID__4AB81AF0
alter table bbsTopic drop FK__bbsTopic__tUID__46E78A0C
select * from bbsReply
--给逍遥加分10
update bbsUsers set upoint=12 where uName='逍遥'
--删除生活百科板块
alter table bbsTopic drop FK__bbsTopic__tSID__47DBAE45
delete bbsSection where sName='生活百科'
--删除所有回帖
delete  bbsReply 