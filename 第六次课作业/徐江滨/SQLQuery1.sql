CREATE DATABASE Students
ON
(NAME='Students_data',
FILENAME='C:\app\Students_data.mdf',
SIZE=5MB,
MAXSIZE=100MB,
FILEGROWTH=10%
)
LOG ON
(
NAME='Students_log',
FILENAME='C:\app\Students_log.ldf',
SIZE=1MB,
FILEGROWTH=5MB
)
use Students
go



-- 1��
create table typeInfo
(
typeId int primary key identity(1,1),
typeName varchar(10) not null,
)
create table loginInfo
(
LoginId int primary key identity(1,1),
LoginName  varchar(10) not null unique,
LoginPwd varchar(20) not null default('1,2,3,4,5,6'),
Logsex char(1),
Logbrithday datetime,
Loghuiyuan  varchar
)


-- 2��
create database company
on
(
name='company_data',
filename='C:\xxx\company_data.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10%
)

log on
(
name='company_log',
filename='C:\xxx\company_log.ldf',

size=5mb,
maxsize=50mb,
filegrowth=10%
)

use company
go

create table sectionInfo
(
sectionID  int primary key,
sectionName  varchar(10) not null
)
create table userInfo
(
userNo int primary key not null,
userName varchar(10) unique not null check(userName>4),
userSex varchar(2)  not null check(userSex='��'or userSex='Ů'),
   userAge   int  not null check(userAge>=1 or userAge<=100),
	userAddress  varchar(50) default('����'),
	userSection  int foreign key references sectionInfo(sectionID)
)
create table workInfo
(
   workId int primary key not null,
   userId  int foreign key references userInfo(userNo),
   workTime datetime not null,
   workDescription varchar(40)  not null check(workDescription='�ٵ�'or workDescription='����'or workDescription='��' or workDescription='����'or workDescription='�¼�')
)

-- 3��
create database School
on
(
  name='School',
  filename='F:\School.mdf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
log on
(
  name='School_log',
  filename='F:\School_log.ldf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
use School
go
create table Classinfo
(
  classid int primary key,
  classname varchar(10) not null unique,
  classremark ntext
)
create table studentinfo
(
 stunum int primary key,
 stuname varchar check (len(stuname)>2) unique,
 stusex  varchar(2) default('��') check(stusex='��' or stusex='Ů'),
 stuage  int check(stuage>=15 or stuage<=40) not null,
 stuaddress nvarchar(50) default('�����人'),
 stuclassid int references Classinfo(classid)
)
create table information
(
 number  int primary key,
 kechenname varchar unique not null,
 miaoshu  ntext
)
create table score
(
  scorenum int  primary key,
  scorestuid int references studentinfo(stunum) not null,
  scoreid int references information(number) not null,
  score int check(score>=0 or score<=100)
)
--4��
create database fangwu
on
(
 name='fangwu',
  filename='F:\fangwu.mdf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
log on
(
  name='fangwu_log',
  filename='F:\fangwu_log.ldf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
use fangwu
go

create table tblUser 
(
 userId int primary key identity(1,1),
 userName varchar,
 userTel  nvarchar(10)
)
create table tblHouseType 
(
 typeId  int primary key identity(1,1),
 typName varchar check(typName='����'or typName='��ͨסլ'or typName='ƽ��' or typName='������') not null
)
create table tblQx 
(
 qxId int primary key identity(1,1),
 qxName varchar check(qxName='���'or qxName='����' or qxName='����') not null
)

create table tblHouseInfo
(
  id int primary key identity(1,1),
  userId int foreign key references tblUser (userId),
  zj money,
  shi varchar,
  ting int,
  typeId int foreign key references tblHouseType (typeId),
  qxId int foreign key references tblQx (qxId)
)



