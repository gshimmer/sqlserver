use master
go
--先创建数据库和表以及约束
--创建一个数据库用来存放某论坛的用户和发帖信息，数据库的名称为bbs，包含1个数据文件1个日志文件
--全部存放在D盘中，初始大小，增长和最大大小自己设定
--注意以下4张表的创建顺序，要求在创建bbsUser和bbsSection表时不添加约束，创建完后单独添加约束，其它的表创建时添加约束
create database bbs
on
(
	name='bbs',
	filename='D:\SIX2\bbs.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10%

)
,
(
	name='bbs2_DATA',
	filename='D:\SIX2\bbs2_DATA.Ndf',
	size=1MB,
	maxsize=100MB,
	filegrowth=10%
)
log on
(
	name='bbs_ldf',
	filename='D:\SIX2\bbs_log.ldf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10%
)

use bbs
go
--用户信息表（bbsUsers）
create table bbsUsers
(
	--用户编号  UID int 主键  标识列
	UID int primary key identity(1,1),
	--用户名    uName varchar(10)  唯一约束 不能为空
	uName varchar(10) unique not null,
	--性别      uSex  varchar(2)  不能为空 只能是男或女
	uSex varchar(2) not null check (uSex='男' or uSex='女'),
	--年龄      uAge  int  不能为空 范围15-60
	uAge int not null check(uAge>=15 and uAge<=60),
	--积分      uPoint  int 不能为空  范围 >= 0
	uPoint int not null check(uPoint>=0) 
)

select *from bbsUsers
--版块表（bbsSection）
create table bbsSection
(
	--版块编号  sID  int 标识列 主键
	sID int identity(1,1) primary key,
	--版块名称  sName  varchar(10)  不能为空
	sName varchar(10) not null,
	--发帖人编号  tUID  int 外键  引用用户信息表的用户编号
	sUid int foreign key references bbsUsers(UID)
)

create table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int foreign key references bbsUsers(UID),
	tSID int foreign key references bbsSection(sID),
	tTitle  varchar(100) not null,
	rMsg text not null,
	rTime datetime ,
	tCount int
)


create table bbsReply
(
	rID int primary key identity(1,1),
	rUID INT foreign key references bbsUsers(UID),
	rTID INT foreign key references bbsTopic(tID),
	rMsg text not null,
	rTime datetime
)
select * from bbsUsers
select * from bbsSection
select *from bbsTopic
select * from bbsReply

insert into bbsUsers(uName, uSex,uAge,uPoint)
values ('小雨点','女',20,0)
insert into bbsUsers(uName, uSex,uAge,uPoint)
values ('逍遥','男',18,4)
insert into bbsUsers(uName, uSex,uAge,uPoint)
values ('七年级生','男',19,2)

insert into bbsSection(sName , sUid)
values('技术交流',1)
insert into bbsSection(sName , sUid)
values('读书世界',3)
insert into bbsSection(sName , sUid)
values('生活百科',1)
insert into bbsSection(sName , sUid)
values('八卦区',3)

insert into bbsTopic(tUID ,tSID,tTitle,rMsg,rTime,tCount)
values (2,5,'范跑跑','谁是范跑跑','2008-7-8',1)
insert into bbsTopic(tUID ,tSID,tTitle,rMsg,rTime,tCount)
values (3,2,'.NET','与JAVA的区别是什么','2008-9-1',2)
insert into bbsTopic(tUID ,tSID,tTitle,rMsg,rTime,tCount)
values (1,4,'今年夏天流行什么','有谁知道今年夏天流行什么呀？','2008-9-10',0)

insert into bbsReply(rUID,rTID,rMsg,rTime)
values(1,3,'范跑跑','2008-10-2')
insert into bbsReply(rUID,rTID,rMsg,rTime)
values(2,2,'区别不大','2008-10-2')
insert into bbsReply(rUID,rTID,rMsg,rTime)
values(3,1,'蓝色','2008-10-2')

select uName,uPoint into bbsPoint from bbsUsers --备份uName uPoint 到新表bbsPoint

alter  table bbsTopic drop constraint FK__bbsTopic__tUID__2D27B809
alter  table bbsReply drop constraint FK__bbsReply__rUID__30F848ED
delete from bbsUsers where uName='逍遥'
update bbsUsers set upoint=10 where uName='小雨点'

alter table bbsTopic drop FK__bbsTopic__tSID__2E1BDC42
delete bbsSection where sName='生活百科'

truncate table bbsReply
