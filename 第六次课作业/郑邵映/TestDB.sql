create database TestDB
on
(
    name='TestDB',
	filename='D:\test\TestDB.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10MB
)

log on 
(
    name='TestDB_log',
	filename='D:\test\TestDB_log.ldf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10MB
)
use TestDB
go
create table typrlnfo
(
typeld int,
typeName varchar(10) not null
)
create table loginlngo
(
Loginld int primary key identity (1,1),
LoginName varchar(10) not null unique,
LoginPwd varchar(20) not null default('1,2,3,4'),
LogSex char(1) default ('��') check (LogSex in ('��','Ů')),
Logbirthday date,
LogFind int 

)