use master
go

create database TestDB
on
(
	name='TestDB',
	filename='D:\SQL作业\SQL作业6\TestDB.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)

log on
(
	name='TestDB_log',
	filename='D:\SQL作业\SQL作业6\TestDB_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use TestDB
go
create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null
)
go

use TestDB
create table loginInfo
(
	LoginId int primary key identity(1,1),
	LoginName char(10) unique(LoginName) not null,
	LoginPwd char(20) default(123456) not null,
	Sex char(2) not null,
	Birthday char(12) not null,
	Category nchar(15) not null
)
go


use master
create database company
on
(
	name='company',
	filename='D:\SQL作业\SQL作业6\company.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)

log on 
(
	name='company_log',
	filename='D:\SQL作业\SQL作业6\company_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use company
create table sectionInfo
(
	 sectionID int primary key identity(1,1),
	  sectionName varchar(10) not null
)
go

use company
create table userInfo
(
	userNo int primary key identity(1,1) not null,
	userName varchar(10) unique(userName) check(len(userName)>=4) not null,
	userSex varchar(2) check(userSex='男' or userSex='女') not null,
	userAge int check(userAge>=1 and userAge<=100) not null,
	userAddress varchar(50) default('湖北'),
	userSection int constraint FK_sectionInfo_sectionID references sectionInfo(sectionID)
)
go

use company
create table workInfo
(
	workId int primary key identity(1,1) not null,
	userId  int constraint FK_userInfo_userNo references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription varchar(40) check(workDescription='迟到' or workDescription='早退' or workDescription='旷工' or workDescription='病假' or workDescription='事假')
)
go


use master
create database manage
on
(
	name='manage',
	filename='D:\SQL作业\SQL作业6\manage.mdf',
	size=5mb,
	maxsize=5mb,
	filegrowth=10%
)

log on
(
	name='manage_log',
	filename='D:\SQL作业\SQL作业6\manage_log.ldf',
	size=5mb,
	maxsize=5mb,
	filegrowth=10%
)
go

use manage
create table classInfo
(
	 classid int primary key identity(1,1),
	 classname char(15) unique(classname) not null,
	 opentime datetime not null,
	 classdescribe text
)
go

use manage
create table stuinfo
(
	stuid int primary key identity(1,1),
	stuname char(8) unique(stuname) check(len(stuname)>2),
	stusex char(2) default('男') check(stusex='男' or stusex='女') not null,
	stuage char(5) check(len(stuage)>=15 and len(stuage)<=40) not null,
	homeadress nchar(30) default('湖北武汉'),
	classid int
)
go

use manage
create table CourseInfo
(
	CourseID  int primary key identity(1,1),
	CourseName nchar(10) unique(CourseName) not null,
	CourseDes text
)
go

use manage
create table ScoreInfo
(
	ScoreID int primary key identity(1,1),
	ScoreStuID int not null,
	ScoreCourID int not null,
	Score char(5) check(score>=1 and score<=100)
)
go


use master
create database Home
on
(
	name='Home',
	filename='D:\SQL作业\SQL作业6\Home.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)

log on
(
	name='Home_log',
	filename='D:\SQL作业\SQL作业6\Home_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Home
create table tblUser 
(
	userId int primary key identity(1,1),
	userName nchar(10) not null,
	userTel char(20)
)
go

use Home
create table tblHouseType
(
	typeId int primary key identity(1,1),
	typName nchar(10) check(typName='别墅' or typName='普通住宅' or typName='平房' or typName='地下室') not null
)
go

use Home
create table tblQx
(
	qxId int primary key identity(1,1),
	qxName char(20) check(qxName='武昌' or qxName='汉阳' or qxName='汉口') not null
)
go

use Home
create table tblHouseInfo
(
	id int primary key identity(1,1),
	userId int constraint FK_tblUser_userId references tblUser(userId),
	zj money not null,
	shi varchar(5),
	ting varchar(5),
	typeId int constraint FK_tblHouseType_typeId references tblHouseType(typeId),
	qxId int constraint FK_tblQx_qxId references tblQx(qxId)
)
go