use master
go
create database testdb
on
(
name='testdb',
filename='D:\testdb.mdf',
size=8mb,
maxsize=2000mb,
filergrowth=1mb
)
log on
(
name='testdb',
filename='D:\testdb_log.ldf',
size=8mb,
maxsize=2000mb,
filergrowth=1mb
)
create table typeinfo
(
typeld int primary key identity(1,1) not null, 
typename varchar(10) not null,
)
create table logininfo
(
loginld int  primary key identity(1,1) not null,
loginname char(10) not null unique,
loginpwd char(20) not null default(123456),
loginsex char(2) check(loginsex='��' or loginsex='Ů'),
loginbrithday int,
LogintypeName  char(10) not null
)
create database company
on
(
name='company',
filename='D:\company.mdf',
size=8mb,
maxsize=2000mb,
filergrowth=1mb
),
(
name='company',
filename='D:\company_log.1df',
size=8mb,
maxsize=2000mb,
filergrowth=1mb
)
create table sectioninfo
(
sectionid int  primary key identity(1,1),
sectionname varchar(10) not null
)
create table userinfo
(
userno int  primary key not null identity(1,1),
username varchar(10) unique not null check(username>4),
usersex varchar(2) not null check(usersex='��'or usersex='Ů'),
useraddress varchar(50) default('����'),
usersction int references sectioninfo(sectionid),
)
create table worklnfo
(
workld int  primary key not null,
userld int references userinfo(userno) not null,
worktime datetime not null,
workdescription varchar(40) not null check(workdescription in('�ٵ�','����','����','����','�¼�')),
)
create database studentmange
on
(
name='studentmange',
filename='D:\studentmange.mdf',
size=8mb,
maxsize=2000mb,
filergrowth=1mb
),
(
name='studentmange',
filename='D:\studentmange_log.ldf',
size=8mb,
maxsize=2000mb,
filergrowth=1mb
)
create table classlnfo 
(
classid  int primary key identity(1,1),
classname char(6) not null unique,
time date not null,
classspeak text
)
create table studentinfo 
(
stuid int primary key identity(1,1),
stuname nchar(10) check(stuname>2),
stusex nchar(1) not null default('��') check(stusex in('��','Ů')),
stuage int not null check(stuage>=15 and stuage<=40),
stuaddress nvarchar(4) default('�����人'),
stuclassid int references classinfo(classid),
)
create table courseinfo 
(
courseid int primary key identity(1,1),
coursename nchar(10) not null unique,
coursespeak text 
)
create table scoreninfo 
(
scoreid int primary key identity(1,1),
scorestuid int not null,
scorecourseid int not null,
score int check(score>=0 and score<=100),
)
create database house 
on
(
name='house',
filename='D:\house.mdf',
size=8mb,
maxsize='2000mb',
filegrowth=1mb
),
(
name='house',
filename='D:\house_log.1df',
size=8mb,
maxsize='2000mb',
filegrowth=1mb
)
create table tbluser
(
userid int primary key identity(1,1),
username nvarchar(10) not null,
usertel char(11) not null,
)
create table tblhousetype
(
typed int primary key identity(1,1),
qxname nchar(4) not null
)
create table tbllqx
(
qxid int primary key identity(1,1),
qxname nchar(2) not null
)
create table tblhouseinfo
(
id int primary key identity(1,1),
descl nvarchar(10) not null,
userid int,
zj int,
shi int,
ting int,
typeid int,
qxid int,
)