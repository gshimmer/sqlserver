create database TestDB
on
( 
    name='TestDB',
	filename='D:\TestDB.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
),
(
    name='TestDB_ndf',
	filename='D:\TestDB_ndf.ndf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='TestDB_log',
	filename='D:\TestDB_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use TestDB
go
create table typeInfo
( 
   typeId int primary key identity(1,1),
   typeName varchar(10) not null, 
)
create table loginInfo
( LoginId int primary key identity(1,1),
  LoginName nvarchar(10) unique not null,
  LoginPwd nvarchar(10) default(123456) not null,
  LoginSex nvarchar(1),
  LoginBirth date,
  LoginMember nvarchar(20),
)
create database company
on
( 
    name='company',
	filename='D:\company.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
),
(
    name='company_ndf',
	filename='D:\company_ndf.ndf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='company_log',
	filename='D:\company_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use company
go
create table sectionInfo
( 
    sectionID int identity primary key,
	sectionName varchar(10) not null,
)
create table userInfo
( 
    userNo int identity primary key not null,
	userName varchar(10)  unique check (userName>4) not null,
	userSex varchar(2) check(userSex='��' or userSex='Ů') not null,
	userAge int check(userAge>=1 and userAge<=100) not null,
	userAdress varchar(50) default('����'),
	userSection int foreign key references sectionInfo(sectionID)
)
create table workInfo
(   
    workID int identity primary key not null,
	userID int foreign key references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription varchar(40) check(workDescription in('�ٵ�','����','����','����','�¼�')) not null,

)
create database studentManSys
on
(
  name='studentManSys',
	filename='D:\studentManSys.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
),
(
	name='studentManSys_ndf',
	filename='D:\studentManSys_ndf.ndf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='studentManSys_log',
	filename='D:\studentManSys_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use studentManSys
go
create table classInfo
(
    classId int identity primary key,
	className varchar(20) unique not null,
	classStart date not null,
	classMs text
)
create table studentInfo
( 
      stuID int identity primary key,
	  stuName varchar(8) check(stuName>2) unique,
	  stuSex nvarchar(1) default('��') check (stuSex='��' or stuSex='Ů') not null,
	  stuAge int check(stuAge>=15 and stuAge<=40) not null,
	  stuAdress nvarchar(200) default('�����人'),
	  bjbh int references classinfo(classid)
 )
 create table courseInfo
 (  
    courseID int identity primary key,
	courseName varchar(20) unique not null,
	kcms nvarchar(200)
 )
 create table markInfo
 ( 
    markID int identity primary key,
	stuID int not null,
	courseID int not null,
	mark int check(mark>=0 and mark<=100)
 )
 create table tblUser
(
	userId int identity primary key,
	userName nvarchar(8) not null,
	userTel char(11) not null
)
create table tblHouseType 
(
	typeId int identity primary key,
	typName nvarchar(8) not null
)
create table tblQx 
(
	qxId int identity primary key,
	qxName nvarchar(8) not null
)
create table tblHouseInfo
(
	id int identity primary key,
	descc nvarchar(8) not null,
	userld int,	
	zj int,	
	shi int,	
	ting int,	
	typeld int,	
	qxld int
)