----练习2
--一、先创建数据库和表以及约束

--	1.创建一个数据库用来存放某论坛的用户和发帖信息，数据库的名称为bbs，包含1个数据文件1个日志
--	文件，数据文件和日志文件全部存放在D盘中，初始大小，增长和最大大小自己设定
use master
go

create database bbs
on(

	name='bbs.mdf',
	filename='D:\sql\bbs.mdf',
	size=8mb,
	maxsize=80mb,
	filegrowth=10%
)
log on(
	name='bbs_log',
	filename='D:\sql\bbs_log.ldf',
	size=8mb,
	maxsize=80mb,
	filegrowth=10%

)
go

use bbs
go
alter table bbsUsers add constraint FK unique (uName)
alter table bbsSection add constraint NK foreign key (sUid) references bbsUsers( UID)
--	2.创建表

--	注意以下4张表的创建顺序，要求在创建bbsUser和bbsSection表时不添加约束，创建完后单独添加约束，其它的表创建时添加约束

--	用户信息表（bbsUsers）
--	用户编号  UID int 主键  标识列
--	用户名    uName varchar(10)  唯一约束 不能为空
--	性别      uSex  varchar(2)  不能为空 只能是男或女
--	年龄      uAge  int  不能为空 范围15-60
--	积分      uPoint  int 不能为空  范围 >= 0
create table bbsUsers
(
	UID int primary key identity(1,1),
	uName varchar(10) unique not null,
	uSex varchar(2) not null check(uSex='男' or uSex='女'),
	uAge int not null check(uAge>=15 and uAge<=60 ),
	uPoint int not null check(uPoint>=0),
)

--	1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--	  小雨点  女  20  0
--	  逍遥    男  18  4
--	  七年级生  男  19  2
insert into bbsUsers(uName,uSex,uAge,uPoint)
values('小雨点','女','20',0),
('逍遥','男','18',4),
('七年级','男','19',2)


create table bbsSection
(
	sID int primary key identity(1,1),
	sName varchar(10) not null,
	sUid int foreign key references bbsUsers(UID)
)



--+	主贴表（bbsTopic）
--	主贴编号  tID  int 主键  标识列，
--	发帖人编号  tUID  int 外键  引用用户信息表的用户编号
--	版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
--	贴子的标题  tTitle  varchar(100) 不能为空
--	帖子的内容  tMsg  text  不能为空
--	发帖时间    tTime  datetime  
--	回复数量    tCount  int
create table   bbsTopic
(
	tID int primary key identity(1,1),
	tUID int foreign key references bbsUsers(UID),
	tSID int foreign key   references bbsSection(sID),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int

)


--+	回帖表（bbsReply）
--	回贴编号  rID  int 主键  标识列，
--	回帖人编号  rUID  int 外键  引用用户信息表的用户编号
--	对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
--	回帖的内容  rMsg  text  不能为空
--	回帖时间    rTime  datetime 
create table bbsReply
(
	rID int primary key identity(1,1),
	rUID int foreign key references bbsUsers(UID),
	rTID int foreign key references bbsTopic(tID),
	rMsg text not null,
	rTime datetime

)
	

--	版块表（bbsSection）
--	版块编号  sID  int 标识列 主键
--	版块名称  sName  varchar(10)  不能为空
--	版主编号  sUid   int 外键  引用用户信息表的用户编号



--二、在上面的数据库、表的基础上完成下列题目：

--	1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--	 1, 小雨点  女  20  0
--	  逍遥    男  18  4
--	  七年级生  男  19  2

--	2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
--	--插入多行：
--		先创建好空表，然后再插入数据，
--		直接插入数据，然后自动生成表。
--	insert into bbsPoint select uName,uPoint from bbsUsers
--	select uName,uPoint into bbsPoint from bbsUsers

select uName,uPoint into bbsPoint from bbsUsers

insert into bbsSection(sName,sUid)
select'技术交流',1 union
select'读书世界',2 union
select'生活百科',3 union
select'八卦区',4 
select * from bbsSection
	
--	3.给论坛开设4个板块
--	  名称        版主名
--	  技术交流    小雨点
--	  读书世界    七年级生
--	  生活百科     小雨点
--	  八卦区       七年级生

--	4.向主贴和回帖表中添加几条记录
	  
--	   主贴：

--	  发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
--	  逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
--	  七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
--	  小雨点   生活百科    今年夏天最流行什么     有谁知道今年夏天最流行  2008-9-10  0
--						      什么呀？
insert into bbsTopic( tid,tUID,tSID,tTitle,tMsg,tTime,tCount )
select 1,3, 4,'范跑跑','谁是范跑跑',' 2008-7-8 ',1 union
select 2,2, 1,' .NET ','与JAVA的区别是什么呀？','2008-9-1',2 union
select 3,1, 3,' 今年夏天最流行是什么呀','有谁知道今年夏天最流行','2008-9-10',0
select * from bbsTopic
	   
--	   回帖：
--	   分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）

select * from bbsUsers
delete  bbsUsers where  uName ='逍遥'


update bbsUsers set uPoint ='10' where uName='小雨点'

select * from bbsSection
delete bbsSection where sName='生活百科'

select * from bbsReply
delete bbsReply
--	6.因为小雨点发帖较多，将其积分增加10分

--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）

--	8.因回帖积累太多，现需要将所有的回帖删除

--drop table bbsReply
--delete from bbsReply
--truncate table bbsReply





  
