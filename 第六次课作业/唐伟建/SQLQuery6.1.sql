create database bbs
on
(
	fileName='F:\homework\bbs.mdf',
	Name='bbs',
	size=5MB,
	Maxsize=5MB,
	filegrowth=1MB
)
log on
(
	fileName='F:\homework\bbs_log.ldf',
	Name='bbs_log',
	size=5MB,
	Maxsize=5MB,
	filegrowth=1MB
)
go

use bbs
go

create table bbsUsers
(
	UID int identity(1,1) ,
	uName varchar(10) not null,
	uSex varchar(2) not null,
	uAge int not null,
	uPoint int not null 
)
--
alter table bbsUsers add constraint PK_bbsUser_UID primary key(UID)
--
alter table bbsUsers add constraint UK_bbsUser_uName unique(uName)
--
alter table bbsUsers add constraint CK_bbsUser_uSex check(uSex in('男','女'))
--
alter table bbsUsers add constraint CK_bbsUser_uAge check(uAge>=15 or uAge<=60)
--
alter table bbsUsers add constraint CK_bbsUser_uPoint check(uPoint>=0)
--

create table bbsSection
(
	sID int identity(1,1),
	sName varchar(10) not null,
	sUid int 
)

alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
--
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(UID)



create  table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int foreign key references bbsUsers(UID),
	tSID int foreign key references bbsSection(sID),
	tTItle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)


create table bbsReply
(
	rID INT primary key identity(1,1),
	rUID int foreign key references bbsUsers(UID),
	rTID int foreign key references bbsTopic(tID),
	rMsg text not null,
	rTime datetime
)

insert into bbsUsers values
('小雨点','女',20,0),
('逍遥','男',18,4),
('七年级生','男',19,2) 


select * into bbsPoint from bbsUsers

insert into bbsSection values
('技术交流',1),
('读书世界',3),
('生活百科',1),
('八卦区',3)

insert into bbsTopic values
(2,4,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀?',2008-9-10,0),
(3,1,'.NET','与JAVA的区别是什么呀?',2008-9-1,2),
(1,3,'范跑跑','谁是范跑跑',2008-7-8,1)

insert into bbsReply values
(2,1,'不知道',2008-9-10),
(3,2,'不知道',2008-9-1),
(1,3,'不知道',2008-7-8)


alter table bbsTopic drop constraint FK__bbsTopic__tUID__1920BF5C

alter table bbsReply drop constraint FK__bbsReply__rUID__1CF15040


delete from bbsUsers where uName='逍遥'

update bbsUsers set uPoint=10 where uName='小雨点'

alter table bbsTopic drop constraint FK__bbsTopic__tSID__1A14E395

delete from bbsSection where sName='生活百科'

delete from bbsReply



