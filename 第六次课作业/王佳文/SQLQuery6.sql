use master
go 
create database bbs
on(
	name='bbs',
	filename='C:\TEXT\bbs.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
log on(
	name='bbs_log',
	filename='C:\TEXT\bbs_log.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
go
use bbs
go
create table bbsUsers
(
	uID int identity(1,1) not null ,
	uName nvarchar(10) not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null,

)
alter table bbsUsers add constraint PK_bbsUsers_uID primary key(uID)
alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex='男' or uSex='女')
alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 or uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>=0)
create table bbsSection
(
	sID  int  identity(1,1) not null ,
	sName  varchar(10) not null,
	sUid   int ,

)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid)references bbsUsers(uID)
create table bbsTopic
(
	tID  int primary key identity(1,1) not null,
	tUID  int references bbsUsers(uID) not null  ,
    tSID int references  bbsSection(sID) ,
	tTitle  varchar(100) not null,
	tMsg  text  not null,
	tTime  datetime  not null,
	tCount  int not null,
)
create table bbsReply
(
	rID  int primary key identity(1,1) not null,
	rUID int references bbsUsers(uID) ,
	rTID  int references bbsTopic(tID) ,
	rMsg  text  not null,
	rTime  datetime ,
)

go
use bbs
go


select * from bbsSection
select * from bbsReply
select * from bbsTopic
select * from bbsUsers
insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select uName,uPoint into bbsPoint_backup from bbsUsers
insert into bbsSection values('技术名流',1),('读书世界',3),('生活百科',1),('八卦区',3)
insert into bbsTopic values(2,4,'范跑跑','谁是范跑跑', 2008-7-8,1),(3,2,'.NET','与JAVA的区别是什么呀？',2008-9-1,2),(1,4,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？',2008-9-10,0)
insert into bbsReply values(2,2,'一名地震自己先跑的教师',2008-7-8),(3,3,'不知道',2008-9-1),(1,1,'流行穿黑裙子',2008-9-10)
update  bbsUsers set  uPoint=30 where uName='小雨点'
delete from bbsTopic where tUID=2
delete from bbsReply where rUID=2
delete from bbsReply where rID=1
delete from bbsUsers where uName='逍遥'
delete from bbsTopic where tSID=3
delete from bbsSection where sName='生活百科'
delete from bbsReply