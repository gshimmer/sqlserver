use master
go 
create database company
on(
	name='company',
	filename='C:\TEXT\company.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
log on(
	name='company_log',
	filename='C:\TEXT\company_log.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
go
use company
go
create table sectionInfo
(
	sectionID int primary key identity(1,1),
	sectionName varchar(10) not null,
)
create table userInfo
(
	userNo  int primary key not null,
	userName  varchar(10) unique not null,
	 userSex   varchar(2) check(userSex='��' or userSex='Ů')not null,
	 userAge   int check(userAge>=1 or userAge<=100) not null,
	 userAddress varchar(50) default('����'),
	  userSection  int references sectionInfo(sectionID),
)
create table workInfo
(
	workId int  primary key identity(1,1) not null,
	userId  int references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription  varchar(40) check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�'),

	
)