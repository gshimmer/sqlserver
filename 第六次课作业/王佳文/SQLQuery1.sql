use master
go 
create database TestDB
on(
	name='TestDB',
	filename='C:\TEXT\TestDB.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
log on(
	name='TestDB_log',
	filename='C:\TEXT\TestDB_log.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
go
use TestDB
go
create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null,
)
create table loginInfo
(
	LoginId int  primary key identity(1,1),
	LoginName nvarchar(10) unique not null,
	LoginPwd nvarchar(20) default('123456') not null,
	LoginBirthday datetime not null,
	LoginVIP nvarchar(10) not null,
)
