use master
go

create database bbs
on
(
	name = 'bbs',
	filename = 'D:\数据库\bbs.mdf'
)

log on
(
	name = 'bbsUser_log',
	filename = 'D:\数据库\bbs_log.ldf'
)

use bbs

create table bbsUsers
(
	UID int identity not null,
	uName varchar(10) not null,
	uSex varchar(2) not null,
	uAge int not null,
	uPoint int not null,
)

alter table bbsUsers add constraint PK_s primary key(UID) 
alter table bbsUsers add constraint UK_b unique(uName) 
alter table bbsUsers add constraint CK_b check(uSex in ('男','女')) 
alter table bbsUsers add constraint CK_a check( uAge>=15 and  uAge<=60) 
alter table bbsUsers add constraint CK_c check( uPoint>=0)
 
create table bbsTopic
(
	tID int identity,
	tUID int ,
	tSID int ,
	tTitle varchar(100) not null,
    tMsg text not null,
	tTime datetime,
	tCount int
)

alter table bbsTopic add constraint FK_S foreign key(tUID) references bbsUsers(UID)

create table bbsReply
(
	rID  int identity,
	rUID  int,
	rTID  int,
	rMsg  text not null,
	rTime  datetime 
)
alter table bbsReply add constraint PK_z primary key(rID)
alter table bbsReply add constraint FK_d foreign key(rUID) references bbsUsers(UID)

create table bbsSection
(
	sID  int identity,
	sName  varchar(10) not null,
	sUid   int,
)
alter table bbsTopic add constraint FK_l foreign key(tSID) references bbsSection(sID)
alter table bbsSection add constraint PK_q primary key(sID) 
alter table bbsSection add constraint PK_p foreign key(sUid) references bbsUsers(UID)

insert into bbsUsers (uName,uSex,uAge,uPoint)
select '小雨点','女',20 ,0 union
select '逍遥','男',18 ,4 union
select '七年级生','男',19 ,2

select uName,uPoint into bbsPoint from bbsUsers
select * from bbsPoint

select * from bbsUsers
select * from bbsSection

insert into bbsSection (sName, sUid)values('技术交流',3),( '读书世界',1),( '生活百科',3),('八卦区',1)

select * from  bbsSection
select * from bbsTopic

insert into bbsTopic (tUID,tSID,tTitle,tMsg,tTime,tCount)values
(2,4,'范跑跑','谁是范跑跑','2008-7-8',1),
(1,1,'.NET ','与JAVA的区别是什么呀？','2008-9-1','2'),
(3,3,'今年夏天最流行什么','今年夏天最流行什么','2008-9-10',0)
select * from bbsTopic

insert into  bbsReply (rUID,rTID,rMsg,rTime)values
(1,4,'范跑跑是一位老师','2008-07-09'),
(2,1,'.NET与JAVA区别很大','2008-10-09'),
(2,1,'.JAVA是一门编程语言','2008-10-09')

select * from bbsReply

alter table bbsTopic drop constraint FK_S 
alter table bbsReply drop constraint FK_d 
delete from bbsUsers where uName = '逍遥'
select * from bbsUsers

update  bbsUsers set uPoint = 10 where uName = '小雨点'
select * from bbsUsers

alter table bbsSection drop constraint PK_q
alter table bbsSection drop constraint PK_p
delete from bbsSection where sName = '生活百科'
select * from bbsSection

alter table bbsReply drop constraint PK_z
delete from bbsReply where rUID = 1
delete from bbsReply where rUID = 2
select * from bbsReply