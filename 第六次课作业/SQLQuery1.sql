use master

create database bbs

on

(

    name='bbs',

	filename='E:\bbs.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)



log on 

(

    name='bbs_log',

	filename='E:\bss_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)

go

create table bbsUsers
(
UID int primary key identity(1,1),
uName varchar(10) not null ,
uSex varchar(2)not null ,
uAge int not null ,
uPoint int not null ,
)  
alter table bbsUsers add constraint PK primary key (uuID) 
alter table bbsUsers add constraint UK unique(uName) 
alter table bbsUsers add constraint CK check(uSex='男' or uSex='女' )
alter table bbsUsers add constraint CK1 check(uAge>=15 or uAge<=60 )
alter table bbsUsers add constraint CK2 check(upoint>=0)

insert into bbsUsers(uName,uSex,uAge,uPoint)
select '小雨点', '女',  20, 0 union
select '逍遥' , '男' ,18,4 union
select '七年级生' ,'男' ,19, 2 

select * from bbsUsers

create table bbsSection
(
sID int primary key identity(1,1),
sName varchar(10) not null,
sUid int 
)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign  key (sUid)references bbsUsers(UID)

insert into bbsSection(sName,sUid) values('技术交流' ,1),
                                         ('读书世界',3),
                                         ('生活百科',1),
                                         ('八卦区',3)



create table bbsTopic
(	 tID  int primary key identity(1,1),
     tUID  int references bbsUsers(UID),
     tSID  int references bbsSection( sID ),
     tTitle  varchar(100) not null,
     tMsg  text not null,
     tTime  datetime , 
	 tCount  int
	 )

select * from bbsTopic
insert into bbsTopic(tuID,tSID,tTitle ,tMsg,tTime,tCount) 
            values(1,    2,'范跑跑','谁是范跑跑','2008-7-8','1'),
				    (3,    4,'.NET  ',' 与JAVA的区别是什么呀？  ','2008-9-1 ','2'),
				    (5,    6,'今年夏天最流行什么 ',' 有谁知道今年夏天最流行','2008-9-10','0')
			
			
select uName, upoint  into bbsPoint from bbsUsers

create table bbsReply
(
rID int primary key,
rUID int  foreign key references bbsUsers(UID),
rTID int  foreign key references bbsTopic(tID ),
rMsg text not null,
rTime datetime ,

 
)
select * from  bbsReply
insert into bbsReply(rUID,rTID ,rMsg,rTime) values(1,2,'二班长的最丑的就是范跑跑',0123),
                                                  (4,3,'我挂科的不知道这么深奥的问题',0123),
                                                  (5,6,'这个夏天最流行的当然是黑丝啊破洞的那种',0123)

delete bbsUsers where uName='逍遥'
alter table  bbsReply drop  FK__bbsReply__rUID__4AB81AF0
alter table bbsTopic drop FK__bbsTopic__tUID__46E78A0C

update bbsUsers set upoint=10 where uName='小雨点'

alter table bbsTopic drop FK__bbsTopic__tSID__47DBAE45
delete bbsSection where sName='生活百科'

delete  bbsReply 










