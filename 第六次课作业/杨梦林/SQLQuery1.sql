use master
create database TestDB1
on

(

    name='TestDB1 ',

	filename='D:\TestDB1.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)



log on 

(

    name='TestDB1_log',

	filename='D:\TestDB1_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)

go

use TestDB1
go

create table typelnfo
(
typeld int primary key not null identity(1,1),
typeName varchar(10) not null ,
)
create table loginlnfo
(
loginld int primary key not null identity(1,1),
loginName nvarchar(10) not null unique,
loginpwd nvarchar(20) not null default('123456'),
loginSex char(2)default('男'),
loginday int ,
loginvip int,
)
--公司数据库
create database company
on
(


    name='company',

	filename='D:\company.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)



log on 

(

    name='company_log',

	filename='D:\company_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)

go

use company
go
--部门表
create table sectionlnfo
(
sectionID int primary key not null ,
sectionName varchar(10) not null ,
)
--员工信息表
create table userlnfo
(
userNo int primary key not null ,
userName varchar(10) unique not null check(len(userName)>=4),
userSex varchar(2) not null check(userSex='男' or userSex='女'),
userAge int  not null check(userAge>=1 and userAge<=100),
userSection int foreign key references sectionlnfo(sectionID)
)
--员工考勤表
create table worklnfo
(
workld int primary key not null ,
userld int foreign key references userlnfo(userNo) not null,
workTime datetime not null,
workDescription varchar(40) not null check (workDescription='迟到'or workDescription='矿工' or workDescription= '病假' or workDescription= '事假' or  workDescription='早退'),
)
--学校数据库
create database shoolinfo
on
(


    name='shoolinfo',

	filename='D:\shoolinfo.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)



log on 

(

    name='shoolinfo_log',

	filename='D:\shoolinfo_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)

go

use shoolinfo
go

create table class
(
classid int primary key not null,
classname char not null unique ,
classtime int  not null ,
classdepict varchar not null ,
)

create table student 
(
stuinfo int primary key not null ,
stuname nvarchar(10) check(len(stuname)>=2) not null ,
stusex char default('男') not null check(stusex ='男'or stusex ='女'),
stuage int check(stuage>=15 and stuage<=40) not null ,
stuhome nvarchar default('贵州'),
stuhao int not null ,
)
--课程表
create table course
(
courseinfo int primary key not null ,
coursename char not null unique,
coursedepict varchar(50) not null ,
)
--成绩表
create table grate 
(
grateinfo int primary key not null,
gratehao int not null,
grateke int not null,
grateing int check(grateing>=1 or grateing <=100)
)
--房屋数据库
create database house
on
(
name =house,
filename='D:\house.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)



log on 

(

    name='house_log',

	filename='D:\house_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)
go
use house
go
--房屋主人表
create table tbluser
(
userld int not null primary key ,
username char default('杨梦林') not null,
usertel int not null default('8848'),
)
--房屋类型表
create table tbhousetype
(
typeld int primary key not null ,
typname char default('梦林妙妙屋') not null,
tycounty nvarchar(30) default('北京天安门') not null ,
tycountyname nvarchar(20) default('天安门小厕所'),
)
--房屋信息表
create table tbhouseinfo
(
tbid int primary key not null ,
tbdesc varchar(10) check(tbdesc >=100) not null ,
tbrent int not null check(tbrent>=100000000000000 and tbrent<=100000000000000000000),
tbliving nvarchar(10) check(tbliving>11000000000000000000000000000000),
)






