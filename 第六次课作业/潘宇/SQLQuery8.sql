use master
go
create database bbs
on
(
name='bss',
filename='D:\bbs.mdf',
maxsize=50,
filegrowth=1,
size=5
)
log on
(
name='bss_log',
filename='D:\bbs_log.ldf',
maxsize=50,
filegrowth=1,
size=5
)
go
use bbs
go
create table bbsUsers
(
UID int primary key identity(1,1),
uName varchar(10) unique not null,
uSex  varchar(2) not null check(uSex='男' or uSex='女'),
uAge  int not null check(uAge>=15 and uAge<=60),
uPoint  int not null check(uPoint>=0)
)
alter table bbsUsers add constraint fk_bbsUsers primary key(UID)
alter table bbsUsers add constraint sk_bbsUsers unique(uName)
alter table bbsUsers add constraint nk_bbsUsers check(uSex='男' or uSex='女' )
alter table bbsUsers add constraint ak_bbsUsers check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint pk_bbsUsers check(uPoint>=0)
create table bbsTopic
(
tID  int primary key identity(1,1),
tUID  int foreign key references bbsUsers( UID),
tSID  int foreign key references bbsSection(sID),
tTitle  varchar(100) not null,
tMsg  text not null,
 tTime  datetime ,
  tCount  int
)
create table bbsReply
(
rID  int primary key identity(1,1),
rUID  int foreign key references bbsUsers(UID),
TID  int foreign key references bbsTopic(tID),
rMsg  text not null,
Time  datetime
)
create table bbsSection
(
sID  int primary key identity(1,1),
sName  varchar(10) not null,
sUid   int foreign key references bbsUsers(UID)
)
alter table bbsSection add constraint ik_bbsSection primary key(sID)
alter table bbsSection add constraint mk_bbsSection primary key(sUid)

insert into bbsUsers
select '小雨点','女','20','0'union
select ' 逍遥 ','男','18','4'union
select '七年级生','男','19','2'

select uName,uPoint into bbsPoint from bbsUsers

insert into bbsPoint select uName,uPoint from bbsUsers

insert into bbsSection(sName,sUid)
select '技术交流',1union
select '读书世界',2union
select '生活百科',3union
select '八卦区',4
select * from bbsSection
insert into BbsTopic values
(4,6,'范跑跑','谁是范跑跑','2008-7-8',1),
(5,3,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),
(3,5,'今年夏天最流行什么','有谁知道及那年夏天最流行什么呀？','2008-9-10',0)

insert into BbsReply values
(3,5,'计划司法机会大','2008-9-1'),
(3,6,'家私房价','2008-10-1'),
(4,7,'山东矿机拉速度快','2008-10-10')

alter table BbsReply drop constraint FK__BbsReply__RepluU__1FCDBCEB
alter table BbsTopic drop constraint FK__BbsTopic__TopicU__1BFD2C07
delete BbsUsers where UserName='逍遥'

update bbsUsers set UserPoint=12 where UserName='小雨点'

alter table BbsTopic drop constraint FK__BbsTopic__TopicS__1CF15040
delete BbsSection where SectionName='生活百科'

delete  BbsReply 