
create database bbs
on
(
    name='bbs',
    filename='E:\test\Class.mdf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
log on
(   
    name='Class_log',
    filename='E:\test\Class_log.ldf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
create table ClassInfo
(
classid int primary key identity (1 ,1),
ClassName nvarchar(20) unique not null,
Starttime date not null,
Classdescription ntext
)
 create table StudentInfo
   (StuID int primary key identity(1,1),
  StuName nvarchar(20) unique check (Len ( StuName)>=2),
  StuSex nvarchar(1) default ('��')check(StuSex='��' or StuSex='Ů') not null,
  Stuage int check(Stuage >= 15 and Stuage <= 40 ),
  StuAddress int default ('����'),
  ClassID int  foreign key references ClassInfo(classid),
  )
  create table CourseInfo
   (CourseID int primary key identity (1,1),
   CourseName nvarchar(50) unique  not null,
   Coursedescription ntext
)
   create table ScoreInfo
(
 ScoreID   int primary key identity (1 ,1) not null,
 StuID int not null,
 CourseID int not null,
 Score int check( Score >= 0 and  Score <= 100 ),
)
