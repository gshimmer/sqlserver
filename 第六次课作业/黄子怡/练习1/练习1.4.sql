create database House
on
(
    name='House',
    filename='E:\test\House.mdf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
log on
(   
    name='House_log',
    filename='E:\test\House_log.ldf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
create table tblUser 
(
userId int primary key identity (1 ,1),
userName nvarchar(20)  not null,
userTel nvarchar(20) not null,
)
create table tblHouseType
(
typeId int primary key identity (1 ,1),
typName nvarchar(20)  check(typName='����'or typName='��ͨסլ'or typName='ƽ��'or typName='���ٵ�����'),
)
create table tblQx 
(
qxId int primary key identity (1 ,1),
qxName nvarchar(20)  check(qxName='���'or qxName='����'or qxName='����'),
)
create table tblHouseInfo
(
id int primary key identity (1 ,1),
Housedesc ntext,
userId int foreign key(userId) references tblUser(userId),
zj money,
shi int,
ting int,
typeId int foreign key(typeId) references tblHouseType(typeId),
qxId int foreign key(qxId) references tblQx(qxId)
)