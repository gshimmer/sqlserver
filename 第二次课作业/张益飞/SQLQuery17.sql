/****** Script for SelectTopNRows command from SSMS  ******/
 create database StudentInfo
  on
  (
  name='Students',
  filename='D:\数据库\test.mdf',
  size=8mb,
  maxsize=100mb,
  filegrowth=10Mb
  )
  log on
  (
   name='Students_log',
  filename='D:\数据库\test.ldf',
  size=8mb,
  maxsize=100mb,
  filegrowth=10Mb
  )
  go
  use Students
  go
  create  table StuInfo
  (
        StuID    int primary key identity(1,1) not null,
		StuNum   char(20) not null,
		StuName  char(40)  not null,
		StuSex   char(2) default('男') check(StuSex='男'or StuSex='女')，
		StuPhone varchar(11) 
  )
use Students
  go
  create  table   ClassInfo
  (
       ClassID      int primary key identity(1,1) not null,
	   ClassNum     char(15) not null,
	   ClassName    char(30)  not null,
	   ClassRemark  text,
	   StuID       int  not null
  )

