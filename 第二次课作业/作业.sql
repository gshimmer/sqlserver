USE [Students]
GO
/****** Object:  Table [dbo].[studeninfo]    Script Date: 2021/3/4 21:46:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[studeninfo](
	[StuiID] [varchar](1) NOT NULL,
	[StuNum] [nvarchar](10) NULL,
	[StuName] [varchar](10) NULL,
	[StuSex] [varchar](2) NULL,
	[StuPhone] [numeric](18, 0) NULL,
 CONSTRAINT [PK_studeninfo] PRIMARY KEY CLUSTERED 
(
	[StuiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Table_1]    Script Date: 2021/3/4 21:46:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_1](
	[StuID] [int] IDENTITY(1,1) NOT NULL,
	[StuNum] [int] NULL,
	[StuName] [nvarchar](10) NULL,
	[StuSex] [nchar](2) NULL,
	[StuPhone] [int] NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[StuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[班级信息表]    Script Date: 2021/3/4 21:46:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[班级信息表](
	[CLassID] [int] IDENTITY(1,1) NOT NULL,
	[CLassNum] [nvarchar](30) NULL,
	[CLassName] [nvarchar](60) NULL,
	[CLassRemark] [nchar](200) NULL,
	[StuID] [int] NULL,
 CONSTRAINT [PK_班级信息表] PRIMARY KEY CLUSTERED 
(
	[CLassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[班级信息表]  WITH CHECK ADD  CONSTRAINT [FK_班级信息表_Table_1] FOREIGN KEY([StuID])
REFERENCES [dbo].[Table_1] ([StuID])
GO
ALTER TABLE [dbo].[班级信息表] CHECK CONSTRAINT [FK_班级信息表_Table_1]
GO
