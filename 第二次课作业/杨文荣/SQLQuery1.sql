if exists(select * from sys.databases where name='Students')
 drop database Students
create database Students
on
(
	name='Students',
	filename='C:\学习\数据库\Students.mdf',
	size=6MB,
	maxsize=100MB,
	filegrowth=10MB
)
log on
(
	name='Students_log',
	filename='C:\学习\数据库\Students_log.mdf',
	size=6MB,
	maxsize=100MB,
	filegrowth=10MB
)
go

use Students
go

create table StuInfo
(
	StuID int primary key identity(1,1) not null,
	StuNum char(10) not null,
	StuName nvarchar(20) not null,
	StuSex nchar(1) default('男') check(StuSex='男'or StuSex='女') not null,
	Stuphone char(11),
)
go

use Students
create table ClassInfo
(
	ClassID int primary key identity(1,1) not null,
	ClassNum char(15) not null,
	ClassName nchar(30) not null,
	ClassRemark text,
	StuID int not null
)

