create database hzt02
go
use hzt02
go
create table Stulnfo
( --列名，数据类型，设置主键,自增（起始1，增量1）,设置非空
  StuID int  primary key identity(1,1) not null,
  StuNum char(10) not null,
  StuName nvarchar(20) not null,
  StuSex char(2) default('男') check(StuSex='男' or StuSex='女') not null,
  StuPhone text,
)
create table Classlnfo
(
 ClassID int primary key identity(1,1) not null,
 ClassNum char(15) not null,
 ClassName char(30) not null,
 ClassRemark text,
 StuID nchar,
)