use master
create database Students
on primary
(
	name=Students,
	filename='D:\Document\MSSQLDatabase\Students\Students.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth = 1Mb
)

log on
(
	name=Students_Log,
	filename='D:\Document\MSSQLDatabase\Students\Students_Log.ldf',
	size=1MB,
	maxsize=10MB,
	filegrowth=10%
)

go

use Students
create table StuInfo
(
	StuID int identity(1,1) primary key,
	StuNum char(10) not null,
	StuName nchar(20) not null,
	StuSex nchar(1) default('��') check(StuSex='��' or StuSex='Ů'),
	StuPhone bigint check(StuPhone>=1000000 and StuPhone<=999999999 or StuPhone >=10000000000 and StuPhone <=99999999999) 
)

go

use Students
create table ClassInfo
(
	ClassID int identity(1,1) primary key,
	ClassNum char(15) not null,
	ClassName nchar(30) not null,
	ClassRemark text,
	StuID int constraint FK_StuID_StuInfo references StuInfo(StuID)
)

go
