use master
go
create database Students
on
(
	name='Students',
	filename='D:\Students.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=100MB
)
log on 
(
	name='Students_log',
	filename='D:\Students_log.ldf',
	size=5MB,
	maxsize=100MB,
	filegrowth=100MB
)
go

use Students
go
create table StuInfo
(
	StuID int primary key identity(1,1) not null,
	StuNum char(20) not null,
	StuName char(40) not null,
	StuSex char(2) default('��') check(StuSex='��'or StuSex='Ů') not null,
	StuPhone char(11)
)
use Students
  go
  create  table   ClassInfo
  (
       ClassID      int primary key identity(1,1) not null,
	   ClassNum     char(15) not null,
	   ClassName    char(30)  not null,
	   ClassRemark  text,
	   StuID       int  not null
  )
