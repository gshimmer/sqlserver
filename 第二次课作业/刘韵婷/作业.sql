USE [Students]
GO
/****** Object:  Table [dbo].[班级表]    Script Date: 2021/3/4 18:18:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[班级表](
	[ClassID] [int] IDENTITY(1,1) NOT NULL,
	[ClassNum] [int] NULL,
	[ClassName] [nvarchar](30) NULL,
	[ClassRemark] [nchar](60) NULL,
	[StuID] [nchar](10) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[班级表-]    Script Date: 2021/3/4 18:18:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[班级表-](
	[ClassID] [int] IDENTITY(1,1) NOT NULL,
	[ClassNum] [nvarchar](30) NULL,
	[CLassName] [nvarchar](60) NULL,
	[CLassRemark] [nchar](200) NULL,
	[StuID] [int] NULL,
 CONSTRAINT [PK_班级表-] PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[学生信息表]    Script Date: 2021/3/4 18:18:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[学生信息表](
	[StuID] [int] IDENTITY(1,1) NOT NULL,
	[StuNum] [int] NULL,
	[StuName] [nvarchar](10) NULL,
	[StuSex] [nchar](2) NULL,
	[StuPhone] [int] NULL,
 CONSTRAINT [PK_学生信息表] PRIMARY KEY CLUSTERED 
(
	[StuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[学生信息表-]    Script Date: 2021/3/4 18:18:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[学生信息表-](
	[StuID] [int] IDENTITY(1,1) NOT NULL,
	[StuNum] [nvarchar](20) NULL,
	[StuName] [nvarchar](40) NULL,
	[StuSex] [nchar](2) NULL,
	[StuPhone] [int] NULL,
 CONSTRAINT [PK_学生信息表-] PRIMARY KEY CLUSTERED 
(
	[StuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
