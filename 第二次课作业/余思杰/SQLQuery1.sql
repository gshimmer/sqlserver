use master
go
create database Students
on
(
	name=Students,
	filename='D:\SQl\Students.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=1mb
)

log on
(
	name=Students_Log,
	filename='D:\SQl\Students_Log.ldf',
	size=1mb,
	maxsize=10mb,
	filegrowth=10%
)

go

use Students
create table StuInfo
(
	StuID int identity(1,1) primary key,
	StuNum char(10) not null,
	StuName char(20) not null,
	StuSex char(2) default('��') check(StuSex='Ů' and StuSex='��') not null,
	StuPhone int check(StuPhone=11 and StuPhone=7),
)

go

use Students
create table ClassInfo
(
	ClassID int identity(1,1) primary key,
	ClassNum char(15) not null,
	ClassName char(30) not null,
	ClassRemark text not null,
	StuID int,
)

go