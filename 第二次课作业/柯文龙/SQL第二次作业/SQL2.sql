create database Students
on
(
	name='Students',
	filename='D:\SQL安装包\MSSQL15.MSSQLSERVER\MSSQL\DATA.mdf'
	size=6MB,
	maxsize=100MB,
	filegrowth=10Mb
)
log on
(
	name='Students_log',
	filename='D:\SQL安装包\MSSQL15.MSSQLSERVER\MSSQL\DATA_log.ldf'
	size=6MB,
	maxsize=100MB,
	filegrowth=10Mb
)
go

use Students
go
