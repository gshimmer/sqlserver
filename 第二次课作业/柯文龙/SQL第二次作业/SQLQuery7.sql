create table Stuinfo
(
--列名 数据类型 约束,
	StuID int primary key identity(1,1) not null,
	StuNum nvarchar(10) not null,
	StuName nvarchar(20) not null,
	StuSex char(2) default('男') check(StuSex='男' or StuSex='女') not null,
	Stuiphone varchar(11),
)