

create database GIAO
on
(
    name='GIAO',
	filename='D:\GIAO.mdf',
	size=6MB,
	maxsize=100MB,
	filegrowth=10MB
)

log on 
(
    name='GIAO',
	filename='D:\GIAO.ldf',
	size=6MB,
	maxsize=100MB,
	filegrowth=10MB
)
go

use GIAO
go

create table  StuInfo
(
StuId int primary key identity(1,1),
StuNum varchar(10) not null,
StuName nvarchar(20) not null,
Stusex  char(2) default('nan') check(Stusex='nan' or Stusex='nv'),
StuPhone bigint ,
)
create table ClassInfo
(
 ClassID int primary key identity(1,1),
 ClassNum varchar(15) not null,
 Classname nvarchar(30) not null,
 ClassRemark ntext,
 StuID int,


)
