use master
go

if exists(select * from sys.databases where name='Students')
	drop database Students

create database Students
on
(
	name='Students',
	filename='D:\sql数据库demo\Students.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10Mb
)
log on
(
	name='Students_log',
	filename='D:\sql数据库demo\Students_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10Mb
)
go

use Students
go

create table StuInfo
(
	StuID int primary key identity(1,1) not null,
	StuNum  nvarchar(10) not null,
	StuName nvarchar(20) not null,
	Stusex char(2) default('男') check(Stusex='男' or Stusex='女') not null,
	StuPhone char(11)
)
create table ClassInfo
(
	ClassID int primary key identity(1,1) not null,
	ClassNum  nvarchar(15) not null,
	ClassName nvarchar(30) not null,
	ClassRemark text,
	StuID int not null,
)