use master
go
if exists(select * from sys.databases where name='Student')
	drop database Student
	create database Student
on
(
	name='Student',
	filename='D:\SQL\Student.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10MB
)
log on
(
	name='Student_log',
	filename='D:\SQL\Student_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10MB
)
go

use Student

create table Class
(
ClassID int primary key identity(1,1),
ClassName nvarchar(20) unique(ClassName) not null,
)
go

use Student
create table Student
(
	StuID int primary key identity(1,1),
	ClassID int constraint FK_Class_ClassID references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) Check(StuSex in('��','Ů')),
	StuBrithday date,
	StuPhone nvarchar(11) unique(StuPhone)
)
go

use Student
create table Course
(
	CourseID int primary key identity(1,1),
	ScoreID int,
	CourseCredit int default(1) check(CourseCredit >= 1 and CourseCredit <= 5)
)
go

use Student
create table Score
(
	ScoreID int identity(1,1),
	StuID int,
	CourseID int,
	Score decimal(5,2) unique(Score)
)
go

use Student
alter table Student add StuAddress nvarchar(200)
alter table Score add primary key(ScoreID)
alter table Score add foreign key (StuID) references Student(StuID)
alter table Score add foreign key (CourseID) references Course(CourseID)