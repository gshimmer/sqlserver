create database Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use Student
go
create table Class
(
	ClassID int primary key identity(1,1) not null,
	ClassName nvarchar(20) unique not null
)
create table Student
(
	StuID int primary key identity(1,1) not null,
	ClassID int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('��') check (StuSex='��' or StuSex='Ů'),
	StuBirthday date,
	StuPhone nvarchar(11) unique
)
create table Course
(
	CourseID int primary key identity(1,1) not null,
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check (CourseCredit>1 and CourseCredit<5) not null
)
create table Score
(
	ScoreID int identity(1,1) not null,
	StuID int ,
	CourseID int ,
	Score decimal(5,2) unique not null
)
alter table Student add StuAddress nvarchar(200)
alter table Score add constraint FK_Score_StuID foreign key (StuID) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key (CourseID) references Course(CourseID)
alter table Score add constraint FK_Score_ScoreID primary key (ScoreID)