create database Student
on
(
  name='Student', --逻辑名称
  filename='D:\Student.mdf',
  size=5MB,
  maxsize=50MB,
  filegrowth=10% 
)
log on
(
 name='StuAge_log',  --逻辑名称
  filename='F:\StuAge_log.ldf',
  size=5MB,
  maxsize=50MB,
  filegrowth=10%
)

create table Class
(
  ClassID int  primary key identity(1,1), --主键并且自增
  ClassName nvarchar(20) unique not null,  --约束：唯一，非空
)

create table Student
(
 StuID int primary key identity(1,1), --主键且自增
 ClassID  int foreign key references Class(ClassID), --添加外键
 StuName nvarchar(20) not null, 
 StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女'), --检查约束
 StuBirthday date ,
 StuPhone nvarchar(11) unique  --唯一约束
 )

 --这是地址字段
 alter table Student add StuAddress nvarchar(200)

 create table Course
 (
  CourseID int primary key identity, --主键且自增
  CourseName nvarchar(50) unique not null, --唯一，非空
  CourseCredit int not null check(CourseCredit='1' or CourseCredit='1,2,3,4,5,') --非空，检查，取值1-5
 )

 create table Score
 (
  ScoreID int,
  StuID int ,
  CourseID int ,
  Score decimal(5,2)
 )

 use mioer
 go

 --给ScoreID 添加非空
 alter table Score alter column ScoreID int not null 

 --给Score添加主键
 alter table Score add constraint  PK_ScoreID primary key(ScoreID)

 --给CourseID添加外键且关联
 alter table Score add constraint FK_CourseID_StuID foreign key (StuID) references Student(StuID)

 --删除Score约束
 alter table Score drop constraint UK_Score

 --给Score添加非空
 alter table Score alter column Score decimal not null

 --给Score添加唯一
 alter table Score add constraint UK_Score unique (Score)