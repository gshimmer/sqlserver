Create database Student
on
(
fileName='D:\homework\Student.mdf',
Name='Student',
Size=1MB,
Maxsize=5MB,
filegrowth=1MB
)
log on
(
fileName='D:\homework\Student_log.ldf',
Name='Student_log.ldf',
size=1MB,
Maxsize=5MB,
filegrowth=1MB
)
go

use Student
go

create table ClassInfo
(
ClassID int primary key identity(1,1),
ClassName nvarchar(20) unique not null
)

create table Student
(
StuID int primary key identity(1,1),
ClassID int foreign key references ClassInfo(ClassID),
StuName nvarchar not null,
StuSex nvarchar(1) check(StuSex='��' or StuSex='Ů'),
StuBirthday date,
StuPhone nvarchar unique,
)


create table Course
(
CourseID int primary key identity(1,1),
CourseName nvarchar unique not null,
CourseCredit int default(1) check(CourseCredit>=1 or CourseCredit<=5),
)


create table Score
(
ScoreID int identity(1,1),
StuID int,
CourseID int ,
Score decimal(5,2) unique not null
)
--
alter table Student add StuAddress nvarchar
--
alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)
--
alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)
--
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)
