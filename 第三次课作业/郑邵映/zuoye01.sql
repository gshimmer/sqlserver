use master
go

create database zuoye01
on
(
name='zuoye01',
filename='D:\test\zuoye.mdf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)
log on
(
name='zuoye01_log',
filename='D:\test\zuoye_log.ldf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)
go

use zuoye01
go
create table Class
(
ClassID int primary key identity(1,1),
ClassName nvarchar(20) not null unique,
)
create table student
(
StuID int primary key identity(1,1),
ClassID int references Class(ClassID),
StuName nvarchar(20) not null,
StuSex char(1) default('��') check(StuSex='��' or StuSex='Ů'),
StuBirthday date,
StuPhone char(11)
)

create table Course
(
CourseID int primary key identity(1,1),
CourseName nvarchar(50) unique not null,
CourseCredit int unique not null
)
create table Score
(
ScoreID int ,
StuID int references Class(ClassID),
CourseID int references Course(CourseID),
Score decimal not null unique,
)
alter table student add StuAddress int not null
 