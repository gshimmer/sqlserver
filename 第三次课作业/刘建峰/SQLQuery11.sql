USE master
go
create database Student
on(
name='student',
filename='D:\',
size=5mb,
filegrowth=1mb,
maxsize=10mb
)
LOG ON(
name='student_log',
filename='D:\',
size=5mb,
maxsize=10mb,
filegrowth=1mb
)
go
use Student
go
create table Class
(
ClassID int primary key IDENTITY(1,1),        
ClassName nvarchar(20)  unique not null,


)
create table Student
(
StuID int primary key IDENTITY(1,1),        
ClassID int foreign key references Class(ClassID),
StuName nvarchar(20) not null,
StuSex nvarchar(1) default('��')  Check(StuSex ='��' or StuSex='Ů'),
StuBirthday date ,
StuPhone nvarchar(11) unique,
StuAddress nvarchar(200)

)
create table Coures
(
CourseID int primary key IDENTITY(1,1),
CourseName nvarchar(50) unique not null,
CourseCredit int default(1) check(CourseCredit=1 or CourseCredit=2 or CourseCredit=3 or CourseCredit=4 or CourseCredit=5)

)
create table Score
(
ScoreID int primary key identity(1,1),
StuID int foreign key references Student(StuID),
CourseID int foreign key  references Coures(CourseID),
Score decimal(5,2) unique not null 
)