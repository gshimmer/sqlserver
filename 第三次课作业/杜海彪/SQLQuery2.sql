select * from sysdatabases
use master
go
create database Students
on primary
(
   name='Students',
   filename='C:\Users\33054\Desktop',
   size=5mb,
   maxsize=50mb,
   filegrowth=10%
   )
   log on
   (
    name='Students_log',
   filename='C:\Users\33054\Desktop',
   size=5mb,
   maxsize=50mb,
   filegrowth=10%
   )
   create table Class

   (

   ClassID int primary key identity(1,1),
   ClassName nvarchar(20) unique not null,
   
   )

   create table Student
   (

   StuID int primary key  identity(1,1),
   SclassID int foreign key references Class( ClassID ),
   StuName nvarchar(10) not null,
   StuSex nchar(1) default('��') check(StuSex='��' or StuSex='Ů'),
   StuBirthday date null,
   StuPhone nvarchar(11) unique null,
   StuAddress nvarchar(200) null,

   )
  

   create table Course
   (
   CourseID int primary key  identity,
   CourseName nvarchar(50) unique not null,
   CourseCredit int not null check(CourseCredit='1' or CourseCredit='1,2,3,4,5')

   )

   create table Score
   (
    ScoreID int primary key  identity(1,1),
	StuID int  foreign key references Student( StuID) ,
	CourseID int foreign key references Course(CourseID),
	Score decimal(5,2) unique not null,
	)

