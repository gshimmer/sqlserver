use master
go
create database Students
on(
	name='Students',
	filename='C:\TEXT\Students.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10%
)
log on(
	name='Students_log',
	filename='C:\TEXT\Students_log.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10%
)
go
use Students
go
create table Class
(
	ClassID  int  not null ,
	ClassName nvarchar(20) unique not null,
)
create table Student
(
	StuID int not null ,
	ClassID int,
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) check(StuSex='��' or StuSex='Ů'),
	StuBirthday date,
	StuPhone nvarchar(11) unique,

)
create table Course
(
	CourseID int not null,
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1)   not null,
)
create table  Score
(
	ScoreID int not null,
	StuID int ,
	CourseID int,
	Score decimal(5,2) unique not null,
)
use Students
go
alter table Student add StuAddress nvarchar(200)
alter table Class add constraint PK_Class_ClassID primary key(ClassID)
alter table Student add constraint PK_Student_StuID primary key(StuID)
alter table Course add constraint PK_Course_CourseID primary key(CourseID)
alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)
alter table Student add constraint FK_Student_ClassID foreign key(StuID) references Class(ClassID)
alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)





