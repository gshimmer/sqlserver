USE [master]
GO

/****** Object:  Database [TestDB]    Script Date: 2021/3/15 16:11:24 ******/
CREATE DATABASE [TestDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TestDB', FILENAME = N'D:\TestDB.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TestDB_log', FILENAME = N'D:\TestDB_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TestDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TestDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TestDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TestDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TestDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TestDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TestDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [TestDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TestDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TestDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TestDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TestDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TestDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TestDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TestDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TestDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TestDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [TestDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TestDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TestDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TestDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TestDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TestDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TestDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TestDB] SET RECOVERY FULL 
GO
ALTER DATABASE [TestDB] SET  MULTI_USER 
GO
ALTER DATABASE [TestDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TestDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TestDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TestDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TestDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'TestDB', N'ON'
GO
USE [TestDB]
GO
/****** Object:  Table [dbo].[ClassInfo]    Script Date: 2021/3/15 16:11:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassInfo](
	[ClassId] [int] IDENTITY(1,1) NOT NULL,
	[ClassName] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CourseInfo]    Script Date: 2021/3/15 16:11:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourseInfo](
	[CourseId] [int] IDENTITY(1,1) NOT NULL,
	[CourseName] [nvarchar](50) NOT NULL,
	[CourseCredit] [int] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Scores]    Script Date: 2021/3/15 16:11:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Scores](
	[ScoreId] [int] IDENTITY(1,1) NOT NULL,
	[StuId] [int] NULL,
	[CourseId] [int] NULL,
	[Score] [int] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[ScoreId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StuInfo]    Script Date: 2021/3/15 16:11:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StuInfo](
	[StuId] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NULL,
	[StuName] [nvarchar](10) NOT NULL,
	[StuSex] [nvarchar](1) NULL DEFAULT ('男'),
	[StuBrithday] [date] NULL,
	[StuPhone] [nvarchar](11) NULL,
	[StuProvince] [nvarchar](200) NULL,
	[CreateDate] [datetime] NULL DEFAULT (getdate()),
	[StuAge] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[StuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ClassInfo] ON 

GO
INSERT [dbo].[ClassInfo] ([ClassId], [ClassName]) VALUES (1, N'软件1班')
GO
INSERT [dbo].[ClassInfo] ([ClassId], [ClassName]) VALUES (2, N'软件2班')
GO
INSERT [dbo].[ClassInfo] ([ClassId], [ClassName]) VALUES (3, N'软件3班')
GO
INSERT [dbo].[ClassInfo] ([ClassId], [ClassName]) VALUES (4, N'软件4班')
GO
INSERT [dbo].[ClassInfo] ([ClassId], [ClassName]) VALUES (5, N'软件5班')
GO
INSERT [dbo].[ClassInfo] ([ClassId], [ClassName]) VALUES (6, N'软件6班')
GO
INSERT [dbo].[ClassInfo] ([ClassId], [ClassName]) VALUES (7, N'软件7班')
GO
SET IDENTITY_INSERT [dbo].[ClassInfo] OFF
GO
SET IDENTITY_INSERT [dbo].[CourseInfo] ON 

GO
INSERT [dbo].[CourseInfo] ([CourseId], [CourseName], [CourseCredit]) VALUES (1, N'计算机基础', 3)
GO
INSERT [dbo].[CourseInfo] ([CourseId], [CourseName], [CourseCredit]) VALUES (2, N'HTML+CSS网页制作', 5)
GO
INSERT [dbo].[CourseInfo] ([CourseId], [CourseName], [CourseCredit]) VALUES (3, N'JAVA编程基础', 5)
GO
INSERT [dbo].[CourseInfo] ([CourseId], [CourseName], [CourseCredit]) VALUES (4, N'SQL Server数据库基础', 4)
GO
INSERT [dbo].[CourseInfo] ([CourseId], [CourseName], [CourseCredit]) VALUES (5, N'C#面向对象编程', 5)
GO
INSERT [dbo].[CourseInfo] ([CourseId], [CourseName], [CourseCredit]) VALUES (6, N'Winform桌面应用程序设计', 5)
GO
SET IDENTITY_INSERT [dbo].[CourseInfo] OFF
GO
SET IDENTITY_INSERT [dbo].[Scores] ON 

GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (1, 1, 1, 80)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (2, 1, 2, 78)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (3, 1, 3, 65)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (4, 1, 4, 90)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (5, 2, 1, 60)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (6, 2, 2, 77)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (7, 2, 3, 68)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (8, 2, 4, 88)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (9, 3, 1, 88)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (10, 3, 2, 45)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (11, 3, 3, 66)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (12, 3, 4, 75)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (13, 4, 1, 56)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (14, 4, 2, 80)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (15, 4, 3, 75)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (16, 4, 4, 66)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (17, 5, 1, 88)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (18, 5, 2, 79)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (19, 5, 3, 72)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (20, 5, 4, 85)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (21, 6, 1, 68)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (22, 6, 2, 88)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (23, 6, 3, 73)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (24, 6, 5, 63)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (25, 7, 1, 84)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (26, 7, 2, 90)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (27, 7, 3, 92)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (28, 7, 5, 78)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (29, 8, 1, 58)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (30, 8, 2, 59)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (31, 8, 3, 65)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (32, 8, 5, 75)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (33, 9, 1, 48)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (34, 9, 2, 67)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (35, 9, 3, 71)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (36, 9, 5, 56)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (37, 9, 5, 56)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (38, 1, 1, 85)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (39, 1, 2, 83)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (40, 1, 3, 70)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (41, 1, 4, 95)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (42, 2, 1, 65)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (43, 2, 2, 82)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (44, 2, 3, 73)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (45, 2, 4, 93)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (46, 3, 1, 93)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (47, 3, 2, 50)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (48, 3, 3, 71)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (49, 3, 4, 80)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (50, 4, 1, 61)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (51, 4, 2, 85)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (52, 4, 3, 80)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (53, 4, 4, 71)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (54, 5, 1, 93)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (55, 5, 2, 84)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (56, 5, 3, 77)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (57, 5, 4, 90)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (58, 6, 1, 73)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (59, 6, 2, 93)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (60, 6, 3, 78)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (61, 6, 5, 68)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (62, 7, 1, 89)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (63, 7, 2, 95)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (64, 7, 3, 97)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (65, 7, 5, 83)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (66, 8, 1, 63)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (67, 8, 2, 64)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (68, 8, 3, 70)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (69, 8, 5, 80)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (70, 9, 1, 53)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (71, 9, 2, 72)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (72, 9, 3, 76)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (73, 9, 5, 61)
GO
INSERT [dbo].[Scores] ([ScoreId], [StuId], [CourseId], [Score]) VALUES (74, 9, 5, 61)
GO
SET IDENTITY_INSERT [dbo].[Scores] OFF
GO
SET IDENTITY_INSERT [dbo].[StuInfo] ON 

GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (1, 1, N'刘正', N'男', CAST(N'2002-08-02' AS Date), N'13245678121', N'广西省', CAST(N'2021-03-14 16:46:00.887' AS DateTime), 19)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (2, 1, N'黄贵', N'男', CAST(N'2003-07-02' AS Date), N'13345678121', N'江西省', CAST(N'2021-03-14 16:46:00.887' AS DateTime), 18)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (3, 1, N'陈美', N'女', CAST(N'2002-07-22' AS Date), N'13355678125', N'福建省', CAST(N'2021-03-14 16:46:00.887' AS DateTime), 19)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (4, 2, N'江文', N'男', CAST(N'2001-07-02' AS Date), N'13347678181', N'湖南省', CAST(N'2021-03-14 16:46:00.887' AS DateTime), 20)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (5, 2, N'钟琪', N'女', CAST(N'2004-01-13' AS Date), N'13345778129', N'安徽省', CAST(N'2021-03-14 16:46:00.887' AS DateTime), 17)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (6, 3, N'曾小林', N'男', CAST(N'2005-05-15' AS Date), N'13345378563', N'安徽省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 16)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (7, 3, N'欧阳天天', N'女', CAST(N'2000-08-19' AS Date), N'13347878121', N'湖北省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 21)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (8, 3, N'李逍遥', N'男', CAST(N'1999-09-02' AS Date), N'13345678557', N'广东省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 22)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (9, 4, N'刘德华', N'男', CAST(N'1995-06-11' AS Date), N'15345679557', N'福建省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 26)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (10, 4, N'刘翔', N'男', CAST(N'1996-07-09' AS Date), N'18346679589', N'江西省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 25)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (11, 4, N'曾小贤', N'男', CAST(N'2003-07-02' AS Date), N'18348979589', N'湖南省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 18)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (12, 5, N'刘德华', N'男', CAST(N'2002-07-02' AS Date), N'18348979509', N'湖北省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 19)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (13, 5, N'陈天翔', N'男', CAST(N'2003-07-02' AS Date), N'18348079509', N'湖北省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 18)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (14, 5, N'刘能', N'男', CAST(N'2005-08-02' AS Date), N'13245678122', N'广西省', CAST(N'2021-03-14 16:46:00.890' AS DateTime), 16)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (15, 5, N'钟馗', N'男', CAST(N'2004-08-02' AS Date), N'13245678123', N'广西省', CAST(N'2021-03-14 16:46:00.893' AS DateTime), 17)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (16, 5, N'钟吴艳', N'女', CAST(N'2002-08-02' AS Date), N'13245678124', N'广西省', CAST(N'2021-03-14 16:46:00.893' AS DateTime), 19)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (17, 5, N'刘欢', N'男', CAST(N'2001-07-02' AS Date), N'13245678125', N'湖南省', CAST(N'2021-03-14 16:46:00.893' AS DateTime), 20)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (18, 5, N'张庭', N'女', CAST(N'2000-07-02' AS Date), N'13245678126', N'江西省', CAST(N'2021-03-14 16:46:00.893' AS DateTime), 21)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (19, 5, N'曹植', N'男', CAST(N'2000-08-02' AS Date), N'13245678127', N'福建省', CAST(N'2021-03-14 16:46:00.893' AS DateTime), 21)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (20, 5, N'曹操', N'男', CAST(N'2002-08-02' AS Date), N'13245678128', N'', CAST(N'2021-03-14 16:46:00.893' AS DateTime), 19)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (21, 5, N'孙尚香', N'女', CAST(N'2003-08-02' AS Date), N'13245678129', N'', CAST(N'2021-03-14 16:46:00.893' AS DateTime), 18)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (22, 3, N'老1', N'女', CAST(N'2002-08-02' AS Date), N'13245678130', N'广东省', CAST(N'2021-03-14 17:02:36.347' AS DateTime), 19)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (24, 2, N'老2', N'男', CAST(N'2002-08-03' AS Date), N'13345678945', NULL, CAST(N'2021-03-14 17:03:37.733' AS DateTime), 19)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (25, 4, N'老3', N'男', NULL, N'13645987545', N'广东省', CAST(N'2021-03-14 17:03:43.307' AS DateTime), NULL)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (28, 5, N'老4', N'男', CAST(N'2006-03-05' AS Date), N'13456987456', NULL, CAST(N'2021-03-14 17:04:03.957' AS DateTime), 15)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (29, 5, N'老5', N'女', CAST(N'1998-04-12' AS Date), N'15978456123', NULL, CAST(N'2021-03-14 17:04:47.103' AS DateTime), 23)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (30, 4, N'老6', N'男', CAST(N'1996-08-06' AS Date), N'18945674561', NULL, CAST(N'2021-03-14 17:05:04.990' AS DateTime), 25)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (31, 3, N'老7', N'女', CAST(N'1997-04-06' AS Date), N'18845678912', NULL, CAST(N'2021-03-14 17:05:20.570' AS DateTime), 24)
GO
INSERT [dbo].[StuInfo] ([StuId], [ClassId], [StuName], [StuSex], [StuBrithday], [StuPhone], [StuProvince], [CreateDate], [StuAge]) VALUES (32, 2, N'老10', N'女', CAST(N'1998-08-09' AS Date), N'19945645612', NULL, CAST(N'2021-03-14 17:06:08.107' AS DateTime), 23)
GO
SET IDENTITY_INSERT [dbo].[StuInfo] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__CourseIn__9526E2773AB7BECE]    Script Date: 2021/3/15 16:11:24 ******/
ALTER TABLE [dbo].[CourseInfo] ADD UNIQUE NONCLUSTERED 
(
	[CourseName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__StuInfo__2D85FC63AF6FC6FA]    Script Date: 2021/3/15 16:11:24 ******/
ALTER TABLE [dbo].[StuInfo] ADD UNIQUE NONCLUSTERED 
(
	[StuPhone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Scores]  WITH CHECK ADD FOREIGN KEY([CourseId])
REFERENCES [dbo].[CourseInfo] ([CourseId])
GO
ALTER TABLE [dbo].[Scores]  WITH CHECK ADD FOREIGN KEY([StuId])
REFERENCES [dbo].[StuInfo] ([StuId])
GO
ALTER TABLE [dbo].[StuInfo]  WITH CHECK ADD FOREIGN KEY([ClassId])
REFERENCES [dbo].[ClassInfo] ([ClassId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[CourseInfo]  WITH CHECK ADD CHECK  (([CourseCredit]>=(1) AND [CourseCredit]<=(5)))
GO
ALTER TABLE [dbo].[StuInfo]  WITH CHECK ADD CHECK  ((len([StuPhone])=(11)))
GO
ALTER TABLE [dbo].[StuInfo]  WITH CHECK ADD CHECK  (([StuSex]='女' OR [StuSex]='男'))
GO
USE [master]
GO
ALTER DATABASE [TestDB] SET  READ_WRITE 
GO


select * from ClassInfo
select * from StuInfo
select * from Scores
select * from CourseInfo

--ClassInfo 班级表  StuInfo 学生表 Scores 成绩表 CourseInfo 课程表
----统计每个班的男生数
select * from StuInfo
select ClassId,count(StuSex)男生数  from StuInfo where StuSex='男' group by ClassId
----统计每个班的男、女生数
select ClassId,StuSex,count(StuSex)男女生数 from StuInfo group by ClassId ,StuSex
----统计每个班的福建人数
select ClassId,count(StuProvince)福建人数 from StuInfo where StuProvince ='福建省'group by ClassId,StuProvince
----统计每个班的各个省的总人数
select ClassId 班级,StuProvince 省,count(StuProvince)各个省的总人数 from StuInfo 
group by ClassId,StuProvince order by ClassId asc
----统计每个省的女生数
select StuProvince,count(StuSex)女生人数  from StuInfo where StuSex='女' group by StuProvince,StuSex
----统计每个省的男、女生数
select StuProvince,StuSex,count(StuSex) from StuInfo group by StuProvince,StuSex
----统计每个学生的考试总分、平均分
select * from Scores
select StuId 学生,sum(Score)总分,avg(Score)平均 from Scores group by StuId
----统计出考试总分大于620的学生的 考试总分
select StuId 学生,sum(Score)总分 from Scores group by StuId having sum(Score)>620
----统计出每门考试成绩最高分和最低分
select CourseId,max(Score)最高分,min(Score)最低分 from Scores  group by CourseId
----统计出每个学生的各门成绩的平均分
select StuId 学生,CourseId 课程编号,avg(Score)平均分 from Scores group by StuId,CourseId